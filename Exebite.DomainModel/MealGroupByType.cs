﻿using System.Collections.Generic;

namespace Exebite.DomainModel
{
    public class MealGroupByType
    {
        public string TypeName { get; set; }

        public IEnumerable<Meal> Meals { get; set; } = new List<Meal>();
    }
}
