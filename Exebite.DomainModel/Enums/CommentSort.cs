﻿namespace Exebite.DomainModel.Enums
{
    public enum CommentSort
    {
        MostRecent = 0,
        MostLiked = 1,
        MyComments = 2
    }
}
