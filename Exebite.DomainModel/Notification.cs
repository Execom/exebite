﻿using System;

namespace Exebite.DomainModel
{
    public class Notification
    {
        public long Id { get; set; }

        public long CustomerId { get; set; }

        public string NotificationMessage { get; set; }

        public DateTime Created { get; set; }

        public bool Read { get; set; }
    }
}
