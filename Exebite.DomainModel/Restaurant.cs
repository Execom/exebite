﻿using System;

namespace Exebite.DomainModel
{
    public class Restaurant
    {
        public long Id { get; set; }

        public string SheetId { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string LogoUrl { get; set; }

        public string Description { get; set; }

        public bool IsActive { get; set; }

        public string Contact { get; set; }

        public DateTime? OrderDue { get; set; }
    }
}
