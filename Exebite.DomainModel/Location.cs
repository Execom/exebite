﻿namespace Exebite.DomainModel
{
    public class Location
    {
        public short Id { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }
    }
}
