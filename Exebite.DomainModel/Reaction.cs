﻿namespace Exebite.DomainModel
{
    public class Reaction
    {
        public long Id { get; set; }

        public string Description { get; set; }
    }
}
