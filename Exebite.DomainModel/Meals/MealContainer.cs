﻿using System.Collections.Generic;

namespace Exebite.DomainModel.Meals
{
    public class MealContainer
    {
        public List<Meal> DailyMeals { get; set; } = new List<Meal>();

        public List<Meal> StandardMeals { get; set; } = new List<Meal>();
    }
}
