﻿namespace Exebite.DomainModel
{
    public class ReactionToMeal
    {
        public long Id { get; set; }

        public Meal Meal { get; set; }

        public Customer Customer { get; set; }

        public Reaction Reaction { get; set; }
    }
}