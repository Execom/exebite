﻿using System;

namespace Exebite.DomainModel
{
    public class Comment
    {
        public int CommentId { get; set; }

        public string Text { get; set; }

        public DateTime Created { get; set; }

        public long NumberOfLikes { get; set; }

        public bool IsLiked { get; set; }

        public Customer Customer { get; set; }

        public Meal Meal { get; set; }
    }
}
