﻿using System.Collections.Generic;

namespace Exebite.DomainModel
{
    public class Customer
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public decimal Balance { get; set; }

        public string GoogleUserId { get; set; }

        public int Role { get; set; }

        public bool IsActive { get; set; }

        public Location DefaultLocation { get; set; }

        public List<Restaurant> FavouriteRestaurants { get; set; } = new List<Restaurant>();
    }
}
