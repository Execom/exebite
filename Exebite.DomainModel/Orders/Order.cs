﻿using Exebite.DomainModel.Orders;
using System;
using System.Collections.Generic;

namespace Exebite.DomainModel
{
    public class Order
    {
        public long Id { get; set; }

        public decimal Total { get; set; }

        public DateTime Date { get; set; }

        public List<OrderRestaurant> Restaurants { get; set; } = new List<OrderRestaurant>();
    }
}
