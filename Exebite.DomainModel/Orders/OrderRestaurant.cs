﻿using System.Collections.Generic;

namespace Exebite.DomainModel.Orders
{
    public class OrderRestaurant
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public IEnumerable<OrderToMeal> Meals { get; set; } = new List<OrderToMeal>();
    }
}
