﻿namespace Exebite.DomainModel
{
    public class OrderToMeal
    {
        public long MealId { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }

        public int Quantity { get; set; }

        public string Note { get; set; }
    }
}
