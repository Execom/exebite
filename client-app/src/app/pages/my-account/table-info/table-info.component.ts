import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { IStore, getTableColumns, getTableData } from '@store/selectors/selector';
import { IInfoTableColumns, IInfoTableDataOrders, IInfoTableDataPayments } from '@core/models/infoTableData';
import { _isNumberValue } from '@angular/cdk/coercion';
import { CurrencyFormatPipe } from '@shared/pipes/currency-format.pipe';

@Component({
  selector: 'app-table-info',
  templateUrl: './table-info.component.html',
  styleUrls: ['./table-info.component.scss'],
})
export class TableInfoComponent {
  public tableColumns$: Observable<Array<IInfoTableColumns>> = this.store.select(getTableColumns);
  public tableData$: Observable<Array<IInfoTableDataOrders | IInfoTableDataPayments>> = this.store.select(getTableData);

  constructor(private store: Store<IStore>, private curencyPipe: CurrencyFormatPipe) {}

  public getDisplayedColumns = (tableColumnObjects: Array<IInfoTableColumns>): Array<string> =>
    tableColumnObjects.map((column) => column.code);

  public transformToDecimalIfNumber = (value) => (_isNumberValue(value) ? this.curencyPipe.transform(value) : value);
}
