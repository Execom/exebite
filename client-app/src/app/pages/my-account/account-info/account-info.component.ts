import { Component } from '@angular/core';
import { ICustomer } from '@core/models/customer';
import { Store } from '@ngrx/store';
import { getCustomerSelector, IStore } from '@store/selectors/selector';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-account-info',
  templateUrl: './account-info.component.html',
  styleUrls: ['./account-info.component.scss'],
})
export class AccountInfoComponent {
  public customer$: Observable<ICustomer> = this.store.select(getCustomerSelector);

  constructor(private store: Store<IStore>) {}
}
