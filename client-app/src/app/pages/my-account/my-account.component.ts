import { Component } from '@angular/core';
import { MY_ACCOUNT_INFO_SETUP } from '@shared/setup/my-account-info.setup';
import { IDropdownItem } from '@core/models/dropdownItem';
import { Store } from '@ngrx/store';
import { IStore } from '@store/selectors/selector';
import { SetSelectedType } from '@store/actions/infoTable.actions';

@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.component.html',
  styleUrls: ['./my-account.component.scss'],
})
export class MyAccountComponent {
  public myAccountInfos: Array<IDropdownItem> = MY_ACCOUNT_INFO_SETUP;
  public selectedInfo: IDropdownItem = null;

  constructor(private store: Store<IStore>) {}

  setInfoType(infoType) {
    this.selectedInfo = infoType;
    this.store.dispatch(SetSelectedType({ payload: infoType.itemName }));
  }
}
