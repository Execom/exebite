import { Component, OnDestroy, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { CloseModal } from '@store/actions/modal.actions';
import { IStore, getModalDataSelector } from '@store/selectors/selector';
import { RemoveComment } from '@store/actions/comments.actions';

@Component({
  selector: 'app-delete-comment',
  templateUrl: './delete-comment.component.html',
  styleUrls: ['./delete-comment.component.scss'],
})
export class DeleteCommentComponent implements OnInit, OnDestroy {
  public comment: any;
  public modalSubscription: Subscription;

  constructor(private store: Store<IStore>) {}

  ngOnInit(): void {
    this.modalSubscription = this.store.pipe(select(getModalDataSelector)).subscribe((data) => {
      this.comment = data;
    });
  }

  ngOnDestroy() {
    this.modalSubscription.unsubscribe();
  }

  public confirmDelete(): void {
    // @TODO connect with API
    this.store.dispatch(RemoveComment({ payload: this.comment.id }));
    this.closeModal();
  }

  public closeModal(): void {
    this.store.dispatch(CloseModal());
  }
}
