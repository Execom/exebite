import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-comment-expand',
  templateUrl: './comment-expand.component.html',
  styleUrls: ['./comment-expand.component.scss'],
})
export class CommentExpandComponent {
  @Input() mealName: string;
  @Input() commentMode: boolean;
  @Output() modeChanged: EventEmitter<boolean> = new EventEmitter();

  public currentText: string = '';
  public charsLeft: number = 250;

  public changeMode() {
    this.commentMode = false;
    this.modeChanged.emit(this.commentMode);
  }
}
