import { Component, Input } from '@angular/core';
import { Store } from '@ngrx/store';
import { OpenModal } from '@store/actions/modal.actions';
import { MODAL } from '@shared/enums/modal.enums';
import { IStore } from '@store/selectors/selector';

@Component({
  selector: 'app-comment-card',
  templateUrl: './comment-card.component.html',
  styleUrls: ['./comment-card.component.scss'],
})
export class CommentCardComponent {
  @Input() comment: any;
  @Input() showButtons: boolean = false;

  constructor(private store: Store<IStore>) {}

  public like(): void {
    // @TODO
  }

  public edit(): void {
    // @TODO
  }

  public openModal(): void {
    this.store.dispatch(OpenModal({ payload: { data: this.comment, type: MODAL.DELETE_COMMENT } }));
  }
}
