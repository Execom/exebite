import { Component, OnDestroy, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { COMMENTS } from '@shared/mock/comments.mock';
import { COMMENTS_TAB, COMMENTS_TAB_SETUP } from '@shared/setup/comments-tab.setup';
import { IStore, getCommentsSelector, getCommentsMealName } from '@store/selectors/selector';
import { AddComments } from '@store/actions/comments.actions';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss'],
})
export class CommentsComponent implements OnInit, OnDestroy {
  // @todo change type for any
  public comments: Array<any> = [];
  public tabs = COMMENTS_TAB_SETUP;
  public commentMode = false;
  public commentsSubscritpion: Subscription;
  public mealName$: Observable<string> = this.store.select(getCommentsMealName);

  constructor(private store: Store<IStore>) {}

  ngOnInit(): void {
    this.getCommentsFromStore();
    this.getCommentsFromAPI();
  }

  ngOnDestroy() {
    this.commentsSubscritpion.unsubscribe();
  }

  public getCommentsFromStore() {
    this.commentsSubscritpion = this.store.pipe(select(getCommentsSelector)).subscribe(({ comments }) => {
      this.comments = comments;
    });
  }

  public getCommentsFromAPI() {
    // @TODO get comments from API and put in store
    this.store.dispatch(AddComments({ payload: COMMENTS }));
  }

  public onFocus() {
    this.commentMode = true;
  }

  public modeChangedHandler(mode: boolean) {
    this.commentMode = mode;
  }

  public onTabChange(event: string): void {
    switch (event) {
      case COMMENTS_TAB.MOST_RECENT:
        // @TODO
        break;
      case COMMENTS_TAB.MOST_LIKED:
        // @TODO
        break;
      case COMMENTS_TAB.MY_COMMENTS:
        // @TODO
        break;
      default:
        // @TODO
        break;
    }
  }
}
