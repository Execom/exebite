import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';
import { ICustomer } from '@core/models/customer';
import { AuthService } from '@core/services/auth.service';
import { EN, RS } from '@shared/constants/language.constants';
import { Store } from '@ngrx/store';
import { IStore, getCustomerSelector } from '@store/selectors/selector';
import { fetchCustomer } from '@store/actions/customer.actions';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnDestroy {
  public customer$: Observable<ICustomer> = this.store.select(getCustomerSelector);
  public currentLanguage;
  public langChangeSubscription;

  constructor(
    private authService: AuthService,
    private router: Router,
    private translateService: TranslateService,
    private store: Store<IStore>
  ) {}

  ngOnInit(): void {
    this.currentLanguage = this.translateService.currentLang;
    this.subscribeOnLangChange();
    this.store.dispatch(fetchCustomer());
  }

  ngOnDestroy() {
    this.langChangeSubscription.unsubscribe();
  }

  private subscribeOnLangChange(): void {
    this.langChangeSubscription = this.translateService.onLangChange.subscribe((event: LangChangeEvent) => {
      this.currentLanguage = event.lang;
    });
  }

  public logout() {
    this.authService.logout();
    this.router.navigateByUrl('/login');
  }

  public getCustomerFirstName(name) {
    return name.split(' ')[0];
  }

  public changeLanguage(): void {
    this.translateService.use(this.currentLanguage === EN ? RS : EN);
  }
}
