import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NOTIFICATIONS } from '@shared/mock/notifications.mock';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss'],
})
export class NotificationsComponent implements OnInit {
  public notifications: Array<any> = NOTIFICATIONS;
  public newNotifications: number = 2;

  constructor(private translateService: TranslateService) {}

  ngOnInit(): void {}

  public checkType(type: string): string {
    return this.translateService.instant(
      type === 'order' ? 'NOTIFICATION.VIEW_HISTORY' : 'NOTIFICATION.VIEW_RESTAURANT'
    );
  }

  public clearNotifications(): void {
    // @TODO
  }
}
