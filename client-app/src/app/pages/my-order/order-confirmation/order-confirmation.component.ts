import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { IStore } from '@store/selectors/selector';
import { CloseModal } from '@store/actions/modal.actions';
@Component({
  selector: 'app-order-confirmation',
  templateUrl: './order-confirmation.component.html',
  styleUrls: ['./order-confirmation.component.scss'],
})
export class OrderConfirmationComponent {
  constructor(private store: Store<IStore>) {}

  public closeModal(): void {
    this.store.dispatch(CloseModal());
  }
}
