import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { RemoveOrder, SetCondimentsForOrder, SetNoteForOrder } from '@store/actions/orders.actions';
import { IStore } from '@store/selectors/selector';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-order-item',
  templateUrl: './order-item.component.html',
  styleUrls: ['./order-item.component.scss'],
})
export class OrderItemComponent implements OnInit {
  @Input() set data(value) {
    if (value) {
      this.order = value;
      this.note = value.note;
      this.selectedCondiments = [...value.condiments];
    }
  }
  @Input() likes: number = 0;
  @Input() dislikes: number = 0;
  // @TODO change type
  public order: any;
  public expanded: boolean = false;
  public arrowDirection: string = 'right';
  public showCondimentsDropdown: boolean = false;
  public selectedCondiments: Array<string> = [];
  public note: string = '';
  public noteChanged: Subject<string> = new Subject<string>();

  constructor(private store: Store<IStore>) {}

  ngOnInit(): void {
    this.setDebounceOnNote();
  }

  public expand(): void {
    this.expanded = !this.expanded;
    this.arrowDirection = this.expanded ? 'down' : 'right';
  }

  public remove(): void {
    this.store.dispatch(RemoveOrder({ payload: this.order.meal.id }));
  }

  public showCondiments(): void {
    this.showCondimentsDropdown = !this.showCondimentsDropdown;
  }

  public closeDropdown(): void {
    this.showCondimentsDropdown = false;
  }

  public selectCondiment(selectedItem: string, index: number): void {
    this.selectedCondiments.includes(selectedItem)
      ? this.selectedCondiments.splice(1, index)
      : this.selectedCondiments.push(selectedItem);
    this.store.dispatch(SetCondimentsForOrder({ mealId: this.order.meal.id, condiments: this.selectedCondiments }));
  }

  public checkIfSelected(item: string): boolean {
    return this.selectedCondiments.includes(item);
  }

  public canSelectCondiment(maxAllowed: number, condiment: string): boolean {
    return this.selectedCondiments.length === maxAllowed && !this.selectedCondiments.includes(condiment);
  }

  private setDebounceOnNote(): void {
    this.noteChanged.pipe(debounceTime(1000), distinctUntilChanged()).subscribe((value) => {
      this.store.dispatch(SetNoteForOrder({ mealId: this.order.meal.id, note: value }));
    });
  }

  public onNoteChange(text: string): void {
    this.noteChanged.next(text);
  }

  public like(): void {
    // @TODO
  }

  public dislike(): void {
    // @TODO
  }
}
