import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { IStore, getOrdersSelector } from '@store/selectors/selector';
import { Observable } from 'rxjs';
import { MODAL } from '@shared/enums/modal.enums';
import { OpenModal } from '@store/actions/modal.actions';
import { RemoveOrders } from '@store/actions/orders.actions';

@Component({
  selector: 'app-my-order',
  templateUrl: './my-order.component.html',
  styleUrls: ['./my-order.component.scss'],
})
export class MyOrderComponent {
  // @TODO change type
  public orders$: Observable<any> = this.store.select(getOrdersSelector);
  public today: number = Date.now();

  constructor(private store: Store<IStore>) {}

  public order(): void {
    // @TODO connect with API
    this.openConfirmationModal();
  }

  public openConfirmationModal(): void {
    this.store.dispatch(OpenModal({ payload: { data: null, type: MODAL.ORDER_CONFIRMATION } }));
  }

  public cancelOrder(): void {
    this.store.dispatch(RemoveOrders());
  }
}
