import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { AddOrder, RemoveOrder } from '@store/actions/orders.actions';
import { getOrdersSelector, IStore } from '@store/selectors/selector';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-meal-item',
  templateUrl: './meal-item.component.html',
  styleUrls: ['./meal-item.component.scss'],
})
export class MealItemComponent implements OnInit, OnDestroy {
  // @TODO once we know the exact type, change this
  @Input() meal: any;
  @Input() likes: number = 0;
  @Input() dislikes: number = 0;
  public expanded: boolean = false;
  public buttonImg: string = 'add-meal';
  public add: boolean = false;
  public ordersSubscription: Subscription;

  constructor(private store: Store<IStore>) {}

  ngOnInit() {
    this.subscribeOnOrders();
  }

  ngOnDestroy() {
    this.ordersSubscription.unsubscribe();
  }

  public subscribeOnOrders() {
    this.ordersSubscription = this.store.select(getOrdersSelector).subscribe((orders) => {
      if (orders[this.meal.id]) {
        this.buttonImg = 'minus';
        this.add = true;
      }
    });
  }

  public expand(): void {
    this.expanded = !this.expanded;
  }

  public onMealSelected(): void {
    this.add = !this.add;
    this.buttonImg = this.add ? 'minus' : 'add-meal';
    this.add
      ? this.store.dispatch(AddOrder({ payload: { meal: this.meal, quantity: 1, note: '', condiments: [] } }))
      : this.store.dispatch(RemoveOrder({ payload: this.meal.id }));
  }
}
