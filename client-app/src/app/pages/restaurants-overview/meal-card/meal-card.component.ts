import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-meal-card',
  templateUrl: './meal-card.component.html',
  styleUrls: ['./meal-card.component.scss'],
})
export class MealCardComponent implements OnInit {
  @Input() carouselType: string;
  // @TODO change type
  @Input() item: any;
  tooltipColor: string = '';
  offer: string = '';
  isFavourite: boolean = false;
  starColor: string = 'grey';

  ngOnInit(): void {
    this.addTooltipIfExist();
  }

  public addTooltipIfExist(): void {
    if (this.item.tooltip) {
      this.setTooltip(this.item.tooltip, this.item.tooltipText);
    }
  }

  public onFavourite(): void {
    this.isFavourite = !this.isFavourite;
    this.starColor = this.isFavourite ? 'yellow' : 'grey';
  }

  public setTooltip(color: string = '', text: string = '') {
    this.tooltipColor = color;
    this.offer = text;
  }
}
