import { Component, OnInit } from '@angular/core';
import { RESTAURANTS_TAB, RESTAURANTS_TAB_SETUP } from '@shared/setup/restaurants-tab.setup';
import { Router } from '@angular/router';
import { RESTAURANTS } from '@shared/mock/restaurats.mock';
import { FOOD_TYPES } from '@shared/mock/food-types.mock';
import { IStore } from '@store/selectors/selector';
import { Store } from '@ngrx/store';
import { fetchRestaurants } from '@store/actions/restaurants.actions';

@Component({
  selector: 'app-restaurants-overview',
  templateUrl: './restaurants-overview.component.html',
  styleUrls: ['./restaurants-overview.component.scss'],
})
export class RestaurantsOverviewComponent implements OnInit {
  public tabs = RESTAURANTS_TAB_SETUP;
  public currentTab: string = 'restaurant';
  // @TODO change type
  public setCurrentList: any = RESTAURANTS;

  constructor(private router: Router, private store: Store<IStore>) {}

  ngOnInit() {
    this.store.dispatch(fetchRestaurants());
  }

  public onTabChange(tab: string): void {
    this.currentTab = tab;
    this.setCurrentList = tab === RESTAURANTS_TAB.RESTAURANTS ? RESTAURANTS : FOOD_TYPES;
  }

  public checkout(): void {
    this.router.navigateByUrl('/my-order');
  }
}
