import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { SocialLoginModule } from 'angularx-social-login';
import { DropdownDirective } from './shared/directives/dropdown.directive';
import { ClickOutsideDirective } from './shared/directives/clickOutside.directive';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatTooltipModule } from '@angular/material/tooltip';
import { AccessTokenInterceptor } from '@core/interceptors/access-token-interceptor';
import { ToastrModule } from 'ngx-toastr';
import { AppComponent } from './app.component';
import { LoginComponent } from './pages/auth/login/login.component';
import { HomeComponent } from './pages/home/home.component';
import { HeaderComponent } from './pages/home/header/header.component';
import { CommentsComponent } from './pages/comments/comments.component';
import { CommentCardComponent } from './pages/comments/comment-card/comment-card.component';
import { MyOrderComponent } from './pages/my-order/my-order.component';
import { OrderItemComponent } from './pages/my-order/order-item/order-item.component';
import { BreadcrumbComponent } from '@shared/components/breadcrumb/breadcrumb.component';
import { TabsComponent } from '@shared/components/tabs/tabs.component';
import { DeleteCommentComponent } from './pages/comments/delete-comment/delete-comment.component';
import { CustomerService } from '@core/services/customer.service';
import { RestaurantService } from '@core/services/restaurant.service';
import { FoodService } from '@core/services/food.service';
import { MyAccountComponent } from './pages/my-account/my-account.component';
import { RestaurantsOverviewComponent } from './pages/restaurants-overview/restaurants-overview.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { FormsModule } from '@angular/forms';
import { TextareaAutosizeModule } from 'ngx-textarea-autosize';
import { SelectDropdownComponent } from '@shared/components/select-dropdown/select-dropdown.component';
import { TableInfoComponent } from './pages/my-account/table-info/table-info.component';
import { CommonModule } from '@angular/common';
import { MatTableModule } from '@angular/material/table';
import { LocationSelectorComponent } from '@shared/components/location-selector/location-selector.component';
import { MealCardComponent } from './pages/restaurants-overview/meal-card/meal-card.component';
import { MealItemComponent } from './pages/restaurants-overview/meal-card/meal-item/meal-item.component';
import { CurrencyFormatPipe } from '@shared/pipes/currency-format.pipe';
import { QuantityComponent } from '@shared/components/quantity/quantity.component';
import { OrderButtonComponent } from '@shared/components/order-button/order-button.component';
import { NotificationsComponent } from './pages/home/notifications/notifications.component';
import { CommentExpandComponent } from './pages/comments/comment-expand/comment-expand.component';
import { StoreModule } from '@ngrx/store';
import { modalReducer } from '@store/reducers/modal.reducer';
import { commentsReducer } from '@store/reducers/comments.reducer';
import { infoTableReducer } from '@store/reducers/infoTable.reducer';
import { ordersReducer } from '@store/reducers/orders.reducer';
import { ModalComponent } from '@shared/components/modal/modal.component';
import { OrderConfirmationComponent } from './pages/my-order/order-confirmation/order-confirmation.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { CarouselComponent } from '@shared/components/carousel/carousel.component';
import { AccountInfoComponent } from './pages/my-account/account-info/account-info.component';
import { customerReducer } from '@store/reducers/customer.reducer';
import { EffectsModule } from '@ngrx/effects';
import { CustomerEffects } from '@store/effects/customer.effects';
import { RestaurantsEffects } from '@store/effects/restaurants.effects';
import { restaurantsReducer } from '@store/reducers/restaurants.reducer';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

const COMPONENTS = [
  AppComponent,
  LoginComponent,
  HomeComponent,
  HeaderComponent,
  MyAccountComponent,
  RestaurantsOverviewComponent,
  CommentsComponent,
  CommentCardComponent,
  MyOrderComponent,
  OrderItemComponent,
  BreadcrumbComponent,
  TabsComponent,
  SelectDropdownComponent,
  TableInfoComponent,
  LocationSelectorComponent,
  MealCardComponent,
  MealItemComponent,
  QuantityComponent,
  OrderButtonComponent,
  NotificationsComponent,
  DeleteCommentComponent,
  ModalComponent,
  CarouselComponent,
  OrderConfirmationComponent,
  AccountInfoComponent,
  CommentExpandComponent,
];

const SERVICES = [CustomerService, RestaurantService, FoodService];

@NgModule({
  declarations: [...COMPONENTS, DropdownDirective, ClickOutsideDirective, CurrencyFormatPipe],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SocialLoginModule,
    HttpClientModule,
    NoopAnimationsModule,
    MatTooltipModule,
    MatExpansionModule,
    MatTableModule,
    CommonModule,
    ToastrModule.forRoot({
      positionClass: 'toast-top-center',
    }),
    CarouselModule,
    FormsModule,
    TextareaAutosizeModule,
    StoreModule.forRoot({
      modal: modalReducer,
      comments: commentsReducer,
      infoTable: infoTableReducer,
      orders: ordersReducer,
      customer: customerReducer,
      restaurants: restaurantsReducer,
    }),
    EffectsModule.forRoot([CustomerEffects, RestaurantsEffects]),
    StoreDevtoolsModule.instrument({ logOnly: true }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
  ],
  providers: [
    ...SERVICES,
    CurrencyFormatPipe,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AccessTokenInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
