import { createReducer, on } from '@ngrx/store';
import { ICustomer } from '@core/models/customer';
import * as CustomerActions from '../actions/customer.actions';

const initialState: ICustomer = {
  id: null,
  name: '',
  balance: 0,
  defaultLocation: null,
  googleUserId: '',
  favouriteRestaurants: null,
  photoUrl: '',
};

export const customerReducer = createReducer(
  initialState,
  on(CustomerActions.fetchCustomerSuccess, (state, action) => ({
    ...state,
    ...action.payload,
  })),
  on(CustomerActions.fetchCustomerError, (state) => ({
    ...state,
    ...initialState,
  }))
);
