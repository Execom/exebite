import { createReducer, on } from '@ngrx/store';
import * as ModalActions from '../actions/modal.actions';
import { IModalState } from '@core/models/modal-state';

const initialState: IModalState = {
  isOpen: false,
  data: null,
  type: '',
};

export const modalReducer = createReducer(
  initialState,
  on(ModalActions.OpenModal, (state, action) => ({
    ...state,
    isOpen: true,
    ...action.payload,
  })),
  on(ModalActions.CloseModal, (state, action) => ({
    ...state,
    isOpen: false,
    data: null,
    type: '',
  }))
);
