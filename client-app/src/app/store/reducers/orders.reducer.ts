import { createReducer, on } from '@ngrx/store';
import { IOrdersState } from '@core/models/orders-state';
import * as OrdersActions from '../actions/orders.actions';

const initialState: IOrdersState = {
  orders: {},
  price: 0,
};

export const ordersReducer = createReducer(
  initialState,
  on(OrdersActions.AddOrder, (state, action) => ({
    ...state,
    orders: { ...state.orders, [action.payload.meal.id]: action.payload },
    price: state.price + action.payload.meal.price,
  })),
  on(OrdersActions.RemoveOrder, (state, action) => ({
    ...state,
    price: state.price - state.orders[action.payload].meal.price * state.orders[action.payload].quantity,
    orders: Object.keys(state.orders).reduce((object, key) => {
      if (key !== action.payload.toString()) {
        object[key] = { ...state.orders[key] };
      }

      return object;
    }, {}),
  })),
  on(OrdersActions.IncreaseQuantity, (state, action) => ({
    ...state,
    orders: {
      ...state.orders,
      [action.payload]: { ...state.orders[action.payload], quantity: state.orders[action.payload].quantity + 1 },
    },
    price: state.price + state.orders[action.payload].meal.price,
  })),
  on(OrdersActions.DecreaseQuantity, (state, action) => ({
    ...state,
    orders: {
      ...state.orders,
      [action.payload]: { ...state.orders[action.payload], quantity: state.orders[action.payload].quantity - 1 },
    },
    price: state.price - state.orders[action.payload].meal.price,
  })),
  on(OrdersActions.SetNoteForOrder, (state, action) => ({
    ...state,
    orders: { ...state.orders, [action.mealId]: { ...state.orders[action.mealId], note: action.note } },
  })),
  on(OrdersActions.SetCondimentsForOrder, (state, action) => ({
    ...state,
    orders: { ...state.orders, [action.mealId]: { ...state.orders[action.mealId], condiments: action.condiments } },
  })),
  on(OrdersActions.RemoveOrders, (state) => ({
    ...state,
    orders: {},
    price: 0,
  }))
);
