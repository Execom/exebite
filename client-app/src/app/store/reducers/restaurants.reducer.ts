import { createReducer, on } from '@ngrx/store';
import { IRestaurant } from '@core/models/restaurant';
import * as RestaurantsActions from '../actions/restaurants.actions';

const initialState: IRestaurant[] = [];

export const restaurantsReducer = createReducer(
  initialState,
  on(RestaurantsActions.fetchRestaurantsSuccess, (state, { payload }) => ({
    ...state,
    ...payload,
  })),
  on(RestaurantsActions.fetchRestaurantsError, (state) => ({
    ...state,
    ...initialState,
  }))
);
