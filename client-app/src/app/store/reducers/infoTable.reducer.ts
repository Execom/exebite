import { createReducer, on } from '@ngrx/store';
import { IInfoTableState } from '@core/models/infoTable-state';
import * as InfoTableActions from '../actions/infoTable.actions';
import { AccountInfoTypes } from '@shared/enums/account-info-types';
import { PAYMENTS, ORDERS } from '@shared/mock/my-account-table-info.mock';
import { PAYMENT_INFO_TABLE_SETUP, ORDER_INFO_TABLE_SETUP } from '@shared/setup/my-account-info.setup';
import { IInfoTableColumns, IInfoTableDataOrders, IInfoTableDataPayments } from '@core/models/infoTableData';

const intitialState: IInfoTableState = {
  selectedType: '',
  tableColumns: [],
  tableData: [],
};

export const infoTableReducer = createReducer(
  intitialState,
  on(InfoTableActions.SetSelectedType, (state, action) => ({
    ...state,
    selectedType: action.payload,
    tableColumns: getTableColumns(action.payload),
    tableData: getTableData(action.payload),
  }))
);

const getTableColumns = (selectedType): Array<IInfoTableColumns> =>
  selectedType === AccountInfoTypes.ORDERS_TYPE ? ORDER_INFO_TABLE_SETUP : PAYMENT_INFO_TABLE_SETUP;

const getTableData = (selectedType): Array<IInfoTableDataOrders | IInfoTableDataPayments> =>
  selectedType === AccountInfoTypes.ORDERS_TYPE ? getOrders() : getPayments();

const getOrders = (): Array<IInfoTableDataOrders> => {
  // @TODO Add API call with ngrx effects
  const ordersArray = ORDERS.map(({ date, meal }) => {
    return {
      date: date,
      name: meal.name,
      restaurantId: meal.restaurantId,
      total: meal.price,
    };
  });

  return ordersArray;
};

const getPayments = (): Array<IInfoTableDataPayments> => {
  // @TODO ADD API call with ngrx effects
  return PAYMENTS;
};
