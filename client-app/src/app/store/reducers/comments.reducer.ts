import { createReducer, on } from '@ngrx/store';
import { ICommentsState } from '@core/models/comments-state';
import * as CommentsActions from '../actions/comments.actions';

const initialState: ICommentsState = {
  mealName: '',
  comments: [],
};

export const commentsReducer = createReducer(
  initialState,
  on(CommentsActions.AddComments, (state, action) => ({
    ...state,
    comments: [...state.comments, ...action.payload],
    mealName: action.payload[0].meal.name,
  })),
  on(CommentsActions.RemoveComment, (state, action) => ({
    ...state,
    comments: state.comments.filter((item) => item.id !== action.payload),
  }))
);
