import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, mergeMap, catchError } from 'rxjs/operators';
import { CustomerService } from '../../core/services/customer.service';
import * as customerActions from '../actions/customer.actions';

@Injectable()
export class CustomerEffects {
  constructor(private actions$: Actions, private customerService: CustomerService) {}

  getCustomer$ = createEffect(() =>
    this.actions$.pipe(
      ofType(customerActions.fetchCustomer),
      mergeMap(() =>
        this.customerService.fetchCustomerData().pipe(
          map((data) => {
            return customerActions.fetchCustomerSuccess({ payload: data });
          }),
          catchError((err) => {
            console.error(err);
            return of(customerActions.fetchCustomerError());
          })
        )
      )
    )
  );
}
