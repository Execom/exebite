import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, mergeMap, catchError } from 'rxjs/operators';
import { RestaurantService } from '@core/services/restaurant.service';
import * as restaurantsActions from '../actions/restaurants.actions';

@Injectable()
export class RestaurantsEffects {
  constructor(private actions$: Actions, private restaurantService: RestaurantService) {}

  getrestaurants$ = createEffect(() =>
    this.actions$.pipe(
      ofType(restaurantsActions.fetchRestaurants),
      mergeMap(() =>
        this.restaurantService.fetchRestaurants().pipe(
          map((data) => restaurantsActions.fetchRestaurantsSuccess({ payload: data })),
          catchError((err) => {
            console.error(err);
            return of(restaurantsActions.fetchRestaurantsError);
          })
        )
      )
    )
  );
}
