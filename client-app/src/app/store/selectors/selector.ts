import { ICommentsState } from '@core/models/comments-state';
import { IModalState } from '@core/models/modal-state';
import { IInfoTableState } from '@core/models/infoTable-state';
import { IOrdersState } from '@core/models/orders-state';
import { ICustomer } from '@core/models/customer';
import { IRestaurant } from '@core/models/restaurant';
export interface IStore {
  comments: ICommentsState;
  modal: IModalState;
  infoTable: IInfoTableState;
  orders: IOrdersState;
  customer: ICustomer;
  restaurant: IRestaurant;
}

export const getCommentsSelector = (state: IStore) => state.comments;
export const getCommentsMealName = (state: IStore) => state.comments.mealName;
export const getModalDataSelector = (state: IStore) => state.modal.data;
export const getModalSelector = (state: IStore) => state.modal;
export const getTableColumns = (state: IStore) => state.infoTable.tableColumns;
export const getTableData = (state: IStore) => state.infoTable.tableData;
export const getSelectedType = (state: IStore) => state.infoTable.selectedType;
export const getOrdersSelector = (state: IStore) => state.orders.orders;
export const getPriceSelector = (state: IStore) => state.orders.price;
export const getCustomerSelector = (state: IStore) => state.customer;
export const getRestaurantsSelector = (state: IStore) => state.restaurant;
