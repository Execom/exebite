import { createAction, props } from '@ngrx/store';
import { IRestaurant } from '@core/models/restaurant';

const FETCH_RESTAURANTS = '[RESTAURANT] Fetch restaurants';
const FETCH_RESTAURANTS_SUCCESS = '[RESTAURANT] Fetch Restaurants Success';
const FETCH_RESTAURANTS_ERROR = '[RESTAURANT] Fetch Restaurants Error';

export const fetchRestaurants = createAction(FETCH_RESTAURANTS);
export const fetchRestaurantsSuccess = createAction(FETCH_RESTAURANTS_SUCCESS, props<{ payload: IRestaurant[] }>());
export const fetchRestaurantsError = createAction(FETCH_RESTAURANTS_ERROR);
