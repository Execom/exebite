import { createAction, props } from '@ngrx/store';

const ADD_ORDER = '[Order] Add Order';
const REMOVE_ORDER = '[Order] Remove Order';
const GET_ORDERS = '[Order] Get Orders';
const INCREASE_QUANTITY = '[Order] Increase Quantity';
const DECREASE_QUANTITY = '[Order] Decrease Quantity';
const REMOVE_ORDERS = '[Order] Remove Orders';
const SET_CONDIMENTS_FOR_ORDER = '[Order] Set Condiments For Order';
const SET_NOTE_FOR_ORDER = '[Order] Set Note For Order';

// @TODO change type
export const AddOrder = createAction(ADD_ORDER, props<{ payload: any }>());
export const RemoveOrder = createAction(REMOVE_ORDER, props<{ payload: string }>());
export const GetOrders = createAction(GET_ORDERS);
export const IncreaseQuantity = createAction(INCREASE_QUANTITY, props<{ payload: string }>());
export const DecreaseQuantity = createAction(DECREASE_QUANTITY, props<{ payload: string }>());
export const RemoveOrders = createAction(REMOVE_ORDERS);
export const SetNoteForOrder = createAction(SET_NOTE_FOR_ORDER, props<{ mealId: string; note: string }>());
export const SetCondimentsForOrder = createAction(
  SET_CONDIMENTS_FOR_ORDER,
  props<{ mealId: string; condiments: Array<string> }>()
);
