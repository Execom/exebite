import { createAction, props } from '@ngrx/store';
import { ICustomer } from '@core/models/customer';

const FETCH_CUSTOMER = '[CUSTOMER] Fetch Customer';
const FETCH_CUSTOMER_SUCCESS = '[CUSTOMER] Fetch Customer Success';
const FETCH_CUSTOMER_ERROR = '[CUSTOMER] Fetch Customer Error';

export const fetchCustomer = createAction(FETCH_CUSTOMER);
export const fetchCustomerSuccess = createAction(FETCH_CUSTOMER_SUCCESS, props<{ payload: ICustomer }>());
export const fetchCustomerError = createAction(FETCH_CUSTOMER_ERROR);
