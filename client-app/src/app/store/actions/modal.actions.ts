import { createAction, props } from '@ngrx/store';

const OPEN_MODAL = '[Modal] Open Modal';
const CLOSE_MODAL = '[Modal] Close Modal';

export const OpenModal = createAction(OPEN_MODAL, props<{ payload }>());

export const CloseModal = createAction(CLOSE_MODAL);
