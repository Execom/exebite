import { createAction, props } from '@ngrx/store';

const ADD_COMMENTS = '[Comment] Add Comment';
const REMOVE_COMMENT = '[Comment] Remove Comment';

export const AddComments = createAction(ADD_COMMENTS, props<{ payload: any }>());

export const RemoveComment = createAction(REMOVE_COMMENT, props<{ payload: any }>());
