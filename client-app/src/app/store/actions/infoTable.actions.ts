import { createAction, props } from '@ngrx/store';

const SET_SELECTED_TYPE = '[InfoTable] SET_SELECTED_TYPE';
export const SetSelectedType = createAction(SET_SELECTED_TYPE, props<{ payload: string }>());
