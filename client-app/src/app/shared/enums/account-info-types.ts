export enum AccountInfoTypes {
  ORDERS_TYPE = 'myOrders',
  PAYMENTS_TYPE = 'myPayments',
}
