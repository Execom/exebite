export const PAYMENTS = [
  {
    id: 0,
    date: '03 Jun 2020',
    creditBalance: -300,
    payment: 3000,
    newBalance: 2700,
  },
  {
    id: 1,
    date: '05 Jun 2020',
    creditBalance: -300,
    payment: 3000,
    newBalance: 2700,
  },
  {
    id: 2,
    date: '03 Jun 2020',
    creditBalance: -300,
    payment: 3000,
    newBalance: 2700,
  },
  {
    id: 3,
    date: '07 Jun 2020',
    creditBalance: -300,
    payment: 3000,
    newBalance: 2700,
  },
  {
    id: 4,
    date: '03 Jun 2020',
    creditBalance: -300,
    payment: 3000,
    newBalance: 2700,
  },
  {
    id: 5,
    date: '03 Jun 2020',
    creditBalance: -300,
    payment: 3000,
    newBalance: 2700,
  },
  {
    id: 6,
    date: '03 Jun 2020',
    creditBalance: -300,
    payment: 3000,
    newBalance: 2700,
  },
  {
    id: 7,
    date: '03 Jun 2020',
    creditBalance: -300,
    payment: 3000,
    newBalance: 2700,
  },
  {
    id: 8,
    date: '03 Jun 2020',
    creditBalance: -300,
    payment: 3000,
    newBalance: 2700,
  },
];

export const ORDERS = [
  {
    id: 0,
    date: '03 Jun 2020',

    meal: {
      id: 0,
      name: 'Španat bečka',
      restaurantId: 'Index House',
      price: 300,
    },
  },

  {
    id: 1,
    date: '04 Jun 2020',

    meal: {
      id: 2,
      name: 'Burger',
      restaurantId: 'Idex House',
      price: 100,
    },
  },

  {
    id: 3,
    date: '03 Jun 2020',

    meal: {
      id: 4,
      name: 'Španat bečka',
      restaurantId: 'Heyday',
      price: 600,
    },
  },

  {
    id: 4,
    date: '03 Jun 2020',

    meal: {
      id: 6,
      name: 'Španat bečka',
      restaurantId: 'Serpica',
      price: 300,
    },
  },
  {
    id: 5,
    date: '03 Jun 2020',

    meal: {
      id: 6,
      name: 'Španat bečka',
      restaurantId: 'Lipca',
      price: 300,
    },
  },
  {
    id: 6,
    date: '03 Jun 2020',

    meal: {
      id: 7,
      name: 'Bela čorba',
      restaurantId: 'Serpica',
      price: 300,
    },
  },
  {
    id: 7,
    date: '03 Jun 2020',

    meal: {
      id: 6,
      name: 'Španat bečka',
      restaurantId: 'Idex House',
      price: 300,
    },
  },
  {
    id: 8,
    date: '03 Jun 2020',

    meal: {
      id: 6,
      name: 'Španat bečka',
      restaurantId: 'HeyDay',
      price: 300,
    },
  },
  {
    id: 9,
    date: '03 Jun 2020',

    meal: {
      id: 6,
      name: 'Španat bečka',
      restaurantId: 'Lipa',
      price: 300,
    },
  },
  {
    id: 10,
    date: '03 Jun 2020',

    meal: {
      id: 6,
      name: 'Španat bečka',
      restaurantId: 'Serpica',
      price: 300,
    },
  },
  {
    id: 11,
    date: '03 Jun 2020',
    meal: {
      id: 7,
      name: 'Bela čorba',
      restaurantId: 'Index',
      price: 300,
    },
  },
];
