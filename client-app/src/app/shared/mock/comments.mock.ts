export const COMMENTS = [
  {
    id: 1,
    user: {
      name: 'Mirjana Marjanović',
      photoUrl: '',
    },
    meal: {
      id: 123456,
      name: 'Španat bečka',
    },
    date: '25.06.2020.',
    comment:
      ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam consectetur malesuada elementum. Sed tristique faucibus ante, nec mattis ante pellentesque at. Morbi iaculis, sem quis suscipit posuere, mi ex rutrum augue, in lobortis erat odio et lectus.',
    likes: 121,
  },
  {
    id: 2,
    user: {
      name: 'Mirjana Marjanović',
      photoUrl: '',
    },
    meal: {
      id: 123456,
      name: 'Španat bečka',
    },
    date: '26.06.2020.',
    comment:
      ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam consectetur malesuada elementum. Sed tristique faucibus ante, nec mattis ante pellentesque at. Morbi iaculis, sem quis suscipit posuere, mi ex rutrum augue, in lobortis erat odio et lectus.',
    likes: 121,
  },
];
