import { ORDERS } from '../mock/oders.mock';
import { ICustomer } from '@core/models/customer';

export const USER_MOCK: ICustomer = {
  id: 190,
  name: 'Mrako Mrakovic',
  balance: 300,
  defaultLocation: {
    id: 1,
    name: 'BVS',
    address: 'Bulever Vojvode Stepe 50',
  },
  googleUserId: 'mmrakovic@execom.eu',
  favouriteMeals: [],
  orders: ORDERS,
  photoUrl: 'https://lh3.googleusercontent.com/a-/AOh14Gh2M33YiUpVO9VXYRbOMSDqntObW1II8NSsLZTj',
};
