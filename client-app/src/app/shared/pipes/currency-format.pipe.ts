import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'currencyFormat',
})
export class CurrencyFormatPipe implements PipeTransform {
  transform(value: number, decimalLength: number = 2, decimalDelimiter: string = ','): string {
    const num = value.toFixed(Math.max(0, decimalLength));

    return decimalDelimiter ? num.replace('.', decimalDelimiter) : num;
  }
}
