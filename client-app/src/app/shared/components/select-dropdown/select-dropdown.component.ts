import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IDropdownItem } from '@core/models/dropdownItem';

@Component({
  selector: 'app-select-dropdown',
  templateUrl: './select-dropdown.component.html',
  styleUrls: ['./select-dropdown.component.scss'],
})
export class SelectDropdownComponent implements OnInit {
  @Input() items: Array<IDropdownItem>;
  @Input() isLocationSelector: boolean = false;
  @Output() selectedItem: EventEmitter<IDropdownItem> = new EventEmitter();

  public currentItem: IDropdownItem = null;
  public showItemDropdown: boolean = false;
  public selectorImg: string = '';

  constructor() {}

  ngOnInit(): void {
    this.selectorImg = this.isLocationSelector ? 'white-arrow-down' : 'light-arrow-down';
    this.currentItem = this.items[0];
    this.selectedItem.emit(this.currentItem);
  }

  public selectItem(dropdownItem) {
    this.currentItem = dropdownItem;
    this.selectedItem.emit(this.currentItem);
  }

  public showDropdownItemSelection(): void {
    this.showItemDropdown = !this.showItemDropdown;
  }

  public closeDropdown(): void {
    this.showItemDropdown = false;
  }
}
