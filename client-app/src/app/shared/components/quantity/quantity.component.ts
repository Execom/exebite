import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { DecreaseQuantity, IncreaseQuantity } from '@store/actions/orders.actions';
import { getOrdersSelector, IStore } from '@store/selectors/selector';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-quantity',
  templateUrl: './quantity.component.html',
  styleUrls: ['./quantity.component.scss'],
})
export class QuantityComponent implements OnInit, OnDestroy {
  @Input() mealId: string;
  public quantity: number = 0;
  public ordersSubscription: Subscription;

  constructor(private store: Store<IStore>) {}

  ngOnInit() {
    this.subscribeOnOrders();
  }

  ngOnDestroy() {
    this.ordersSubscription.unsubscribe();
  }

  public subscribeOnOrders() {
    this.ordersSubscription = this.store.select(getOrdersSelector).subscribe((orders) => {
      if (orders[this.mealId]) {
        this.quantity = orders[this.mealId].quantity;
      }
    });
  }

  public increase(): void {
    this.store.dispatch(IncreaseQuantity({ payload: this.mealId }));
  }

  public decrease(): void {
    this.store.dispatch(DecreaseQuantity({ payload: this.mealId }));
  }
}
