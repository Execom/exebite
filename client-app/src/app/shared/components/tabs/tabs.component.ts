import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss'],
})
export class TabsComponent {
  @Input() tabs: Array<any>;
  @Input() activeTab: string;
  @Output() selectedTab: EventEmitter<any> = new EventEmitter();

  public onClick(tabName: string): void {
    this.activeTab = tabName;
    this.selectedTab.emit(tabName);
  }
}
