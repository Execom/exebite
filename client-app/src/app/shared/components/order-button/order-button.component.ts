import { Component, EventEmitter, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { getPriceSelector, IStore } from '@store/selectors/selector';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-order-button',
  templateUrl: './order-button.component.html',
  styleUrls: ['./order-button.component.scss'],
})
export class OrderButtonComponent {
  // @TODO once we know the exact type, change this
  @Output() confirmOrder: EventEmitter<any> = new EventEmitter();
  public price$: Observable<number> = this.store.select(getPriceSelector);

  constructor(private store: Store<IStore>) {}

  public order(): void {
    this.confirmOrder.emit();
  }
}
