import { Component, OnInit } from '@angular/core';
import { IDropdownItem } from '@core/models/dropdownItem';
import { LOCATION_SETUP } from '../../setup/my-account-info.setup';

@Component({
  selector: 'app-location-selector',
  templateUrl: './location-selector.component.html',
  styleUrls: ['./location-selector.component.scss'],
})
export class LocationSelectorComponent {
  public locationSetup: Array<IDropdownItem> = LOCATION_SETUP;
}
