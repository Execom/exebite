import { Component, Input, ViewEncapsulation } from '@angular/core';
import { CAROUSEL_OPTIONS } from '@shared/setup/carousel.setup';
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CarouselComponent {
  // @TODO once we know the exact type, change this
  @Input() carouselList: any = null;
  @Input() carouselType: string = '';
  public carouselOptions: OwlOptions = CAROUSEL_OPTIONS;
}
