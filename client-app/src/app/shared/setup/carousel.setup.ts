export const CAROUSEL_OPTIONS = {
  loop: false,
  dots: false,
  mouseDrag: true,
  touchDrag: true,
  pullDrag: false,
  lazyLoad: true,
  autoWidth: false,
  navSpeed: 700,
  navText: ['<span>&#9664;</span>', '<span>&#9654;</span>'],
  autoHeight: true,
  responsive: {
    0: {
      items: 1,
    },
    600: {
      items: 2,
    },
    1300: {
      items: 3,
    },
  },
  nav: true,
};
