export const COMMENTS_TAB = {
  MOST_RECENT: 'mostRecent',
  MOST_LIKED: 'mostLiked',
  MY_COMMENTS: 'myComments',
};

export const COMMENTS_TAB_SETUP = [
  { text: 'COMMENT.MOST_RECENT', tabName: COMMENTS_TAB.MOST_RECENT },
  { text: 'COMMENT.MOST_LIKED', tabName: COMMENTS_TAB.MOST_LIKED },
  { text: 'COMMENT.MY_COMMENTS', tabName: COMMENTS_TAB.MY_COMMENTS },
];
