export const MY_ACCOUNT_INFO_SETUP = [
  { id: 0, text: 'DROPDOWN_INFO.MY_ORDERS', itemName: 'myOrders' },
  { id: 1, text: 'DROPDOWN_INFO.MY_PAYMENTS', itemName: 'myPayments' },
];

export const LOCATION_SETUP = [
  { id: 0, text: 'BVS', itemName: 'bvs' },
  { id: 1, text: 'MM', itemName: 'mm' },
];

export const PAYMENT_INFO_TABLE_SETUP = [
  {
    code: 'date',
    text: 'TABLE_INFO.DATE',
  },
  {
    code: 'creditBalance',
    text: 'TABLE_INFO.CREDIT_BALANCE',
  },
  {
    code: 'payment',
    text: 'TABLE_INFO.PAYMENT',
  },
  {
    code: 'newBalance',
    text: 'TABLE_INFO.NEW_BALANCE',
  },
];

export const ORDER_INFO_TABLE_SETUP = [
  {
    code: 'date',
    text: 'TABLE_INFO.DATE',
  },
  {
    code: 'restaurantId',
    text: 'TABLE_INFO.RESTAURANT',
  },
  {
    code: 'name',
    text: 'TABLE_INFO.ORDERS',
  },

  {
    code: 'total',
    text: 'TABLE_INFO.TOTAL',
  },
];
