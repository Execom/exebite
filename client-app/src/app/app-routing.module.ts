import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthCallbackComponent } from './pages/auth/auth-callback/auth-callback.component';
import { AuthGuard } from '@core/guards/auth.guard';
import { LoginComponent } from './pages/auth/login/login.component';
import { HomeComponent } from './pages/home/home.component';
import { MyAccountComponent } from './pages/my-account/my-account.component';
import { CommentsComponent } from './pages/comments/comments.component';
import { MyOrderComponent } from './pages/my-order/my-order.component';
import { RestaurantsOverviewComponent } from './pages/restaurants-overview/restaurants-overview.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'restaurants' },
  { path: 'login', component: LoginComponent },
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'auth-callback', component: AuthCallbackComponent },
  {
    path: '',
    canActivate: [AuthGuard],
    component: HomeComponent,
    children: [
      { path: 'my-order', component: MyOrderComponent },
      { path: 'my-account', component: MyAccountComponent },
      { path: 'comments', component: CommentsComponent },
      { path: 'restaurants', component: RestaurantsOverviewComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
