import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { AuthService } from '@core/services/auth.service';
import { Observable, from } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Injectable()
export class AccessTokenInterceptor implements HttpInterceptor {
  constructor(public auth: AuthService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // Excluded static assests URLs to avoid error when getAuthorizationHeaderValue() is called before user has logged in.
    // @TODO Change backend URLs that require authorization to have specific prefix for possibly better solution.
    if (!req.url.toLocaleLowerCase().includes('/assets/')) {
      return from(this.auth.getAuthorizationHeaderValue()).pipe(
        switchMap((authHeader) => {
          req = req.clone({
            setHeaders: {
              Authorization: authHeader,
            },
          });

          return next.handle(req);
        })
      );
    }

    return next.handle(req);
  }
}
