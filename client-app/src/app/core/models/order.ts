import { IMeal } from '../models/meal';

export interface IOrder {
  id: number;
  date: string;
  meal: IMeal;
  quantity: number;
  note: string;
}
