export class IFood {
  id: number;
  name: string;
  price: number;
  description: string;
  restaurantId: number;
}
