export interface IModalState {
  isOpen: boolean;
  data: any;
  type: string;
}
