export class IRestaurant {
  id: number;
  name: string;
  isActive: boolean;
  orderDue: string;
}
