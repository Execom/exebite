import { ILocation } from '../models/location';
import { IRestaurant } from '../models/restaurant';

export interface ICustomer {
  id: number;
  name: string;
  balance: number;
  defaultLocation: ILocation;
  googleUserId: string;
  favouriteRestaurants: Array<IRestaurant>;
  photoUrl: string;
}
