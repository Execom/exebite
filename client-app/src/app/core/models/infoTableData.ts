export interface IInfoTableDataOrders {
  date: string;
  name: string;
  restaurantId: string;
  total: number;
}

export interface IInfoTableDataPayments {
  id: number;
  date: string;
  creditBalance: number;
  payment: number;
  newBalance: number;
}

export interface IInfoTableColumns {
  code: string;
  text: string;
}
