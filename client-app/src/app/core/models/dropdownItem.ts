export interface IDropdownItem {
  id: number;
  text: string;
  itemName: string;
}
