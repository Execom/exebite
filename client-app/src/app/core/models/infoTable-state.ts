import { IInfoTableColumns, IInfoTableDataPayments, IInfoTableDataOrders } from '@core/models/infoTableData';

export interface IInfoTableState {
  selectedType: string;
  tableColumns: Array<IInfoTableColumns>;
  tableData: Array<IInfoTableDataOrders | IInfoTableDataPayments>;
}
