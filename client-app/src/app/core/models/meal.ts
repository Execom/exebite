export interface IMeal {
  id: number;
  name: string;
  type: string;
  price: number;
  description: string;
  note: string;
  condiments: Array<string>;
}
