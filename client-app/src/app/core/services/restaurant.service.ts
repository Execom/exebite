import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IRestaurant } from '@core/models/restaurant';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { environment } from '@environment/environment';

@Injectable()
export class RestaurantService {
  constructor(private http: HttpClient) {}

  private restaurantsDataUrl = `${environment.backendBaseAPIUrl}/restaurants/Query?Page=1&Size=10`;

  private restaurantDataUrl = (id) => `${environment.backendBaseAPIUrl}/restaurant/Query?id=${id}&Page=1&Size=5`;

  fetchRestaurants(): Observable<IRestaurant[]> {
    return this.http.get<{ items: IRestaurant[] }>(this.restaurantsDataUrl).pipe(map((data) => [...data.items]));
  }

  fetchDataForRestaurant(id) {
    return this.http
      .get<{ items: IRestaurant }>(this.restaurantDataUrl(id))
      .pipe(map((items) => ({ ...items.items[0] })));
  }
}
