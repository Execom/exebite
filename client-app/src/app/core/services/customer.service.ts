import { Injectable } from '@angular/core';
import { ICustomer } from '@core/models/customer';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environment/environment';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';

@Injectable()
export class CustomerService {
  private customerDataUrl = (googleUserId) =>
    `${environment.backendBaseAPIUrl}/Customer/Query?GoogleUserId=${googleUserId}&Page=1&Size=1`;

  constructor(private http: HttpClient, private authService: AuthService) {}

  fetchCustomerData(): Observable<ICustomer> {
    const profile = this.authService.getClaimsFromLocalStorage();

    return this.http
      .get<{ items: ICustomer[] }>(this.customerDataUrl(profile.email))
      .pipe(map((items) => ({ ...items.items[0], photoUrl: profile.picture })));
  }
}
