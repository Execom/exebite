import { Component, OnDestroy, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { IStore, getModalSelector } from '@store/selectors/selector';
import { DEFAULT_LANGUAGE } from '@shared/constants/language.constants';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'client-app';

  public isOpen: boolean;
  public type: string;
  public modalSubscription: Subscription;

  constructor(private store: Store<IStore>, private translateService: TranslateService) {
    this.translateService.setDefaultLang(DEFAULT_LANGUAGE);
    this.translateService.use(DEFAULT_LANGUAGE);
  }

  ngOnInit(): void {
    this.modalSubscription = this.store.pipe(select(getModalSelector)).subscribe(({ isOpen, type }) => {
      this.isOpen = isOpen;
      this.type = type;
    });
  }

  ngOnDestroy() {
    this.modalSubscription.unsubscribe();
  }
}
