# Exebite

## Project goal

Exebite is a research and development project for internal food ordering. Goal of this project is to automate the ordering of food, payment's and reporting for this current process.

Menus/lunches are administers by restaurants delivering food. Restaurants are filling sheets with available orders for each day.

This system/app will load the data from sheets to internal database and make it available via REST point to Client application(s). Client applications will be used as a GUI for food ordering.

## Project setup and starting

### Requirements

-   Net Core SDK 3.1.201
-   SQL server
-   Visual Studio 2017 or some other IDE compatible with .Net Core 3.1 projects

### Setup

1. Clone the application `git clone https://github.com/execom-eu/exebite'
2. [Get a Service Account key for Google Drive access](https://github.com/execom-eu/exebite/wiki/Getting-a-Service-Account-key-for-Google-Drive-access.)
3. [Setup Local and Google project](https://github.com/execom-eu/exebite/wiki/Security)
4. [Setup DB and update connection string](<https://github.com/execom-eu/exebite/wiki/Set-up-DB-and-update-connection-string-(for-migration-purpose)>)
5. Run both Exebite.API and Exebite.IdentityServer projects as explained in 3rd step
6. Access http://localhost:50586/swagger to see available endpoints/generate clinets
7. Happy coding :-)

# Current build status

[![Build status](https://dev.azure.com/exebite/exebite/_apis/build/status/exebite-ASP.NET%20Core-CI)](https://dev.azure.com/exebite/exebite/_build/latest?definitionId=6)
