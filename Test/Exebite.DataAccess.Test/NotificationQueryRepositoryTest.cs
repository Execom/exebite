﻿using Exebite.DataAccess.Context;
using Exebite.DataAccess.Entities;
using Exebite.DataAccess.Repositories;
using Exebite.DataAccess.Test.BaseTests;
using Exebite.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using static Exebite.DataAccess.Test.RepositoryTestHelpers;

namespace Exebite.DataAccess.Test
{
    public sealed class NotificationQueryRepositoryTest : QueryRepositoryTests<NotificationQueryRepositoryTest.Data, Notification, NotificationQueryModel>
    {
        protected override IEnumerable<Data> SampleData =>
           Enumerable.Range(1, int.MaxValue).Select(content => new Data
           {
               CustomerId = content
           });

        protected override NotificationQueryModel ConvertEmptyToQuery()
        {
            return new NotificationQueryModel();
        }

        protected override NotificationQueryModel ConvertNullToQuery()
        {
#pragma warning disable RETURN0001 // Do not return null
            return null;
#pragma warning restore RETURN0001 // Do not return null

        }

        protected override NotificationQueryModel ConvertToQuery(Data data)
        {
            return new NotificationQueryModel { CustomerId = data.CustomerId };
        }

        protected override NotificationQueryModel ConvertToQuery(long id)
        {
            return new NotificationQueryModel { CustomerId = 2 };
        }

        protected override NotificationQueryModel ConvertWithPageAndSize(int page, int size)
        {
            return new NotificationQueryModel(page, size);
        }

        protected override IDatabaseQueryRepository<Notification, NotificationQueryModel> CreateSut(IMealOrderingContextFactory factory)
        {
            return CreateOnlyNotificationQueryRepositoryInstance(factory);
        }

        protected override long GetId(Notification result)
        {
            return result.Id;
        }

        protected override void InitializeStorage(IMealOrderingContextFactory factory, int count)
        {
            using (var context = factory.Create())
            {
                var random = new Random();
                var notification = Enumerable.Range(1, count)
                     .Select(x => new NotificationEntity()
                     {
                         Id = x,
                         CustomerId = x,
                         Customer = new CustomerEntity
                         {
                             Id = x,
                             Balance = x,
                             GoogleUserId = (1000 + x).ToString(),
                             DefaultLocationId = (short)x,
                             DefaultLocation = new LocationEntity { Id = (short)x, Address = $"Address {x}", Name = $"Name {x}" },
                             Name = $"Name {x}",
                             Role = random.Next(),
                             IsActive = true
                         },
                         NotificationMessage = "Test message"
                     });
                context.Notification.AddRange(notification);
                context.SaveChanges();
            }
        }

        public sealed class Data
        {
            public long CustomerId { get; set; }
        }
    }
}
