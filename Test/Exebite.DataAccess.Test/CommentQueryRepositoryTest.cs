﻿using System;
using System.Collections.Generic;
using System.Linq;
using Exebite.DataAccess.Context;
using Exebite.DataAccess.Entities;
using Exebite.DataAccess.Repositories;
using Exebite.DataAccess.Repositories.CommentRepository.Model;
using Exebite.DataAccess.Test.BaseTests;
using Exebite.DomainModel;
using Exebite.DomainModel.Enums;
using static Exebite.DataAccess.Test.RepositoryTestHelpers;

namespace Exebite.DataAccess.Test
{
    public sealed class CommentQueryRepositoryTest : QueryRepositoryTests<CommentQueryRepositoryTest.Data, Comment, CommentQueryModel>
    {
        protected override IEnumerable<Data> SampleData =>
            Enumerable.Range(1, int.MaxValue).Select(content => new Data
            {
                MealId = content,
                CustomerId = content,
                SortType = CommentSort.MostRecent
            });

        protected override CommentQueryModel ConvertEmptyToQuery()
        {
            return new CommentQueryModel();
        }

        protected override CommentQueryModel ConvertNullToQuery() => null;

        protected override CommentQueryModel ConvertToQuery(Data data)
        {
            return new CommentQueryModel
            {
                CustomerId = data.CustomerId,
                MealId = data.MealId,
                SortType = data.SortType
            };
        }

        protected override CommentQueryModel ConvertToQuery(long id)
        {
            return new CommentQueryModel
            {
                MealId = id
            };
        }

        protected override CommentQueryModel ConvertWithPageAndSize(int page, int size)
        {
            return new CommentQueryModel(page, size);
        }

        protected override IDatabaseQueryRepository<Comment, CommentQueryModel> CreateSut(IMealOrderingContextFactory factory)
        {
            return CreateCommentQueryRepositoryInstance(factory);
        }

        protected override long GetId(Comment result)
        {
            return result.CommentId;
        }

        protected override void InitializeStorage(IMealOrderingContextFactory factory, int count)
        {
            using (var context = factory.Create())
            {
                context.Location.Add(new LocationEntity
                {
                    Id = 1,
                    Name = "Lorem ipsum"
                });

                context.Restaurant.Add(new RestaurantEntity
                {
                    Id = 1,
                    Name = "Lorem ipsum"
                });

                context.Customer.Add(new CustomerEntity
                {
                    Id = 1,
                    Name = "Lorem ipsum",
                    DefaultLocationId = 1
                });

                context.Meal.Add(new MealEntity
                {
                    Id = 1,
                    Name = "Lorem ipsum",
                    RestaurantId = 1
                });

                var comments = Enumerable.Range(1, count).Select(content => new CommentEntity()
                {
                    MealId = 1,
                    CommentId = content,
                    CustomerId = 1,
                    Text = "Lorem ipsum",
                    Created = DateTime.Now
                });

                context.Comment.AddRange(comments);
                context.SaveChanges();
            }
        }

        public sealed class Data
        {
            public long? MealId { get; set; }

            public long? CustomerId { get; set; }

            public CommentSort SortType { get; set; }
        }
    }
}
