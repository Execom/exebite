﻿using System.Collections.Generic;
using System.Linq;
using Either;
using Exebite.Common;
using Exebite.DataAccess.Context;
using Exebite.DataAccess.Entities;
using Exebite.DataAccess.Repositories;
using Exebite.DataAccess.Repositories.CommentRepository.Model;
using Exebite.DataAccess.Test.BaseTests;
using Optional.Xunit;
using static Exebite.DataAccess.Test.RepositoryTestHelpers;

namespace Exebite.DataAccess.Test
{
    public sealed class CommentCommandRepositoryTest : CommandRepositoryTests<CommentCommandRepositoryTest.Data, long, CommentInsertModel, CommentUpdateModel>
    {
        protected override IEnumerable<Data> SampleData =>
            Enumerable.Range(1, int.MaxValue).Select(content => new Data
            {
                MealId = 1,
                CustomerId = 1,
                Text = $"Text {content}"
            });

        protected override CommentInsertModel ConvertToInput(Data data) =>
            new CommentInsertModel
            {
                MealId = data.MealId,
                CustomerId = data.CustomerId,
                Text = data.Text
            };

        protected override CommentInsertModel ConvertToInvalidInput(Data data) => null;

        protected override CommentUpdateModel ConvertToInvalidUpdate(Data data) => null;

        protected override CommentUpdateModel ConvertToUpdate(Data data) =>
            new CommentUpdateModel
            {
                MealId = data.MealId,
                CustomerId = data.CustomerId,
                Text = data.Text
            };

        protected override IDatabaseCommandRepository<long, CommentInsertModel, CommentUpdateModel> CreateSut(IMealOrderingContextFactory factory) =>
            CreateCommentCommandRepositoryInstance(factory);

        protected override long GetId(Either<Error, long> newObj) => newObj.RightContent();

        protected override long GetUnExistingId() => 0;

        protected override void InitializeStorage(IMealOrderingContextFactory factory, int count)
        {
            using (var context = factory.Create())
            {
                context.Location.Add(new LocationEntity
                {
                    Id = 1,
                    Name = "Lorem ipsum"
                });

                context.Restaurant.Add(new RestaurantEntity
                {
                    Id = 1,
                    Name = "Lorem ipsum"
                });

                context.Customer.Add(new CustomerEntity
                {
                    Id = 1,
                    Name = "Lorem ipsum",
                    DefaultLocationId = 1
                });

                context.Meal.Add(new MealEntity
                {
                    Id = 1,
                    Name = "Lorem ipsum",
                    RestaurantId = 1
                });

                var comments = Enumerable.Range(1, count).Select(content => new CommentEntity()
                {
                    MealId = 1,
                    CommentId = content,
                    CustomerId = 1,
                    Text = "Lorem ipsum",
                });

                context.Comment.AddRange(comments);
                context.SaveChanges();
            }
        }

        public sealed class Data
        {
            public long MealId { get; set; }

            public long CustomerId { get; set; }

            public string Text { get; set; }
        }
    }
}
