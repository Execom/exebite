﻿using System;
using Either;
using Exebite.API.Authorization;
using Exebite.Business;
using Exebite.Common;
using Exebite.DataAccess.Repositories;
using Exebite.DataAccess.Repositories.NotificationRepository;
using Exebite.DtoModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;

namespace Exebite.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Payment")]
    public class PaymentController : ControllerBase
    {
        private readonly IPaymentQueryRepository _queryRepo;
        private readonly IPaymentCommandRepository _commandRepo;
        private readonly IEitherMapper _mapper;
        private readonly ILogger<CustomerController> _logger;
        private readonly IHubContext<NotificationHubService, INotificationHubClientService> _hubContext;
        private readonly INotificationCommandRepository _notificationCommandRepository;

        public PaymentController(
            IPaymentQueryRepository queryRepo,
            IPaymentCommandRepository commandRepo,
            IEitherMapper mapper,
            ILogger<CustomerController> logger,
            IHubContext<NotificationHubService, INotificationHubClientService> hubContext,
            INotificationCommandRepository notificationCommandRepository)
        {
            _queryRepo = queryRepo ?? throw new ArgumentNullException(nameof(queryRepo));
            _commandRepo = commandRepo ?? throw new ArgumentNullException(nameof(commandRepo));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _hubContext = hubContext ?? throw new ArgumentNullException(nameof(hubContext));
            _notificationCommandRepository = notificationCommandRepository ?? throw new ArgumentNullException(nameof(notificationCommandRepository));
        }

        [HttpPost]
        [Authorize(Policy = nameof(AccessPolicy.CreatePaymentAccessPolicy))]
        public IActionResult Post([FromBody] CreatePaymentDto createModel) =>
            _mapper.Map<PaymentInsertModel>(createModel)
                   .Map(_commandRepo.Insert)
                   .Map(id =>
                   {
                       _notificationCommandRepository.CheckUserBalance(id).Map(model =>
                       {
                           if (model?.AmountBalance < 0)
                           {
                               _notificationCommandRepository.Insert(model);
                               _hubContext.Clients.Group(model.GoogleUserId)
                               .ReceiveMessage(_notificationCommandRepository.ReturnNotification(string.Empty, model.AmountBalance.ToString()));
                           }
                           return true;
                       });
                       return id;
                   })
                   .Map(x => Created(new { id = x }))
                   .Reduce(_ => BadRequest(), error => error is ArgumentNotSet)
                   .Reduce(_ => InternalServerError(), x => _logger.LogError(x.ToString()));

        [HttpPut("{id}")]
        [Authorize(Policy = nameof(AccessPolicy.UpdatePaymentAccessPolicy))]
        public IActionResult Put(int id, [FromBody] UpdatePaymentDto model) =>
            _mapper.Map<PaymentUpdateModel>(model)
                   .Map(x => _commandRepo.Update(id, x))
                   .Map(x => AllOk(new { updated = x }))
                   .Reduce(_ => NotFound(), error => error is RecordNotFound)
                   .Reduce(_ => InternalServerError(), x => _logger.LogError(x.ToString()));

        [HttpDelete("{id}")]
        [Authorize(Policy = nameof(AccessPolicy.DeletePaymentAccessPolicy))]
        public IActionResult Delete(int id) =>
            _commandRepo.Delete(id)
                        .Map(_ => OkNoContent())
                        .Reduce(_ => NotFound(), error => error is RecordNotFound)
                        .Reduce(_ => InternalServerError(), x => _logger.LogError(x.ToString()));

        [HttpGet("Query")]
        [Authorize(Policy = nameof(AccessPolicy.ReadPaymentAccessPolicy))]
        public IActionResult Query([FromQuery] PaymentQueryDto query) =>
            _mapper.Map<PaymentQueryModel>(query)
                      .Map(_queryRepo.Query)
                      .Map(_mapper.Map<PagingResult<PaymentDto>>)
                      .Map(AllOk)
                      .Reduce(_ => BadRequest(), error => error is ArgumentNotSet, x => _logger.LogError(x.ToString()))
                      .Reduce(_ => InternalServerError(), x => _logger.LogError(x.ToString()));
    }
}
