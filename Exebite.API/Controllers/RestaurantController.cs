﻿using System;
using Either;
using Exebite.API.Authorization;
using Exebite.Business;
using Exebite.Business.Enums;
using Exebite.Common;
using Exebite.DataAccess.Repositories;
using Exebite.DataAccess.Repositories.NotificationRepository;
using Exebite.DtoModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;

namespace Exebite.API.Controllers
{
    [Produces("application/json")]
    [Route("api/restaurants")]
    public class RestaurantController : ControllerBase
    {
        private readonly IRestaurantQueryRepository _queryRepository;
        private readonly IRestaurantCommandRepository _commandRepository;
        private readonly IEitherMapper _mapper;
        private readonly ILogger<RestaurantController> _logger;
        private readonly IHubContext<NotificationHubService, INotificationHubClientService> _hubContext;
        private readonly INotificationCommandRepository _notificationCommandRepository;

        public RestaurantController(
            IRestaurantQueryRepository queryRepository,
            IRestaurantCommandRepository commandRepository,
            IEitherMapper mapper,
            ILogger<RestaurantController> logger,
            IHubContext<NotificationHubService, INotificationHubClientService> hubContext,
            INotificationCommandRepository notificationCommandRepository)
        {
            _queryRepository = queryRepository ?? throw new ArgumentNullException(nameof(queryRepository));
            _commandRepository = commandRepository ?? throw new ArgumentNullException(nameof(commandRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _hubContext = hubContext ?? throw new ArgumentNullException(nameof(hubContext));
            _notificationCommandRepository = notificationCommandRepository ?? throw new ArgumentNullException(nameof(notificationCommandRepository));
        }

        [HttpPost]
        [Authorize(Policy = nameof(AccessPolicy.CreateRestaurantAccessPolicy))]
        public IActionResult Post([FromBody] CreateRestaurantDto restaurant) =>
             _mapper.Map<RestaurantInsertModel>(restaurant)
                      .Map(_commandRepository.Insert)
                      .Map(id =>
                      {
                          _notificationCommandRepository.InsertRestauranNotificationsAsync(restaurant.Name);
                          _hubContext.Clients.Group(nameof(NameOfHubGroups.HubUsers)).ReceiveMessage(_notificationCommandRepository.ReturnNotification(restaurant.Name, string.Empty));
                          return id;
                      })
                      .Map(x => Created(new { id = x }))
                      .Reduce(_ => BadRequest(), error => error is ArgumentNotSet)
                      .Reduce(_ => InternalServerError(), x => _logger.LogError(x.ToString()));

        [HttpPut("{id}")]
        [Authorize(Policy = nameof(AccessPolicy.UpdateRestaurantAccessPolicy))]
        public IActionResult Put(int id, [FromBody] UpdateRestaurantDto restaurant) =>
            _mapper.Map<RestaurantUpdateModel>(restaurant)
                   .Map(x => _commandRepository.Update(id, x))
                   .Map(x => AllOk(new { updated = x }))
                   .Reduce(_ => NotFound(), error => error is RecordNotFound, x => _logger.LogError(x.ToString()))
                   .Reduce(_ => InternalServerError(), x => _logger.LogError(x.ToString()));

        [HttpDelete("{id}")]
        [Authorize(Policy = nameof(AccessPolicy.DeleteRestaurantAccessPolicy))]
        public IActionResult Delete(int id) =>
            _commandRepository.Delete(id)
                              .Map(_ => (IActionResult)NoContent())
                              .Reduce(_ => NotFound(), error => error is RecordNotFound, x => _logger.LogError(x.ToString()))
                              .Reduce(_ => InternalServerError(), x => _logger.LogError(x.ToString()));

        [HttpGet("Query")]
        [Authorize(Policy = nameof(AccessPolicy.ReadRestaurantAccessPolicy))]
        public IActionResult Query([FromQuery] RestaurantQueryDto query) =>
            _mapper.Map<RestaurantQueryModel>(query)
                   .Map(_queryRepository.Query)
                   .Map(_mapper.Map<PagingResult<RestaurantDto>>)
                   .Map(AllOk)
                   .Reduce(_ => BadRequest(), error => error is ArgumentNotSet, x => _logger.LogError(x.ToString()))
                   .Reduce(_ => InternalServerError(), x => _logger.LogError(x.ToString()));
    }
}
