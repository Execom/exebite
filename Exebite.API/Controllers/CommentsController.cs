﻿using System;
using Either;
using Exebite.API.Authorization;
using Exebite.Common;
using Exebite.DataAccess.Repositories;
using Exebite.DataAccess.Repositories.CommentRepository;
using Exebite.DataAccess.Repositories.CommentRepository.Model;
using Exebite.DtoModels.Comment;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Exebite.API.Controllers
{
    [Produces("application/json")]
    [Route("api/comments")]
    public class CommentsController : ControllerBase
    {
        private readonly IEitherMapper _mapper;
        private readonly ILogger<RestaurantController> _logger;
        private readonly ICommentQueryRepository _queryRepository;
        private readonly ICommentCommandRepository _commandRepository;

        public CommentsController(
            IEitherMapper mapper,
            ILogger<RestaurantController> logger,
            ICommentQueryRepository queryRepository,
            ICommentCommandRepository commandRepository)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _queryRepository = queryRepository ?? throw new ArgumentNullException(nameof(queryRepository));
            _commandRepository = commandRepository ?? throw new ArgumentNullException(nameof(commandRepository));
        }

        [HttpPost]
        [Authorize(Policy = nameof(AccessPolicy.CreateCommentAccessPolicy))]
        public IActionResult Post([FromBody] CreateCommentDto comment) =>
            _mapper.Map<CommentInsertModel>(comment)
                   .Map(_commandRepository.Insert)
                   .Map(x => Created(new { id = x }))
                   .Reduce(_ => UnprocessableEntity(), error => error is ValidationError)
                   .Reduce(_ => InternalServerError(), x => _logger.LogError(x.ToString()));

        [HttpPost("reaction")]
        [Authorize(Policy = nameof(AccessPolicy.CreateCommentReactionAccessPolicy))]
        public IActionResult PostReaction([FromBody] InsertCommentReactionDto commentReaction) =>
            _mapper.Map<CommentInsertReactionModel>(commentReaction)
                   .Map(_commandRepository.InsertReaction)
                   .Map(x => Created(new { saved = x }))
                   .Reduce(_ => BadRequest(), error => error is ValidationError)
                   .Reduce(_ => InternalServerError(), x => _logger.LogError(x.ToString()));

        [HttpDelete("reaction/{id}")]
        [Authorize(Policy = nameof(AccessPolicy.DeleteCommentReactionAccessPolicy))]
        public IActionResult DeleteReaction(long id, [FromBody] DeleteCommentReactionDto commentReaction) =>
            _mapper.Map<CommentDeleteReactionModel>(commentReaction)
                   .Map(model => _commandRepository.DeleteReaction(id, model))
                   .Map(_ => OkNoContent())
                   .Reduce(_ => NotFound(), error => error is RecordNotFound)
                   .Reduce(_ => InternalServerError(), x => _logger.LogError(x.ToString()));

        [HttpPut("{id}")]
        [Authorize(Policy = nameof(AccessPolicy.UpdateCommentAccessPolicy))]
        public IActionResult Put(int id, [FromBody] UpdateCommentDto comment) =>
            _mapper.Map<CommentUpdateModel>(comment)
                   .Map(model => _commandRepository.Update(id, model))
                   .Map(x => AllOk(new { updated = x }))
                   .Reduce(_ => NotFound(), error => error is RecordNotFound, x => _logger.LogError(x.ToString()))
                   .Reduce(_ => InternalServerError(), x => _logger.LogError(x.ToString()));

        [HttpDelete("{id}")]
        [Authorize(Policy = nameof(AccessPolicy.DeleteMealAccessPolicy))]
        public IActionResult Delete(int id, [FromBody] DeleteCommentDto comment) =>
            _commandRepository.Delete(id, comment)
                        .Map(_ => OkNoContent())
                        .Reduce(_ => NotFound(), error => error is RecordNotFound)
                        .Reduce(_ => InternalServerError(), x => _logger.LogError(x.ToString()));

        [HttpGet("Query")]
        [Authorize(Policy = nameof(AccessPolicy.ReadCommentAccessPolicy))]
        public IActionResult Query([FromQuery] CommentQueryDto query) =>
            _mapper.Map<CommentQueryModel>(query)
                   .Map(_queryRepository.Query)
                   .Map(_mapper.Map<PagingResult<CommentDto>>)
                   .Map(AllOk)
                   .Reduce(_ => BadRequest(), error => error is ArgumentNotSet, x => _logger.LogError(x.ToString()))
                   .Reduce(_ => InternalServerError(), x => _logger.LogError(x.ToString()));
    }
}
