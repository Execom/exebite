﻿using Either;
using Exebite.API.Authorization;
using Exebite.Business;
using Exebite.Business.Model;
using Exebite.Common;
using Exebite.DataAccess.Repositories;
using Exebite.DataAccess.Repositories.OrderRepository.Model;
using Exebite.DtoModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;

namespace Exebite.API.Controllers
{
    [Produces("application/json")]
    [Route("api/orders")]
    public class OrdersController : ControllerBase
    {
        private readonly IOrderQueryRepository _queryRepo;
        private readonly IOrderCommandRepository _commandRepo;
        private readonly IEitherMapper _mapper;
        private readonly IRestaurantService _restaurantService;
        private readonly ILogger<OrdersController> _logger;

        public OrdersController(
            IOrderQueryRepository queryRepo,
            IOrderCommandRepository commandRepo,
            IEitherMapper mapper,
            IRestaurantService restaurantService,
            ILogger<OrdersController> logger)
        {
            _queryRepo = queryRepo ?? throw new ArgumentNullException(nameof(queryRepo));
            _commandRepo = commandRepo ?? throw new ArgumentNullException(nameof(commandRepo));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _restaurantService = restaurantService ?? throw new ArgumentNullException(nameof(restaurantService));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpPost]
        [Authorize(Policy = nameof(AccessPolicy.CreateOrdersAccessPolicy))]
        public IActionResult Post([FromBody] CreateOrderDto model) =>
            _mapper.Map<RestaurantOrder>(model)
                    .Map(_restaurantService.PlaceOrdersForRestaurant)
                    .Map(_mapper.Map<OrderInsertModel>)
                    .Map(_commandRepo.Insert)
                    .Map(x => Created(new { id = x }))
                    .Reduce(_ => BadRequest(), error => error is ArgumentNotSet)
                    .Reduce(_ => InternalServerError(), x => _logger.LogError(x.ToString()));

        [HttpGet("query")]
        [Authorize(Policy = nameof(AccessPolicy.ReadOrdersAccessPolicy))]
        public IActionResult Query([FromQuery] GetOrdersDto query) =>
            _mapper.Map<GetOrdersModel>(query)
                   .Map(_queryRepo.GetOrders)
                   .Map(_mapper.Map<PagingResult<OrderDto>>)
                   .Map(x => AllOk(x))
                   .Reduce(_ => BadRequest(), error => error is ArgumentNotSet, x => _logger.LogError(x.ToString()))
                   .Reduce(_ => InternalServerError(), x => _logger.LogError(x.ToString()));
    }
}