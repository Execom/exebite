﻿using System;
using Either;
using Exebite.API.Authorization;
using Exebite.Common;
using Exebite.DataAccess.Repositories;
using Exebite.DataAccess.Repositories.NotificationRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Exebite.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Notifications")]
    public class NotificationsController : ControllerBase
    {
        private readonly INotificationCommandRepository _commandRepository;
        private readonly INotificationQueryRepository _queryRepository;
        private readonly IEitherMapper _mapper;
        private readonly ILogger<NotificationsController> _logger;

        public NotificationsController(
            IEitherMapper mapper,
            ILogger<NotificationsController> logger,
            INotificationQueryRepository queryRepository,
            INotificationCommandRepository commandRepository)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _queryRepository = queryRepository ?? throw new ArgumentNullException(nameof(queryRepository));
            _commandRepository = commandRepository ?? throw new ArgumentNullException(nameof(commandRepository));
        }

        [HttpGet("NumberOfNewNotifications")]
        [Authorize(Policy = nameof(AccessPolicy.ReadNotificationAccessPolicy))]
        public IActionResult NumberOfNewNotifications(long customerId) =>
            _mapper.Map<long>(customerId)
                  .Map(_queryRepository.ReturnNumberOfNewNotifications)
                  .Map(AllOk)
                  .Reduce(_ => BadRequest(), error => error is ArgumentNotSet, x => _logger.LogError(x.ToString()))
                  .Reduce(_ => InternalServerError(), x => _logger.LogError(x.ToString()));

        [HttpGet("Query")]
        [Authorize(Policy = nameof(AccessPolicy.ReadNotificationAccessPolicy))]
        public IActionResult Query([FromQuery] NotificationQueryModel query) =>
           _mapper.Map<NotificationQueryModel>(query)
                  .Map(_queryRepository.Query)
                  .Map(AllOk)
                  .Reduce(_ => BadRequest(), error => error is ArgumentNotSet, x => _logger.LogError(x.ToString()))
                  .Reduce(_ => InternalServerError(), x => _logger.LogError(x.ToString()));

        [HttpDelete("{customerId}")]
        [Authorize(Policy = nameof(AccessPolicy.DeleteNotificationAccessPolicy))]
        public IActionResult Delete(long customerId) =>
            _commandRepository.Delete(customerId)
                              .Map(_ => (IActionResult)NoContent())
                              .Reduce(_ => NotFound(), error => error is RecordNotFound, x => _logger.LogError(x.ToString()))
                              .Reduce(_ => InternalServerError(), x => _logger.LogError(x.ToString()));
    }
}
