﻿using System;
using Either;
using Exebite.API.Authorization;
using Exebite.Common;
using Exebite.DataAccess.Repositories;
using Exebite.DomainModel;
using Exebite.DtoModels;
using Exebite.DtoModels.Meal;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Exebite.API.Controllers
{
    [Produces("application/json")]
    [Route("api/meals")]
    public class MealController : ControllerBase
    {
        private readonly IMealQueryRepository _queryRepo;
        private readonly IMealCommandRepository _commandRepo;
        private readonly IEitherMapper _mapper;
        private readonly ILogger<MealController> _logger;

        public MealController(
            IMealQueryRepository queryRepo,
            IMealCommandRepository commandRepo,
            IEitherMapper mapper,
            ILogger<MealController> logger)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _queryRepo = queryRepo ?? throw new ArgumentNullException(nameof(queryRepo));
            _commandRepo = commandRepo ?? throw new ArgumentNullException(nameof(commandRepo));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpPost]
        [Authorize(Policy = nameof(AccessPolicy.CreateMealAccessPolicy))]
        public IActionResult Post([FromBody] CreateMealDto model) =>
            _mapper.Map<MealInsertModel>(model)
                   .Map(_commandRepo.Insert)
                   .Map(x => Created(new { id = x }))
                   .Reduce(_ => NotFound(), error => error is RecordNotFound)
                   .Reduce(_ => InternalServerError(), x => _logger.LogError(x.ToString()));

        [HttpPost("{id}/reaction")]
        [Authorize(Policy = nameof(AccessPolicy.CreateReactionToMealAccessPolicy))]
        public IActionResult PostReaction(long id, [FromBody] CreateReactionToMealDto model) =>
            _mapper.Map<ReactionToMealInsertModel>(model)
                   .Map(_commandRepo.InsertReaction)
                   .Map(x => AllOk(new { saved = x }))
                   .Reduce(_ => NotFound(), error => error is RecordNotFound)
                   .Reduce(_ => InternalServerError(), x => _logger.LogError(x.ToString()));

        [HttpPut("{id}/reaction/{reaction}")]
        [Authorize(Policy = nameof(AccessPolicy.UpdateReactionToMealAccessPolicy))]
        public IActionResult PutReaction(long id, ReactionType reaction, [FromBody] UpdateReactionToMealDto customerReactionToMeal) =>
            _mapper.Map<ReactionToMealUpdateModel>(customerReactionToMeal)
                   .Map(model => _commandRepo.UpdateReaction(model))
                   .Map(x => AllOk(new { updated = x }))
                   .Reduce(_ => NotFound(), error => error is RecordNotFound, x => _logger.LogError(x.ToString()))
                   .Reduce(_ => InternalServerError(), x => _logger.LogError(x.ToString()));

        [HttpPut("{id}")]
        [Authorize(Policy = nameof(AccessPolicy.UpdateMealAccessPolicy))]
        public IActionResult Put(int id, [FromBody] UpdateMealDto model) =>
            _mapper.Map<MealUpdateModel>(model)
                   .Map(x => _commandRepo.Update(id, x))
                   .Map(x => AllOk(new { updated = x }))
                   .Reduce(_ => NotFound(), error => error is RecordNotFound)
                   .Reduce(_ => InternalServerError(), x => _logger.LogError(x.ToString()));

        [HttpDelete("{id}")]
        [Authorize(Policy = nameof(AccessPolicy.DeleteMealAccessPolicy))]
        public IActionResult Delete(int id) =>
            _commandRepo.Delete(id)
                        .Map(_ => OkNoContent())
                        .Reduce(_ => NotFound(), error => error is RecordNotFound)
                        .Reduce(_ => InternalServerError(), x => _logger.LogError(x.ToString()));

        [HttpGet("Query")]
        [Authorize(Policy = nameof(AccessPolicy.ReadMealAccessPolicy))]
        public IActionResult Query([FromQuery] MealQueryDto query) =>
            _mapper.Map<MealQueryModel>(query)
                   .Map(_queryRepo.Query)
                   .Map(_mapper.Map<PagingResult<MealDto>>)
                   .Map(AllOk)
                   .Reduce(_ => BadRequest(), error => error is ArgumentNotSet, x => _logger.LogError(x.ToString()))
                   .Reduce(_ => InternalServerError(), x => _logger.LogError(x.ToString()));

        [HttpGet("GetMealsGroupByType")]
        [Authorize(Policy = nameof(AccessPolicy.ReadMealAccessPolicy))]
        public IActionResult GetMealsGroupByType([FromQuery] MealByTypeDto query) =>
            _mapper.Map<MealGroupByTypeModel>(query)
                   .Map(_queryRepo.GetMealsByType)
                   .Map(_mapper.Map<PagingResult<MealGroupByTypeDto>>)
                   .Map(AllOk)
                   .Reduce(_ => BadRequest(), error => error is ArgumentNotSet, x => _logger.LogError(x.ToString()))
                   .Reduce(_ => InternalServerError(), x => _logger.LogError(x.ToString()));

        [HttpGet("GetMealsByRestaurantId")]
        [Authorize(Policy = nameof(AccessPolicy.ReadMealAccessPolicy))]
        public IActionResult GetMealsByRestaurantId([FromQuery] MealByRestaurantDto query) =>
            _mapper.Map<MealByRestaurantModel>(query)
                   .Map(_queryRepo.GetMealsByRestaurantId)
                   .Map(_mapper.Map<PagingResult<MealDto>>)
                   .Map(AllOk)
                   .Reduce(_ => BadRequest(), error => error is ArgumentNotSet, x => _logger.LogError(x.ToString()))
                   .Reduce(_ => InternalServerError(), x => _logger.LogError(x.ToString()));

        [HttpGet("GetSoupsAndSideDishesByRestaurantId")]
        [Authorize(Policy = nameof(AccessPolicy.ReadMealAccessPolicy))]
        public IActionResult GetSoupsAndSideDishesByRestaurantId([FromQuery] MealByRestaurantDto query) =>
            _mapper.Map<MealByRestaurantModel>(query)
                   .Map(_queryRepo.GetSoupsAndSideDishesByRestaurantId)
                   .Map(_mapper.Map<PagingResult<MealDto>>)
                   .Map(AllOk)
                   .Reduce(_ => BadRequest(), error => error is ArgumentNotSet, x => _logger.LogError(x.ToString()))
                   .Reduce(_ => InternalServerError(), x => _logger.LogError(x.ToString()));
    }
}
