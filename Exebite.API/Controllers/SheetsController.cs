﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Exebite.GoogleSheetAPI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Exebite.API.Controllers
{
    [Produces("application/json")]
    [Route("/api/sheets")]
    public class SheetsController : ControllerBase
    {
        private readonly ILogger<SheetsController> _logger;
        private readonly IGoogleSheetAPIService _apiService;
        private readonly List<Task> tasks = new List<Task>();

        public SheetsController(
            ILogger<SheetsController> logger,
            IGoogleSheetAPIService apiService)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _apiService = apiService ?? throw new ArgumentNullException(nameof(apiService));
        }

        [HttpGet("fetch")]
        public JsonResult FetchData()
        {
            try
            {
                _apiService.UpdateRestaurants();

                tasks.Add(Task.Factory.StartNew(() => _apiService.UpdateCustomers()));
                UpdateDailyMenus();
                UpdateMainMenus();

                Task.WaitAll(tasks.ToArray());

                _logger.LogInformation("Successfully fetched and updated DB information from Google Sheets.");

                return new JsonResult(new
                {
                    success = true,
                });
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);

                return new JsonResult(new
                {
                    success = false,
                    message = "Error occurred trying to fetch the data"
                });
            }
        }

        private void UpdateMainMenus()
        {
            tasks.Add(Task.Factory.StartNew(() => _apiService.UpdateMainMenuIndex()));
            tasks.Add(Task.Factory.StartNew(() => _apiService.UpdateMainMenuParrilla()));
            tasks.Add(Task.Factory.StartNew(() => _apiService.UpdateMainMenuHeyDay()));
        }

        private void UpdateDailyMenus()
        {
            tasks.Add(Task.Factory.StartNew(() => _apiService.UpdateDailyMenuLipa()));
            tasks.Add(Task.Factory.StartNew(() => _apiService.UpdateDailyMenuTopliObrok()));
            tasks.Add(Task.Factory.StartNew(() => _apiService.UpdateDailyMenuParrilla()));
            tasks.Add(Task.Factory.StartNew(() => _apiService.UpdateDailyMenuSerpica()));
            tasks.Add(Task.Factory.StartNew(() => _apiService.UpdateDailyMenuMimas()));
        }
    }
}