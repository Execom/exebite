﻿using System;
using System.Text;
using Google.Apis.Sheets.v4.Data;

namespace Exebite.GoogleSheetAPI.Common
{
    /// <summary>
    /// This class represents a merged region in the Google Sheet
    /// Since we need more information that regular GridRange provides
    /// This is used as a wrapper around it.
    /// </summary>
    public class MergedRegion
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MergedRegion"/> class.
        /// Default constructor for the class.
        /// </summary>
        public MergedRegion(Sheet sheet, GridRange range)
        {
            _sheet = sheet ?? throw new ArgumentNullException(nameof(sheet));
            Range = range ?? throw new ArgumentNullException(nameof(range));

            A1FirstCell = CalculateFirstCellA1();
        }

        #region private members

        private readonly Sheet _sheet;

        #endregion

        #region Public properties

        /// <summary>
        /// Gets gridRange of the Merged region.
        /// </summary>
        public GridRange Range { get; private set; }

        /// <summary>
        /// Gets a1 Notation of the first cell location.
        /// </summary>
        public string A1FirstCell { get; private set; }

        /// <summary>
        /// Gets name of the sheet. Used to construct queries.
        /// We don't need to expose entire sheet.
        /// </summary>
        public string SheetName
        {
            get
            {
                return _sheet.Properties.Title;
            }
        }
        #endregion

        #region Private methods

        /// <summary>
        /// Calculates A1 Notation for the first cell.
        /// </summary>
        private string CalculateFirstCellA1()
        {
            var sb = new StringBuilder();
            sb.Append("'");
            sb.Append(_sheet.Properties.Title);
            sb.Append("'");
            sb.Append("!");
            sb.Append(A1Notation.ToCellFormat(
                    Range.StartColumnIndex.Value,
                    Range.StartRowIndex.Value));

            return sb.ToString();
        }

        #endregion
    }
}
