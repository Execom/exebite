﻿using System;
using System.Collections.Generic;
using System.Linq;
using Either;
using Exebite.Common.Constants;
using Exebite.DataAccess.Repositories;
using Exebite.DomainModel;
using Exebite.GoogleSheetAPI.GoogleSSFactory;
using Exebite.GoogleSheetAPI.SheetExtractor;
using Google.Apis.Sheets.v4.Data;

namespace Exebite.GoogleSheetAPI.Connectors.Kasa
{
    public sealed class KasaConnector : IKasaConnector
    {
        private const string _infoRange = "!A2:D";
        private const string _defaultStringCellValue = "Not found";
        private const decimal _defaultDecimalCellValue = 0m;
        private const string _mmLocationName = "MM";
        private const string _bvsLocationName = "BVS";
        private readonly IGoogleSheetExtractor _googleSheetExtractor;
        private readonly string _sheetId;
        private readonly string _range;

        private readonly ILocationQueryRepository _locationQueryRepository;

        public KasaConnector(
            IGoogleSheetExtractor googleSheetExtractor,
            IGoogleSpreadsheetIdFactory googleSheetIdFactory,
            ILocationQueryRepository locationQueryRepository)
        {
            _googleSheetExtractor = googleSheetExtractor ?? throw new ArgumentNullException(nameof(googleSheetExtractor));
            _sheetId = googleSheetIdFactory.GetSheetId(Enums.ESheetOwner.KASA);
            _range = _infoRange;
            _locationQueryRepository = locationQueryRepository ?? throw new ArgumentNullException(nameof(locationQueryRepository));
        }

        public IEnumerable<Customer> GetAllCustomers()
        {
            var locations = _locationQueryRepository
               .Query(new LocationQueryModel())
               .Map(x => x.Items)
               .Reduce(x => new List<Location>());

            return _googleSheetExtractor
                 .GetRowsWithAdditionalInfo(_sheetId, _range)
                 .Select(row =>
                 {
                     var nameCellColor = row.Values[0].EffectiveFormat.BackgroundColor;

                     return new Customer()
                     {
                         Name = _googleSheetExtractor.ExtractCell(row.Values, 0, _defaultStringCellValue),
                         Role = 1,
                         IsActive = true,
                         DefaultLocation = GetLocationByColor(locations, nameCellColor),
                         GoogleUserId = _googleSheetExtractor.ExtractCell(row.Values, 1, _defaultStringCellValue),
                         Balance = _googleSheetExtractor.ExtractCell(row.Values, 3, _defaultDecimalCellValue),
                     };
                 })
                 .Where(c => !string.IsNullOrWhiteSpace(c.GoogleUserId))
                 .OrderBy(c => c.GoogleUserId);
        }

        private Location GetLocationByColor(IEnumerable<Location> locations, Color color)
        {
            return color.Green.HasValue && !color.Red.HasValue && !color.Blue.HasValue ?
                   locations.SingleOrDefault(l => l.Name == _mmLocationName) :
                   locations.SingleOrDefault(l => l.Name == _bvsLocationName);
        }
    }
}