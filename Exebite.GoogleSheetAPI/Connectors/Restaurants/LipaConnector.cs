﻿using Either;
using Exebite.Common;
using Exebite.DataAccess.Repositories;
using Exebite.DomainModel;
using Exebite.GoogleSheetAPI.Connectors.Restaurants.Base;
using Exebite.GoogleSheetAPI.GoogleSSFactory;
using Exebite.GoogleSheetAPI.SheetExtractor;
using System;
using System.Collections.Generic;

namespace Exebite.GoogleSheetAPI.Connectors.Restaurants
{
    public sealed class LipaConnector : RestaurantConnector, ILipaConnector
    {
        private const string _dailyMenuRange = "!1:4";
        private const string _orderDueRange = "A5";
        private const int _dateIndex = 1;

        public LipaConnector(
            IGoogleSheetExtractor googleSheetService,
            IGoogleSpreadsheetIdFactory googleSSIdFactory,
            IRestaurantQueryRepository restaurantQueryRepository,
            IRestaurantCommandRepository restaurantCommandRepository)
            : base(googleSheetService, restaurantQueryRepository, restaurantCommandRepository, RestaurantConstants.POD_LIPOM_NAME)
        {
            SheetId = googleSSIdFactory.GetSheetId(Enums.ESheetOwner.LIPA);
            ColumnsPerDay = 10;
            DailyMenuSheet = $"{GetLocalMonthName(DailyMenuDate.Month - 1)}{DailyMenuDate.Year}";
        }

        /// <summary>
        /// Write menu with all meals in DB to sheet. Used for initial writing of old menu
        /// </summary>
        /// <param name="meals">List of all meals to be written</param>
        public override void WriteMenu(List<Meal> meals)
        {
            // not needed for now. But will probably be needed in the future to write orders
            // in sheets until everything is moved to be get from DB (reports, orders...)
        }

        /// <summary>
        /// Gets food available for today
        /// </summary>
        /// <returns>List of foods</returns>
        public Either<Error, IEnumerable<Meal>> GetDailyMenu()
        {
            try
            {
                return new Right<Error, IEnumerable<Meal>>(GetDailyMenu(_dailyMenuRange, _dateIndex));
            }
            catch (Exception e)
            {
                return new Left<Error, IEnumerable<Meal>>(new ValidationError(e.Message));
            }
        }

        public Either<Error, bool> UpdateRestaurant()
        {
            return UpdateRestaurant(_orderDueRange, SheetId, Restaurant.Id);
        }
    }
}
