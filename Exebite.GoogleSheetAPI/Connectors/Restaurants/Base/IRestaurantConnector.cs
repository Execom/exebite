﻿using System.Collections.Generic;
using Exebite.DomainModel;

namespace Exebite.GoogleSheetAPI.Connectors.Restaurants.Base
{
    public interface IRestaurantConnector
    {
        void WriteMenu(List<Meal> foods);

        /// <summary>
        /// Write selected meals to google sheet
        /// </summary>
        /// <param name="customerName">Customer that made order</param>
        /// <param name="locationName">Location of customer</param>
        /// <param name="meals">Meals in order</param>
        /// <param name="dateRowIndex">Index of a date row inside sheet</param>
        void WriteDailyOrder(string customerName, string locationName, ICollection<Meal> meals, int dateRowIndex);
    }
}
