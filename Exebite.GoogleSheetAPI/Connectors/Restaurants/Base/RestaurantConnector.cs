﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Either;
using Exebite.Common;
using Exebite.Common.Constants;
using Exebite.Common.Extensions;
using Exebite.DataAccess.Repositories;
using Exebite.DomainModel;
using Exebite.GoogleSheetAPI.Common;
using Exebite.GoogleSheetAPI.SheetExtractor;
using Google.Apis.Sheets.v4.Data;

namespace Exebite.GoogleSheetAPI.Connectors.Restaurants.Base
{
    public abstract class RestaurantConnector : IRestaurantConnector
    {
        private const string _orderMark = "x";
        private const short _validRowInfoCount = 4;
        private const string _sheetCultureInfo = "sr-Latn-CS";

        private readonly IRestaurantQueryRepository _restaurantQueryRepository;
        private readonly IRestaurantCommandRepository _restaurantCommandRepository;

        protected RestaurantConnector(
            IGoogleSheetExtractor googleSheetExtractor,
            IRestaurantQueryRepository restaurantQueryRepository,
            IRestaurantCommandRepository restaurantCommandRepository,
            string restaurantName)
        {
            _restaurantQueryRepository = restaurantQueryRepository ?? throw new ArgumentNullException(nameof(restaurantQueryRepository));
            _restaurantCommandRepository = restaurantCommandRepository ?? throw new ArgumentNullException(nameof(restaurantCommandRepository));
            GoogleSheetExtractor = googleSheetExtractor ?? throw new ArgumentNullException(nameof(googleSheetExtractor));
            Restaurant = GetRestaurant(restaurantName);
            DailyMenuDate = DateTime.Today;
        }

        internal IGoogleSheetExtractor GoogleSheetExtractor { get; set; }

        internal string SheetId { get; set; }

        internal string DailyMenuSheet { get; set; }

        internal int ColumnsPerDay { get; set; }

        internal DateTime DailyMenuDate { get; }

        internal Restaurant Restaurant { get; set; }

        public abstract void WriteMenu(List<Meal> foods);

        public void WriteDailyOrder(string customerName, string locationName, ICollection<Meal> meals, int dateRowIndex)
        {
            var valueRange = GoogleSheetExtractor.GetRows(SheetId, DailyMenuSheet);

            var rowValues = valueRange.Values;
            var mealRow = rowValues[0];
            int startDateIndex = GetStartDateColumnIndex(rowValues, dateRowIndex);
            int endDateIndex = startDateIndex + ColumnsPerDay;

            object[] mealsData = new object[ColumnsPerDay];

            for (int mealIndex = startDateIndex; mealIndex < endDateIndex && mealIndex < mealRow.Count; mealIndex++)
            {
                mealsData[mealIndex - startDateIndex] = meals.Any(m => m.Name == mealRow[mealIndex].ToString()) ? _orderMark : string.Empty;
            }

            var mealsBody = new ValueRange()
            {
                Values = new List<IList<object>>() { mealsData }
            };

            int rowIndex = GetCustomerRowIndex(rowValues, customerName);

            GoogleSheetExtractor.Update(
                mealsBody,
                SheetId,
                $"{DailyMenuSheet}!{A1Notation.ToCellFormat(startDateIndex, rowIndex)}:{A1Notation.ToCellFormat(endDateIndex, rowIndex)}");

            var locationBody = new ValueRange()
            {
                Values = new List<IList<object>>() { new object[1] { locationName.ToUpper() } }
            };

            GoogleSheetExtractor.Update(
                locationBody,
                SheetId,
                $"{DailyMenuSheet}!{A1Notation.ToCellFormat(2, rowIndex)}");
        }

        /// <summary>
        /// Translate month number into the local month name.
        /// </summary>
        /// <param name="month">Month number</param>
        /// <returns>If correct month number is sent returns local month name, otherwise returns 'Not existing month'.</returns>
        internal static string GetLocalMonthName(int month)
        {
            if (month >= 0 && month < 12)
            {
                CultureInfo cultureInfo = new CultureInfo(_sheetCultureInfo);
                DateTimeFormatInfo dateTimeFormat = cultureInfo.DateTimeFormat;

                return dateTimeFormat.MonthGenitiveNames[month].Capitalize();
            }

            return string.Empty;
        }

        /// <summary>
        /// Get meals from daily menu
        /// </summary>
        /// <param name="range">Range of rows you want to read</param>
        /// <param name="dateIndex">Index of date parameter inside row value</param>
        /// <returns>List of meals</returns>
        internal IEnumerable<Meal> GetDailyMenu(string range, int dateIndex)
        {
            List<Meal> meals = new List<Meal>();
            var dailyMenuInfo = GoogleSheetExtractor.GetColumns(SheetId, range).Values
                                                    .SkipWhile(r => IsItRowWithCurrentDate(r, dateIndex))
                                                    .Take(ColumnsPerDay).ToList();

            foreach (var row in dailyMenuInfo)
            {
                var mealName = row.Count > 0 ? row[0].ToString() : string.Empty;
                if (row.Count == _validRowInfoCount && !string.IsNullOrEmpty(mealName))
                {
                    var rowDate = row[1].ToString();
                    var rowPrice = ParsePrice(row[3].ToString(), false);
                    meals.Add(new Meal { Name = mealName, Price = rowPrice, Restaurant = Restaurant, Type = (int)MealType.MAIN_COURSE, IsActive = true });
                }
            }

            return meals;
        }

        /// <summary>
        /// Parse price from sheet, if price has currency, set flag to remove it
        /// </summary>
        /// <param name="priceString">Price in string format</param>
        /// <param name="substractCurrency">Flag that is used to remove currency</param>
        /// <returns>Decimal value of a price</returns>
        internal decimal ParsePrice(string priceString, bool substractCurrency)
        {
            if (substractCurrency)
            {
                var priceWithoutCurrencyLength = priceString.Length - 4;
                priceString = priceString.Substring(0, priceWithoutCurrencyLength);
            }

            if (!decimal.TryParse(priceString, out decimal price))
            {
                throw new InvalidOperationException($"Error while parsing {nameof(price)}");
            }

            return price;
        }

        /// <summary>
        /// Get restaurant info from sheet and update restaurant
        /// </summary>
        /// <param name="orderDueRange">Range that is used to read orderDue time inside sheet</param>
        /// <param name="sheetId">Id of used sheet</param>
        /// <param name="restaurantId">Id of used restaurant</param>
        /// <returns>Flag that states if restaurant is updated successfully</returns>
        internal Either<Error, bool> UpdateRestaurant(string orderDueRange, string sheetId, long restaurantId)
        {
            var orderDueString = GoogleSheetExtractor.ReadOneValue(orderDueRange, sheetId);
            var orderDueTime = GetOrderDueTime(orderDueString.Value.ToString());

            if (!orderDueTime.HasValue)
            {
                return new Left<Error, bool>(new ArgumentNotSet(nameof(orderDueTime)));
            }

            UpdateRestaurant(restaurantId, orderDueTime.Value, sheetId);

            return new Right<Error, bool>(true);
        }

        /// <summary>
        /// Update restaurant with provided info
        /// </summary>
        /// <param name="restaurantId">Restaurant id</param>
        /// <param name="orderDue">Restaurants accept orders due this time</param>
        /// <param name="sheetId">Used sheet id</param>
        internal void UpdateRestaurant(long restaurantId, DateTime orderDue, string sheetId)
        {
            var restaurant = _restaurantQueryRepository.Query(new RestaurantQueryModel { Id = restaurantId })
                                                       .Map(res => res.Items.FirstOrDefault())
                                                       .Reduce(r => null);

            if (restaurant == null)
            {
                return;
            }

            _restaurantCommandRepository.Update(restaurantId, new RestaurantUpdateModel { SheetId = sheetId, Name = Restaurant.Name, OrderDue = orderDue, IsActive = Restaurant.IsActive });
        }

        private bool IsItRowWithCurrentDate(IList<object> row, int dateIndex)
        {
            var rowDate = row.Count == _validRowInfoCount ? row[dateIndex].ToString() : string.Empty;
            if (!string.IsNullOrEmpty(rowDate))
            {
                rowDate = rowDate.Split(CharacterConstants.WHITESPACE_CHAR).FirstOrDefault();
                DateTime.TryParseExact(rowDate, DateTimeConstants.SHORT_DATE_FORMAT, CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime dailyMenuDate);

                return dailyMenuDate.Date != DateTime.Now.Date;
            }

            return true;
        }

        private DateTime? GetOrderDueTime(string orderDue)
        {
            var timeFormatLength = 5;
            orderDue = string.Concat(orderDue.Where(x => x == CharacterConstants.DOT_CHAR || char.IsDigit(x) || x == CharacterConstants.SEMICOLON_CHAR))
                             .Substring(0, timeFormatLength);

            var hourFormat = orderDue.Contains(CharacterConstants.DOT_CHAR)
                                     ? DateTimeConstants.DOT_HOUR_FORMAT
                                     : DateTimeConstants.SEMICOLON_HOUR_FORMAT;
            return DateTime.TryParseExact(orderDue, hourFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime time)
                                              ? (DateTime?)time
                                              : null;
        }

        private Restaurant GetRestaurant(string name)
        {
            var restaurant = _restaurantQueryRepository
                    .Query(new RestaurantQueryModel { Name = name })
                    .Map(res => res.Items.FirstOrDefault())
                    .Reduce(r => null);

            if (restaurant == null)
            {
                var restaurantId = _restaurantCommandRepository.
                    Insert(new RestaurantInsertModel { Name = name, IsActive = true })
                    .Map(id => id)
                    .Reduce(r => 0);

                restaurant = _restaurantQueryRepository
                    .Query(new RestaurantQueryModel { Id = restaurantId })
                    .Map(res => res.Items.FirstOrDefault())
                    .Reduce(r => null);
            }

            return restaurant;
        }

        private int GetCustomerRowIndex(IList<IList<object>> values, string customerName)
        {
            var firstCustomerIndex = 6;
            var index = values.Skip(firstCustomerIndex)
                              .Select((list, i) => new { list, index = i })
                              .FirstOrDefault(row => row.list[1].ToString() == customerName)?.index;

            if (index.HasValue)
            {
                return index.Value + firstCustomerIndex;
            }

            throw new Exception("Customer not found!");
        }

        private int GetStartDateColumnIndex(IList<IList<object>> values, int dateRowIndex)
        {
            var dateRow = values[dateRowIndex];
            var index = dateRow.Select((cell, i) => new { cell, index = i })
                               .FirstOrDefault(row =>
                               {
                                   var date = row.cell.ToString().Split(CharacterConstants.WHITESPACE_CHAR).FirstOrDefault();
                                   if (DateTime.TryParseExact(date, DateTimeConstants.SHORT_DATE_FORMAT, CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime dateCell))
                                   {
                                       return DailyMenuDate.Date == dateCell.Date;
                                   }
                                   return false;
                               })?.index;

            if (index.HasValue)
            {
                return index.Value;
            }

            throw new Exception("Date not found!");
        }
    }
}
