﻿using Exebite.Common;
using Exebite.Common.Constants;
using Exebite.DataAccess.Repositories;
using Exebite.DomainModel;
using Exebite.GoogleSheetAPI.Connectors.Restaurants.Base;
using Exebite.GoogleSheetAPI.GoogleSSFactory;
using Exebite.GoogleSheetAPI.SheetExtractor;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Exebite.GoogleSheetAPI.Connectors.Restaurants
{
    public sealed class SerpicaConnector : RestaurantConnector, ISerpicaConnector
    {
        private const string _phoneNumberText = "brtelefona";
        private const string _passwordText = "sifra";
        private const string _rememberMeText = "zapamtime";
        private const string _dateText = "datum";

        public SerpicaConnector(
            IGoogleSheetExtractor googleSheetService,
            IGoogleSpreadsheetIdFactory googleSSIdFactory,
            IRestaurantQueryRepository restaurantQueryRepository,
            IRestaurantCommandRepository restaurantCommandRepository)
            : base(googleSheetService, restaurantQueryRepository, restaurantCommandRepository, RestaurantConstants.SERPICA_NAME)
        {
            SheetId = googleSSIdFactory.GetSheetId(Enums.ESheetOwner.SERPICA);
            ColumnsPerDay = 1;
        }

        public override void WriteMenu(List<Meal> foods)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Meal> GetDailyMenu()
        {
            return GetDailyFromUrlAsync().Result;
        }

        public async Task<IEnumerable<Meal>> GetDailyFromUrlAsync()
        {
            string dailyMenuString = string.Empty;
            List<Meal> dailyMenu = new List<Meal>();

            var loginContent = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>(_phoneNumberText, Properties.Resources.SERPICA_USERNAME),
                new KeyValuePair<string, string>(_passwordText, Properties.Resources.SERPICA_PASSWORD),
                new KeyValuePair<string, string>(_rememberMeText, "false")
            });

            var dailyMenuContent = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>(_dateText, DateTime.Today.ToString(DateTimeConstants.REVERSE_SHORT_DATE_FORMAT))
            });

            using (var client = new HttpClient())
            {
                try
                {
                    var responseLogin = await client.PostAsync(Properties.Resources.SERPICA_LOGIN_URL, loginContent).ConfigureAwait(false);
                    var responseMenu = await client.PostAsync(Properties.Resources.SERPICA_DAILY_MENU_URL, dailyMenuContent).ConfigureAwait(false);
                    dailyMenuString = responseMenu.Content.ReadAsStringAsync().Result;
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine("Message :{0} ", e.Message);
                }
            }

            var meals = JsonConvert.DeserializeObject<List<List<string>>>(dailyMenuString);

            foreach (var meal in meals)
            {
                if (meal[7] != "0")
                {
                    dailyMenu.Add(new Meal()
                    {
                        Name = meal[1],
                        Description = meal[2],
                        Price = int.Parse(meal[7]),
                        Type = (int)GetMealType(meal),
                        Restaurant = Restaurant,
                        IsActive = true,
                        IsFromStandardMenu = false
                    });
                }
                else if (meal[8] == "6")
                {
                    dailyMenu.Add(new Meal()
                    {
                        Name = meal[1] + " - Jedan paket",
                        Description = meal[2],
                        Price = int.Parse(meal[5]),
                        Type = (int)GetMealType(meal),
                        Restaurant = Restaurant,
                        IsActive = true,
                        IsFromStandardMenu = false
                    });

                    dailyMenu.Add(new Meal()
                    {
                        Name = meal[1] + " - Dva paketa",
                        Description = meal[2],
                        Price = int.Parse(meal[6]),
                        Type = (int)GetMealType(meal),
                        Restaurant = Restaurant,
                        IsActive = true,
                        IsFromStandardMenu = false
                    });
                }
                else
                {
                    dailyMenu.Add(new Meal()
                    {
                        Name = meal[1] + " - Mala porcija",
                        Description = meal[2],
                        Price = int.Parse(meal[5]),
                        Type = (int)GetMealType(meal),
                        Restaurant = Restaurant,
                        IsActive = true,
                        IsFromStandardMenu = false
                    });

                    dailyMenu.Add(new Meal()
                    {
                        Name = meal[1] + " - Velika porcija",
                        Description = meal[2],
                        Price = int.Parse(meal[6]),
                        Type = (int)GetMealType(meal),
                        Restaurant = Restaurant,
                        IsActive = true,
                        IsFromStandardMenu = false
                    });

                    dailyMenu.Add(new Meal()
                    {
                        Name = meal[1] + " - XL porcija",
                        Description = meal[2],
                        Price = int.Parse(meal[9]),
                        Type = (int)GetMealType(meal),
                        Restaurant = Restaurant,
                        IsActive = true,
                        IsFromStandardMenu = false
                    });
                }
            }

            return dailyMenu;
        }

        public void UpdateRestaurant()
        {
            var orderDue = new DateTime(2020, 1, 1, 10, 0, 0);
            UpdateRestaurant(Restaurant.Id, orderDue, SheetId);
        }

        private MealType GetMealType(List<string> meal)
        {
            if (meal[11] == "da")
            {
                return MealType.VEGETARIAN;
            }
            else
            {
                switch (int.Parse(meal[8]))
                {
                    case 5:
                    case 8:
                    case 10:
                        return MealType.SALAD;
                    case 6:
                        return MealType.SIDE_DISH;
                    case 11:
                        return MealType.DESSERT;
                    default:
                        return MealType.MAIN_COURSE;
                }
            }
        }
    }
}
