﻿using System.Collections.Generic;
using Either;
using Exebite.Common;
using Exebite.DomainModel;
using Exebite.GoogleSheetAPI.Connectors.Restaurants.Base;

namespace Exebite.GoogleSheetAPI.Connectors.Restaurants
{
    public interface IParrillaConnector : IRestaurantConnector
    {
        /// <summary>
        /// Get all meals from the main menu
        /// </summary>
        /// <returns>List of meals</returns>
        Either<Error, List<Meal>> GetMainMenu();

        /// <summary>
        /// Gets meals available for today
        /// </summary>
        /// <returns>List of meals</returns>
        Either<Error, IEnumerable<Meal>> GetDailyMenu();

        /// <summary>
        /// Update restaurant with restaurant info from sheet
        /// </summary>
        /// <returns>Is restaurant updated succesffully</returns>
        Either<Error, bool> UpdateRestaurant();
    }
}
