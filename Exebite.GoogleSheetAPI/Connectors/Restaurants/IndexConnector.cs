﻿using System;
using System.Collections.Generic;
using Either;
using Exebite.Common;
using Exebite.DataAccess.Repositories;
using Exebite.DomainModel;
using Exebite.GoogleSheetAPI.Connectors.Restaurants.Base;
using Exebite.GoogleSheetAPI.GoogleSSFactory;
using Exebite.GoogleSheetAPI.SheetExtractor;

namespace Exebite.GoogleSheetAPI.Connectors.Restaurants
{
    public sealed class IndexConnector : RestaurantConnector, IIndexConnector
    {
        private const string _mainMenuRange = "'Opisi jela i cene'!A3:D";
        private const short _validRowInfoCount = 2;
        private const string _foodSideDish = "DODACI";
        private const string _foodBurger = "BURGERI";
        private const string _foodSerbianBurger = "PLJESKAVICE";
        private const string _foodGrilledSandwich = "GRILL SENDVIČI";
        private const string _foodFinishedSandwich = "GOTOVI SENDVIČI";
        private const string _foodMealSalad = "OBROK SALATE";
        private const string _foodChickenNuggets = "CHICKEN NUGGETS";
        private const string _chickenNuggetsNamePrefix = "Chicken Nuggets - ";
        private const string _foodVegeterian = "VEGETERIJANSKA I POSNA JELA";
        private const string _foodPasta = "PASTE";
        private const string _foodChicken = "JELA OD PILETINE";
        private const string _foodSalad = "SALATE";
        private const string _orderDueRange = "!A3:A5";

        public IndexConnector(
            IGoogleSheetExtractor googleSheetService,
            IGoogleSpreadsheetIdFactory googleSSIdFactory,
            IRestaurantQueryRepository restaurantQueryRepository,
            IRestaurantCommandRepository restaurantCommandRepository)
            : base(googleSheetService, restaurantQueryRepository, restaurantCommandRepository, RestaurantConstants.INDEX_NAME)
        {
            SheetId = googleSSIdFactory.GetSheetId(Enums.ESheetOwner.INDEX_HOUSE);
            ColumnsPerDay = 13;
        }

        /// <summary>
        /// Write menu with all foods in DB to sheet. Used for initial writing of old menu
        /// </summary>
        /// <param name="foods">List of all food to be written</param>
        public override void WriteMenu(List<Meal> foods)
        {
            // not needed for now. But will probably be needed in the future to write orders
            // in sheets until everything is moved to be get from DB (reports, orders...)
        }

        public Either<Error, List<Meal>> GetMainMenu()
        {
            try
            {
                var offersList = GoogleSheetExtractor.ReadSheetData(_mainMenuRange, SheetId);
                var meals = new List<Meal>();
                var grillSandwichesAndSerbianBurgers = new List<Meal>();
                var mealSalads = new List<Meal>();
                var mealCondiments = new List<Meal>();
                var saladCondiments = new List<Meal>();
                var foodCategory = string.Empty;
                string foodName;
                MealType foodType;

                foreach (var row in offersList.Values)
                {
                    if (row.Count > _validRowInfoCount)
                    {
                        foodName = row[1].ToString();

                        var rowFoodCategory = row[0].ToString();
                        if (!string.IsNullOrWhiteSpace(rowFoodCategory))
                        {
                            foodCategory = rowFoodCategory;
                        }

                        var price = ParsePrice(row[2].ToString(), false);

                        if (!foodCategory.StartsWith(_foodSideDish))
                        {
                            switch (foodCategory)
                            {
                                case _foodBurger:
                                case _foodSerbianBurger:
                                    foodType = MealType.BURGER;
                                    grillSandwichesAndSerbianBurgers.Add(new Meal { Name = foodName, Price = price, Restaurant = Restaurant, Type = (int)foodType, IsActive = true, IsFromStandardMenu = true });
                                    continue;
                                case _foodGrilledSandwich:
                                    foodType = MealType.SANDWICH;
                                    grillSandwichesAndSerbianBurgers.Add(new Meal { Name = foodName, Price = price, Restaurant = Restaurant, Type = (int)foodType, IsActive = true, IsFromStandardMenu = true });
                                    continue;
                                case _foodFinishedSandwich:
                                    foodType = MealType.SANDWICH;
                                    break;
                                case _foodMealSalad:
                                    foodType = MealType.SALAD;
                                    mealSalads.Add(new Meal { Name = foodName, Price = price, Restaurant = Restaurant, Type = (int)foodType, IsActive = true, IsFromStandardMenu = true });
                                    continue;
                                case _foodChickenNuggets:
                                    foodType = MealType.CHICKEN;
                                    foodName = $"{_chickenNuggetsNamePrefix}{foodName}";
                                    break;
                                case _foodVegeterian:
                                    foodType = MealType.VEGETARIAN;
                                    break;
                                case _foodPasta:
                                    foodType = MealType.PASTA;
                                    break;
                                case _foodChicken:
                                    foodType = MealType.CHICKEN;
                                    break;
                                default:
                                    foodType = MealType.MAIN_COURSE;
                                    break;
                            }

                            meals.Add(new Meal { Name = foodName, Price = price, Restaurant = Restaurant, Type = (int)foodType, IsFromStandardMenu = true });
                        }
                        else
                        {
                            var meal = new Meal { Name = foodName, Price = price, Restaurant = Restaurant, Type = (int)MealType.CONDIMENT, IsActive = true, IsFromStandardMenu = true };
                            if (foodCategory.EndsWith(_foodSalad))
                            {
                                saladCondiments.Add(meal);
                            }
                            else
                            {
                                mealCondiments.Add(meal);
                            }
                        }
                    }
                }

                foreach (var meal in grillSandwichesAndSerbianBurgers)
                {
                    meal.Condiments = mealCondiments;
                }

                foreach (var meal in mealSalads)
                {
                    meal.Condiments = saladCondiments;
                }

                meals.AddRange(grillSandwichesAndSerbianBurgers);
                meals.AddRange(mealSalads);

                return new Right<Error, List<Meal>>(meals);
            }
            catch (Exception e)
            {
                return new Left<Error, List<Meal>>(new ValidationError(e.Message));
            }
        }

        public Either<Error, bool> UpdateRestaurant()
        {
            return UpdateRestaurant(_orderDueRange, SheetId, Restaurant.Id);
        }
    }
}