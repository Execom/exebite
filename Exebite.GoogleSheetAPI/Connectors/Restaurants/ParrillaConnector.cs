﻿using Either;
using Exebite.Common;
using Exebite.DataAccess.Repositories;
using Exebite.DomainModel;
using Exebite.GoogleSheetAPI.Connectors.Restaurants.Base;
using Exebite.GoogleSheetAPI.GoogleSSFactory;
using Exebite.GoogleSheetAPI.SheetExtractor;
using System;
using System.Collections.Generic;

namespace Exebite.GoogleSheetAPI.Connectors.Restaurants
{
    public sealed class ParrillaConnector : RestaurantConnector, IParrillaConnector
    {
        private const string _dailyMenuRange = "!1:4";
        private const string _orderDueRange = "!A5";
        private const string _dailyMenuPrefix = "DNEVNA PONUDA-";
        private const string _foodMealSalad = "Obrok salate";
        private const string _foodBurger = "Burgeri";
        private const string _foodSandwiches = "Sendviči";
        private const string _foodDesert = "Desert";
        private const string _sideDish = "Dodaci";
        private const short _mealTypeRow = 1;
        private const int _dateIndex = 1;
        private const string _mainMenuRange = "'Opisi jela i cene'!A2:C";

        public ParrillaConnector(
            IGoogleSheetExtractor googleSheetExtractor,
            IGoogleSpreadsheetIdFactory googleSSIdFactory,
            IRestaurantQueryRepository restaurantQueryRepository,
            IRestaurantCommandRepository restaurantCommandRepository)
            : base(googleSheetExtractor, restaurantQueryRepository, restaurantCommandRepository, RestaurantConstants.PARRILLA_NAME)
        {
            SheetId = googleSSIdFactory.GetSheetId(Enums.ESheetOwner.PARRILLA);
            ColumnsPerDay = 10;
            DailyMenuSheet = $"{_dailyMenuPrefix}{GetLocalMonthName(DailyMenuDate.Month - 1).ToUpper()}{DailyMenuDate.Year}";
        }

        public override void WriteMenu(List<Meal> foods)
        {
            // not needed for now. But will probably be needed in the future to write orders
            // in sheets until everything is moved to be get from DB (reports, orders...)
        }

        public Either<Error, IEnumerable<Meal>> GetDailyMenu()
        {
            var range = $"'{DailyMenuSheet}'{_dailyMenuRange}";
            try
            {
                return new Right<Error, IEnumerable<Meal>>(GetDailyMenu(range, _dateIndex));
            }
            catch (Exception e)
            {
                return new Left<Error, IEnumerable<Meal>>(new ValidationError(e.Message));
            }
        }

        public Either<Error, List<Meal>> GetMainMenu()
        {
            try
            {
                var offersList = GoogleSheetExtractor.ReadSheetData(_mainMenuRange, SheetId);
                var meals = new List<Meal>();
                var foodType = MealType.MAIN_COURSE;

                if (offersList?.Values?.Count > 0)
                {
                    foreach (var row in offersList.Values)
                    {
                        if (row.Count == _mealTypeRow)
                        {
                            switch (row[0].ToString())
                            {
                                case _foodMealSalad:
                                    foodType = MealType.SALAD;
                                    break;
                                case _foodBurger:
                                    foodType = MealType.BURGER;
                                    break;
                                case _foodSandwiches:
                                    foodType = MealType.SANDWICH;
                                    break;
                                case _foodDesert:
                                    foodType = MealType.DESSERT;
                                    break;
                                case _sideDish:
                                    foodType = MealType.SIDE_DISH;
                                    break;
                                default:
                                    foodType = MealType.MAIN_COURSE;
                                    break;
                            }
                        }
                        else
                        {
                            var price = ParsePrice(row[2].ToString(), true);
                            meals.Add(new Meal { Name = row[0].ToString(), Description = row[1].ToString(), Price = price, Restaurant = Restaurant, IsFromStandardMenu = true, Type = (int)foodType });
                        }
                    }
                }

                return new Right<Error, List<Meal>>(meals);
            }
            catch (Exception e)
            {
                return new Left<Error, List<Meal>>(new ValidationError(e.Message));
            }
        }

        public Either<Error, bool> UpdateRestaurant()
        {
            var tabAndRange = $"'{DailyMenuSheet}'{_orderDueRange}";

            return UpdateRestaurant(tabAndRange, SheetId, Restaurant.Id);
        }
    }
}
