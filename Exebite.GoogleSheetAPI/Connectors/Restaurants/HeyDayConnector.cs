﻿using Either;
using Exebite.Common;
using Exebite.DataAccess.Repositories;
using Exebite.DomainModel;
using Exebite.GoogleSheetAPI.Connectors.Restaurants.Base;
using Exebite.GoogleSheetAPI.GoogleSSFactory;
using Exebite.GoogleSheetAPI.SheetExtractor;
using System;
using System.Collections.Generic;

namespace Exebite.GoogleSheetAPI.Connectors.Restaurants
{
    public sealed class HeyDayConnector : RestaurantConnector, IHeyDayConnector
    {
        private const string _orderDueRange = "!B3:C3";
        private const string _foodSalads = "salads";
        private const string _foodTorillas = "tortillas";
        private const string _foodSandwiches = "sandwiches";
        private const string _foodDressings = "dressings";
        private const string _foodAcaiBowls = "acai bowls";
        private const string _foodDesert = "desert";
        private const string _foodSmoothies = "smoothies";
        private const string _foodWelnessShots = "wellness shots";
        private const short _mealTypeRow = 1;
        private const string _mainMenuRange = "'MENI'!A2:C";

        public HeyDayConnector(
            IGoogleSheetExtractor googleSheetService,
            IGoogleSpreadsheetIdFactory googleSSIdFactory,
            IRestaurantQueryRepository restaurantQueryRepository,
            IRestaurantCommandRepository restaurantCommandRepository)
            : base(googleSheetService, restaurantQueryRepository, restaurantCommandRepository, RestaurantConstants.HEY_DAY_NAME)
        {
            SheetId = googleSSIdFactory.GetSheetId(Enums.ESheetOwner.HEY_DAY);
            ColumnsPerDay = 18;
        }

        public override void WriteMenu(List<Meal> foods)
        {
            // not needed for now. But will probably be needed in the future to write orders
            // in sheets until everything is moved to be get from DB (reports, orders...)
        }

        public Either<Error, List<Meal>> GetMainMenu()
        {
            try
            {
                var offersList = GoogleSheetExtractor.ReadSheetData(_mainMenuRange, SheetId);
                var meals = new List<Meal>();
                var foodType = MealType.MAIN_COURSE;

                foreach (var row in offersList.Values)
                {
                    if (row.Count == _mealTypeRow)
                    {
                        switch (row[0].ToString())
                        {
                            case _foodSalads:
                                foodType = MealType.SALAD;
                                break;
                            case _foodTorillas:
                                foodType = MealType.TORTILLA;
                                break;
                            case _foodSandwiches:
                                foodType = MealType.SANDWICH;
                                break;
                            case _foodDesert:
                            case _foodAcaiBowls:
                                foodType = MealType.DESSERT;
                                break;
                            case _foodSmoothies:
                            case _foodWelnessShots:
                                foodType = MealType.DRINK;
                                break;
                            case _foodDressings:
                                foodType = MealType.CONDIMENT;
                                break;
                            default:
                                foodType = MealType.MAIN_COURSE;
                                break;
                        }
                    }
                    else
                    {
                        var price = ParsePrice(row[1].ToString(), false);
                        meals.Add(new Meal { Name = row[0].ToString(), Description = row[2].ToString(), Price = price, Restaurant = Restaurant, Type = (int)foodType, IsActive = true, IsFromStandardMenu = true });
                    }
                }

                return new Right<Error, List<Meal>>(meals);
            }
            catch (Exception e)
            {
                return new Left<Error, List<Meal>>(new ValidationError(e.Message));
            }
        }

        public Either<Error, bool> UpdateRestaurant()
        {
            return UpdateRestaurant(_orderDueRange, SheetId, Restaurant.Id);
        }
    }
}
