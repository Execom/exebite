﻿using Exebite.DomainModel;
using Exebite.GoogleSheetAPI.Connectors.Restaurants.Base;
using System.Collections.Generic;

namespace Exebite.GoogleSheetAPI.Connectors.Restaurants
{
    public interface ISerpicaConnector : IRestaurantConnector
    {
        /// <summary>
        /// Gets meals available for today
        /// </summary>
        /// <returns>List of meals</returns>
        IEnumerable<Meal> GetDailyMenu();

        /// <summary>
        /// Update restaurant with restaurant info from sheet
        /// </summary>
        void UpdateRestaurant();
    }
}
