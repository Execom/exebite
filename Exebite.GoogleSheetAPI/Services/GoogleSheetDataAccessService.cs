﻿using System;
using System.Collections.Generic;
using System.Linq;
using Either;
using Exebite.Common;
using Exebite.DataAccess.Repositories;
using Exebite.DomainModel;

namespace Exebite.GoogleSheetAPI.Services
{
    /// <inheritdoc/>
    public sealed class GoogleSheetDataAccessService : IGoogleSheetDataAccessService
    {
        private readonly IEitherMapper _mapper;
        private readonly ICustomerCommandRepository _customerCommandRepository;
        private readonly ICustomerQueryRepository _customerQueryRepository;
        private readonly IMealQueryRepository _mealQueryRepository;
        private readonly IMealCommandRepository _mealCommandRepository;
        private readonly IDailyMenuQueryRepository _dailyMenuQueryRepository;
        private readonly IDailyMenuCommandRepository _dailyMenuCommandRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="GoogleSheetDataAccessService"/> class.
        /// </summary>
        /// <param name="mapper">Mapper instance.</param>
        /// <param name="customerCommandRepository">Customer command repository</param>
        /// <param name="customerQueryRepository">Customer query repository.</param>
        /// <param name="foodCommandRepository">Food command repository</param>
        /// <param name="foodQueryRepository">Food query repository.</param>
        /// <param name="dailyMenuCommandRepository">Daily menu command repository.</param>
        /// <param name="dailyMenuQueryRepository">Daily menu query repository.</param>
        public GoogleSheetDataAccessService(
            IEitherMapper mapper,
            ICustomerCommandRepository customerCommandRepository,
            ICustomerQueryRepository customerQueryRepository,
            IMealCommandRepository foodCommandRepository,
            IMealQueryRepository foodQueryRepository,
            IDailyMenuCommandRepository dailyMenuCommandRepository,
            IDailyMenuQueryRepository dailyMenuQueryRepository)
        {
            _mapper = mapper;

            _customerCommandRepository = customerCommandRepository;
            _customerQueryRepository = customerQueryRepository;
            _mealCommandRepository = foodCommandRepository;
            _mealQueryRepository = foodQueryRepository;
            _dailyMenuCommandRepository = dailyMenuCommandRepository;
            _dailyMenuQueryRepository = dailyMenuQueryRepository;
        }

        /// <inheritdoc/>
        public Either<Error, (int Added, int Updated)> UpdateCustomers(IEnumerable<Customer> customers)
        {
            var added = 0;
            var updated = 0;

            if (!customers.Any())
            {
                return new Left<Error, (int, int)>(new ArgumentNotSet(nameof(customers)));
            }

            foreach (var customer in customers)
            {
                var exists = _customerQueryRepository
                     .ExistsByGoogleId(customer.GoogleUserId)
                     .Reduce(r => false);

                if (!exists)
                {
                    added += _mapper
                        .Map<CustomerInsertModel>(customer)
                        .Map(_customerCommandRepository.Insert)
                        .Reduce(r => 0) > 0 ? 1 : 0;
                }
                else
                {
                    updated += _mapper
                        .Map<CustomerUpdateModel>(customer)
                        .Map(_customerCommandRepository.UpdateByGoogleId)
                        .Reduce(r => false) ? 1 : 0;
                }
            }

            return new Right<Error, (int, int)>((added, updated));
        }

        /// <inheritdoc/>
        public Either<Error, (int Added, int Updated)> UpdateDailyMeals(IEnumerable<Meal> meals)
        {
            (int addedCount, int updateCount) = UpdateMeals(meals)
                                                .Reduce(r => (-1, -1));
            if (addedCount == -1 && updateCount == -1)
            {
                return new Left<Error, (int, int)>(new ArgumentNotSet($"{nameof(meals)} is empty"));
            }

            DailyMenu dailyMenu = GetDailyMenu(meals.First().Restaurant.Id);
            if (dailyMenu == null)
            {
                return new Left<Error, (int, int)>(new ArgumentNotSet($"{nameof(dailyMenu)} doesn't exist"));
            }

            _dailyMenuCommandRepository.Update(dailyMenu.Id, new DailyMenuUpdateModel() { RestaurantId = dailyMenu.Restaurant.Id, Date = dailyMenu.Date, Meals = meals.ToList() });

            return new Right<Error, (int, int)>((addedCount, updateCount));
        }

        public Either<Error, (int Added, int Updated)> UpdateMeals(IEnumerable<Meal> meals)
        {
            var addedCount = 0;
            var updatedCount = 0;
            var updatedFood = new List<Meal>();

            if (!meals.Any())
            {
                return new Left<Error, (int, int)>(new ArgumentNotSet(nameof(meals)));
            }

            foreach (var meal in meals)
            {
                var mealId = _mealQueryRepository
                            .FindByNameAndRestaurantId(new MealQueryModel { Name = meal.Name, RestaurantId = meal.Restaurant.Id })
                            .Reduce(r => 0);

                if (mealId == 0)
                {
                    var insertFood = new MealInsertModel
                    {
                        Description = meal.Description,
                        IsActive = meal.IsActive,
                        Name = meal.Name,
                        Price = meal.Price,
                        RestaurantId = meal.Restaurant.Id,
                        Type = (MealType)meal.Type,
                        Condiments = meal.Condiments,
                        IsFromStandardMenu = meal.IsFromStandardMenu
                    };

                    mealId = _mapper
                        .Map<MealInsertModel>(insertFood)
                        .Map(_mealCommandRepository.Insert)
                        .Reduce(r => 0);

                    addedCount += mealId > 0 ? 1 : 0;
                }
                else
                {
                    updatedCount += _mapper
                        .Map<MealUpdateModel>(meal)
                        .Map(_mealCommandRepository.UpdateByNameAndRestaurantId)
                        .Reduce(r => false) ? 1 : 0;
                }

                meal.Id = mealId;
            }

            return new Right<Error, (int, int)>((addedCount, updatedCount));
        }

        private DailyMenu GetDailyMenu(long restaurantId)
        {
            var dailyMenus = _dailyMenuQueryRepository
                                .Query(new DailyMenuQueryModel { RestaurantId = restaurantId })
                                .Map(d => d.Items)
                                .Reduce(r => new List<DailyMenu>(), ex => Console.WriteLine(ex.ToString()));

            if (dailyMenus.Any())
            {
                return dailyMenus.First();
            }

            var dailyMenuId = _dailyMenuCommandRepository
                                .Insert(new DailyMenuInsertModel { RestaurantId = restaurantId, Date = DateTime.Today })
                                .Map(d => d)
                                .Reduce(r => 0, ex => Console.WriteLine(ex.ToString()));

            dailyMenus = _dailyMenuQueryRepository
                                .Query(new DailyMenuQueryModel { Id = dailyMenuId })
                                .Map(d => d.Items)
                                .Reduce(r => new List<DailyMenu>(), ex => Console.WriteLine(ex.ToString()));

            return dailyMenus.First();
        }
    }
}