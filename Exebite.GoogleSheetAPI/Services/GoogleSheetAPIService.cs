﻿using Either;
using Exebite.Common;
using Exebite.DomainModel;
using Exebite.DomainModel.Meals;
using Exebite.GoogleSheetAPI.Connectors.Kasa;
using Exebite.GoogleSheetAPI.Connectors.Restaurants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exebite.GoogleSheetAPI.Connectors.Restaurants.Base;

namespace Exebite.GoogleSheetAPI.Services
{
    /// <inheritdoc/>
    public sealed class GoogleSheetAPIService : IGoogleSheetAPIService
    {
        private const int _lipaDateIndex = 2;
        private const int _mimasDateIndex = 3;
        private const int _topliObrokDateIndex = 3;
        private const int _parrillaDateIndex = 2;

        private readonly IEitherMapper _mapper;
        private readonly IGoogleSheetDataAccessService _googleSheetDataAccessService;

        private readonly IKasaConnector _kasaConnector;
        private readonly ILipaConnector _lipaConnector;
        private readonly ITopliObrokConnector _topliObrokConnector;
        private readonly IIndexConnector _indexConnector;
        private readonly IMimasConnector _mimasConnector;
        private readonly ISerpicaConnector _serpicaConnector;
        private readonly IHeyDayConnector _heyDayConnector;
        private readonly IParrillaConnector _parrillaConnector;

        /// <summary>
        /// Initializes a new instance of the <see cref="GoogleSheetAPIService"/> class.
        /// </summary>
        /// <param name="mapper">Mapper that map result.</param>
        /// <param name="googleSheetDataAccessService">Perform the actual persistence of the <see cref="IGoogleSheetAPIService"/> operations.</param>
        /// <param name="kasaConnector">Connector for kasa google sheet.</param>
        /// <param name="lipaConnector">Connector for restaurant Lipa google sheet.</param>
        /// <param name="topliObrokConnector">Connector for restaurant topli obrok google sheet.</param>
        /// <param name="indexConnector">Connector for restaurant Index google sheet.</param>
        /// <param name="mimasConnector">Connector for restaurant mimas google sheet.</param>
        /// <param name="serpicaConnector">Connector for restaurant serpica API.</param>
        /// <param name="heyDayConnector">Connector for restaurant Hey Day google sheet.</param>
        /// <param name="parrillaConnector">Connector for restaurant Parrilla google sheet.</param>
        public GoogleSheetAPIService(
            IEitherMapper mapper,
            IGoogleSheetDataAccessService googleSheetDataAccessService,
            IKasaConnector kasaConnector,
            ILipaConnector lipaConnector,
            ITopliObrokConnector topliObrokConnector,
            IIndexConnector indexConnector,
            IMimasConnector mimasConnector,
            ISerpicaConnector serpicaConnector,
            IHeyDayConnector heyDayConnector,
            IParrillaConnector parrillaConnector)
        {
            _mapper = mapper;
            _googleSheetDataAccessService = googleSheetDataAccessService;
            _kasaConnector = kasaConnector;
            _lipaConnector = lipaConnector;
            _topliObrokConnector = topliObrokConnector;
            _indexConnector = indexConnector;
            _mimasConnector = mimasConnector;
            _serpicaConnector = serpicaConnector;
            _heyDayConnector = heyDayConnector;
            _parrillaConnector = parrillaConnector;
        }

        /// <inheritdoc/>
        public void UpdateCustomers()
        {
            _mapper
               .Map<IEnumerable<Customer>>(_kasaConnector.GetAllCustomers())
               .Map(_googleSheetDataAccessService.UpdateCustomers)
               .Map(count => LogRowsAffected(count, typeof(Customer), nameof(_kasaConnector)))
               .Reduce(_ => (0, 0), ex => Console.WriteLine(ex.Message));
        }

        /// <inheritdoc/>
        public void UpdateDailyMenuLipa()
        {
            _lipaConnector.GetDailyMenu().Map(_googleSheetDataAccessService.UpdateDailyMeals)
                                         .Map(count => LogRowsAffected(count, typeof(Meal), nameof(_lipaConnector)))
                                         .Reduce(_ => (0, 0), ex => Console.WriteLine(ex.Message));
        }

        /// <inheritdoc/>
        public void UpdateDailyMenuTopliObrok()
        {
            _topliObrokConnector.GetDailyMenu().Map(_googleSheetDataAccessService.UpdateDailyMeals)
                                               .Map(count => LogRowsAffected(count, typeof(Meal), nameof(_topliObrokConnector)))
                                               .Reduce(_ => (0, 0), ex => Console.WriteLine(ex.Message));
        }

        /// <inheritdoc/>
        public void UpdateDailyMenuMimas()
        {
            _mimasConnector.GetDailyMenu().Map(_googleSheetDataAccessService.UpdateDailyMeals)
                                          .Map(count => LogRowsAffected(count, typeof(Meal), nameof(_mimasConnector)))
                                          .Reduce(_ => (0, 0), ex => Console.WriteLine(ex.Message));
        }

        /// <inheritdoc/>
        public void UpdateDailyMenuSerpica()
        {
            _mapper
                .Map<IEnumerable<Meal>>(_serpicaConnector.GetDailyMenu())
                .Map(_googleSheetDataAccessService.UpdateDailyMeals)
                .Map(count => LogRowsAffected(count, typeof(Meal), nameof(_serpicaConnector)))
                .Reduce(_ => (0, 0), ex => Console.WriteLine(ex.Message));
        }

        /// <inheritdoc/>
        public void UpdateDailyMenuParrilla()
        {
            _parrillaConnector.GetDailyMenu().Map(_googleSheetDataAccessService.UpdateDailyMeals)
                                             .Map(count => LogRowsAffected(count, typeof(Meal), nameof(_parrillaConnector)))
                                             .Reduce(_ => (0, 0), ex => Console.WriteLine(ex.Message));
        }

        /// <inheritdoc/>
        public void UpdateMainMenuIndex()
        {
            _indexConnector.GetMainMenu().Map(_googleSheetDataAccessService.UpdateMeals)
                                         .Map(count => LogRowsAffected(count, typeof(Meal), nameof(_indexConnector)))
                                         .Reduce(_ => (0, 0), ex => Console.WriteLine(ex.Message));
        }

        /// <inheritdoc/>
        public void UpdateMainMenuParrilla()
        {
            _parrillaConnector.GetMainMenu().Map(_googleSheetDataAccessService.UpdateMeals)
                                            .Map(count => LogRowsAffected(count, typeof(Meal), nameof(_parrillaConnector)))
                                            .Reduce(_ => (0, 0), ex => Console.WriteLine(ex.Message));
        }

        /// <inheritdoc/>
        public void UpdateMainMenuHeyDay()
        {
            _heyDayConnector.GetMainMenu().Map(_googleSheetDataAccessService.UpdateMeals)
                                          .Map(count => LogRowsAffected(count, typeof(Meal), nameof(_heyDayConnector)))
                                          .Reduce(_ => (0, 0), ex => Console.WriteLine(ex.Message));
        }

        public void UpdateRestaurants()
        {
            var tasks = new List<Task>
            {
                Task.Factory.StartNew(() => _heyDayConnector.UpdateRestaurant()
                                                                      .Reduce(_ => false, ex => Console.WriteLine(ex.Message))),
                Task.Factory.StartNew(() => _indexConnector.UpdateRestaurant()
                                                                      .Reduce(_ => false, ex => Console.WriteLine(ex.Message))),
                Task.Factory.StartNew(() => _parrillaConnector.UpdateRestaurant()
                                                                      .Reduce(_ => false, ex => Console.WriteLine(ex.Message))),
                Task.Factory.StartNew(() => _lipaConnector.UpdateRestaurant()
                                                                      .Reduce(_ => false, ex => Console.WriteLine(ex.Message))),
                Task.Factory.StartNew(() => _topliObrokConnector.UpdateRestaurant()
                                                                      .Reduce(_ => false, ex => Console.WriteLine(ex.Message))),
                Task.Factory.StartNew(() => _mimasConnector.UpdateRestaurant()
                                                                     .Reduce(_ => false, ex => Console.WriteLine(ex.Message))),
                Task.Factory.StartNew(() => _serpicaConnector.UpdateRestaurant())
            };

            Task.WaitAll(tasks.ToArray());
            Console.WriteLine("\nRestaurants updated successfully");
        }

        public void WriteOrder(string customerName, string locationName, Dictionary<string, MealContainer> restaurants)
        {
            foreach (var restaurant in restaurants)
            {
                switch (restaurant.Key)
                {
                    case RestaurantConstants.POD_LIPOM_NAME:
                        WriteOrders(restaurant.Value, _lipaDateIndex, customerName, locationName, _lipaConnector);
                        break;
                    case RestaurantConstants.MIMAS_NAME:
                        WriteOrders(restaurant.Value, _mimasDateIndex, customerName, locationName, _mimasConnector);
                        break;
                    case RestaurantConstants.TOPLI_OBROK_NAME:
                        WriteOrders(restaurant.Value, _topliObrokDateIndex, customerName, locationName, _topliObrokConnector);
                        break;
                    case RestaurantConstants.PARRILLA_NAME:
                        WriteOrders(restaurant.Value, _parrillaDateIndex, customerName, locationName, _parrillaConnector);
                        break;
                    default:
                        throw new NotImplementedException();
                }
            }
        }

        private void WriteOrders(MealContainer mealContainer, int dateRowIndex, string customerName, string locationName, IRestaurantConnector connector)
        {
            if (mealContainer.DailyMeals.Any())
            {
                connector.WriteDailyOrder(customerName, locationName, mealContainer.DailyMeals, dateRowIndex);
            }

            if (mealContainer.StandardMeals.Any())
            {

            }
        }

        private (int, int) LogRowsAffected((int added, int updated) rows, Type modelType, string connectorName)
        {
            Console.WriteLine(
                "\nInserted  >> {0} records \nUpdated   >> {1} records \nType      >> {2}\nConnector >> {3}",
                rows.added,
                rows.updated,
                modelType,
                connectorName);
            return rows;
        }
    }
}
