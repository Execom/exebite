﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Exebite.GoogleSheetAPI.Common;
using Exebite.GoogleSheetAPI.GoogleSSFactory;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using Microsoft.Extensions.Logging;

namespace Exebite.GoogleSheetAPI.SheetExtractor
{
    public sealed class GoogleSheetExtractor : IGoogleSheetExtractor
    {
        private readonly SheetsService _sheetService;
        private readonly IGoogleSheetServiceFactory _googleSSFactory;
        private readonly ILogger<GoogleSheetExtractor> _logger;

        public GoogleSheetExtractor(IGoogleSheetServiceFactory googleSheetServiceFactory, ILogger<GoogleSheetExtractor> logger)
        {
            _googleSSFactory = googleSheetServiceFactory ?? throw new ArgumentNullException(nameof(googleSheetServiceFactory));
            _sheetService = _googleSSFactory?.GetService() ?? throw new ArgumentNullException(nameof(_googleSSFactory));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public ValueRange GetRows(string sheetId, string range)
        {
            SpreadsheetsResource.ValuesResource.GetRequest request =
            _sheetService.Spreadsheets.Values.Get(sheetId, range);
            request.MajorDimension = SpreadsheetsResource.ValuesResource.GetRequest.MajorDimensionEnum.ROWS;

            return request.Execute();
        }

        public IList<ValueRange> GetBatchRows(string sheetId, List<string> ranges)
        {
            var request = _sheetService.Spreadsheets.Values.BatchGet(sheetId);
            request.MajorDimension = SpreadsheetsResource.ValuesResource.BatchGetRequest.MajorDimensionEnum.ROWS;
            request.Ranges = ranges;

            return request.Execute().ValueRanges;
        }

        public IList<RowData> GetRowsWithAdditionalInfo(string sheetId, string range)
        {
            var request = _sheetService.Spreadsheets.Get(sheetId);
            request.Ranges = range;
            request.IncludeGridData = true;

            var result = request.Execute();

            return result.Sheets[0]?.Data[0]?.RowData;
        }

        public ValueRange GetColumns(string sheetId, string range)
        {
            SpreadsheetsResource.ValuesResource.GetRequest request =
            _sheetService.Spreadsheets.Values.Get(sheetId, range);
            request.MajorDimension = SpreadsheetsResource.ValuesResource.GetRequest.MajorDimensionEnum.COLUMNS;

            return request.Execute();
        }

        public void Update(ValueRange body, string sheetId, string range)
        {
            SpreadsheetsResource.ValuesResource.UpdateRequest updateRequest = _sheetService.Spreadsheets.Values.Update(body, sheetId, range);
            updateRequest.ValueInputOption = SpreadsheetsResource.ValuesResource.UpdateRequest.ValueInputOptionEnum.USERENTERED;
            updateRequest.Execute();
        }

        public void Clear(string sheetId, string range)
        {
            ClearValuesRequest body = new ClearValuesRequest();
            SpreadsheetsResource.ValuesResource.ClearRequest clearRequest = _sheetService.Spreadsheets.Values.Clear(body, sheetId, range);
            clearRequest.Execute();
        }

        public IEnumerable<Sheet> GetWorkSheets(string sheetId)
        {
            try
            {
                return _sheetService
                        .Spreadsheets
                        .Get(sheetId)
                        .Execute()
                        .Sheets;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);

                return new List<Sheet>();
            }
        }

        public ValueRange ReadSheetData(string range, string sheetId)
        {
            try
            {
                var request = _sheetService
                                .Spreadsheets
                                .Values
                                .Get(sheetId, range);

                // We need to set this in order to get date-time properly.
                request.ValueRenderOption = SpreadsheetsResource.ValuesResource.GetRequest.ValueRenderOptionEnum.UNFORMATTEDVALUE;

                return request.Execute();
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);

                return new ValueRange();
            }
        }

        public T ExtractCell<T>(IList<CellData> objectList, int index, T defaultValue)
        {
            T retVal;

            try
            {
                retVal = (T)Convert.ChangeType(objectList[index].FormattedValue, typeof(T), CultureInfo.InvariantCulture);

                if (retVal is string a && string.IsNullOrWhiteSpace(a))
                {
                    retVal = defaultValue;
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                retVal = defaultValue;
            }

            return retVal;
        }

        public Result<object> ReadOneValue(string range, string sheetId)
        {
            var readSheetResult = ReadSheetData(range, sheetId);

            if (readSheetResult.Values != null
                && readSheetResult.Values[0] != null
                && readSheetResult.Values[0][0] != null)
            {
                return Result<object>.Success(readSheetResult.Values[0][0]);
            }

            return Result<object>.Fail(new object(), "Could not find value in that range");
        }
    }
}
