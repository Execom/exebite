﻿using System.Collections.Generic;

namespace Exebite.DtoModels.Meal
{
    public class MealGroupByTypeDto
    {
        public string TypeName { get; set; }

        public IEnumerable<MealDto> Meals { get; set; } = new List<MealDto>();
    }
}
