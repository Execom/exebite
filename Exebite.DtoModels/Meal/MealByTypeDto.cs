﻿using Exebite.DomainModel;
using System.ComponentModel.DataAnnotations;

namespace Exebite.DtoModels.Meal
{
    public class MealByTypeDto : QueryBaseDto
    {
        [Required]
        [Range(1, long.MaxValue)]
        public long CustomerId { get; set; }

        public MealType? MealType { get; set; }
    }
}
