﻿using Exebite.DomainModel;
using System.ComponentModel.DataAnnotations;

namespace Exebite.DtoModels
{
    public class UpdateReactionToMealDto
    {
        [Required]
        public long MealId { get; set; }

        [Required]
        public long CustomerId { get; set; }

        [Required]
        public ReactionType Reaction { get; set; }
    }
}
