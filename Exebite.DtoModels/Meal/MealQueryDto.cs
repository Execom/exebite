﻿using System.ComponentModel.DataAnnotations;

namespace Exebite.DtoModels
{
    public class MealQueryDto : QueryBaseDto
    {
        public long? Id { get; set; }

        public long? RestaurantId { get; set; }

        [Required]
        [Range(1, long.MaxValue)]
        public long CustomerId { get; set; }

        public string Name { get; set; }

        public int? Type { get; set; }
    }
}