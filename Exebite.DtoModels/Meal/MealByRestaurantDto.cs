﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Exebite.DtoModels.Meal
{
    public class MealByRestaurantDto : QueryBaseDto
    {
        [Required]
        [Range(1, long.MaxValue)]
        public long CustomerId { get; set; }

        public long RestaurantId { get; set; }
    }
}
