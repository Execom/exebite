﻿using Exebite.DtoModels.Order;
using System;
using System.Collections.Generic;

namespace Exebite.DtoModels
{
    public class OrderDto
    {
        public int Id { get; set; }

        public decimal Total { get; set; }

        public DateTime Date { get; set; }

        public List<OrderRestaurantDto> Restaurants { get; set; } = new List<OrderRestaurantDto>();
    }
}