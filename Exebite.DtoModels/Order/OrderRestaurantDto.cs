﻿using System.Collections.Generic;

namespace Exebite.DtoModels.Order
{
    public class OrderRestaurantDto
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public List<OrderToMealDto> Meals { get; set; } = new List<OrderToMealDto>();
    }
}
