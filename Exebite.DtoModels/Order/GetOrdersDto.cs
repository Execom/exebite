﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Exebite.DtoModels
{
    public class GetOrdersDto : QueryBaseDto
    {
        [Required]
        [Range(1, long.MaxValue)]
        public long CustomerId { get; set; }
    }
}
