﻿namespace Exebite.DtoModels
{
    public class OrderToMealDto
    {
        public long MealId { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }

        public string Note { get; set; }

        public int Quantity { get; set; }
    }
}
