﻿using System.ComponentModel.DataAnnotations;

namespace Exebite.DtoModels.Comment
{
    public class UpdateCommentDto
    {
        [Required]
        public long MealId { get; set; }

        [Required]
        public int CommentId { get; set; }

        [Required]
        public long CustomerId { get; set; }

        [Required]
        [StringLength(255)]
        public string Text { get; set; }
    }
}
