﻿using System.ComponentModel.DataAnnotations;

namespace Exebite.DtoModels.Comment
{
    public class DeleteCommentReactionDto
    {
        [Required]
        public long MealId { get; set; }

        [Required]
        public int CommentId { get; set; }

        [Required]
        public long CustomerId { get; set; }
    }
}
