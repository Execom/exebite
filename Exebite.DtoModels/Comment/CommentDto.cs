﻿using System;

namespace Exebite.DtoModels.Comment
{
    public class CommentDto
    {
        public int CommentId { get; set; }

        public string Text { get; set; }

        public DateTime Created { get; set; }

        public long NumberOfLikes { get; set; }

        public bool IsLiked { get; set; }

        public CustomerDto Customer { get; set; }

        public MealDto Meal { get; set; }
    }
}
