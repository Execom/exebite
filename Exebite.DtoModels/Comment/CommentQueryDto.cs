﻿using Exebite.DomainModel.Enums;

namespace Exebite.DtoModels.Comment
{
    public class CommentQueryDto : QueryBaseDto
    {
        public long? MealId { get; set; }

        public long? CustomerId { get; set; }

        public int? CommentId { get; set; }

        public CommentSort SortType { get; set; }
    }
}
