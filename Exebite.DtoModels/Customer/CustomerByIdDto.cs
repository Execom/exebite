﻿namespace Exebite.DtoModels
{
    public class CustomerByIdDto
    {
        public long? Id { get; set; }

        public string GoogleUserId { get; set; }
    }
}
