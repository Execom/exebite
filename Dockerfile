FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS restore
WORKDIR /src
COPY ["Exebite.API/Exebite.API.csproj", "Exebite.API/"]
COPY ["Exebite.Business/Exebite.Business.csproj", "Exebite.Business/"]
COPY ["Exebite.Sheets.API/Exebite.Sheets.API.csproj", "Exebite.Sheets.API/"]
COPY ["Exebite.DomainModel/Exebite.DomainModel.csproj", "Exebite.DomainModel/"]
COPY ["Option/Option.csproj", "Option/"]
COPY ["Exebite.GoogleSheetAPI/Exebite.GoogleSheetAPI.csproj", "Exebite.GoogleSheetAPI/"]
COPY ["Exebite.DataAccess/Exebite.DataAccess.csproj", "Exebite.DataAccess/"]
COPY ["Either/Either.csproj", "Either/"]
COPY ["Exebite.Common/Exebite.Common.csproj", "Exebite.Common/"]
COPY ["Exebite.DtoModels/Exebite.DtoModels.csproj", "Exebite.DtoModels/"]
RUN dotnet restore "Exebite.API/Exebite.API.csproj"

FROM restore AS build
WORKDIR /src
COPY . .
WORKDIR "/src/Exebite.API"
RUN dotnet publish "Exebite.API.csproj" -c Release -o /app

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
EXPOSE 50586
WORKDIR /app
COPY --from=build /app .
CMD ["dotnet", "Exebite.API.dll"]