﻿using System;
using System.Collections.Generic;
using Exebite.Common;
using Exebite.DataAccess.Context;
using Exebite.DataAccess.Entities;
using Exebite.DataAccess.Repositories;
using Exebite.DomainModel;

namespace FeatureTestingConsole
{
    public sealed class App : IApp
    {
        private readonly IRestaurantCommandRepository _restaurantCommandRepository;
        private readonly ILocationCommandRepository _locationCommandRepo;
        private readonly IMealOrderingContextFactory _factory;

        public App(
            IRestaurantCommandRepository restaurantCommandRepo,
            ILocationCommandRepository locationCommandRepo,
            IMealOrderingContextFactory factory)
        {
            _restaurantCommandRepository = restaurantCommandRepo;
            _locationCommandRepo = locationCommandRepo;
            _factory = factory;
        }

        public void Run(string[] args)
        {
            ResetDatabase();

            SeedLocation();

            SeedReactions();
        }

        private void ResetDatabase()
        {
            using (var dc = _factory.Create())
            {
                dc.Database.EnsureDeleted();
                dc.Database.EnsureCreated();
            }
        }

        private void SeedLocation()
        {
            _locationCommandRepo.Insert(new LocationInsertModel()
            {
                Name = "BVS",
                Address = "Vojvode Stepe 50"
            });

            _locationCommandRepo.Insert(new LocationInsertModel()
            {
                Name = "MM",
                Address = "Đorđa Rajkovića 2"
            });
        }

        private void SeedReactions()
        {
            using (var context = _factory.Create())
            {
                var reactions = new List<ReactionEntity>
                {
                    new ReactionEntity
                    {
                        Description = Enum.GetName(typeof(ReactionType), ReactionType.Like)
                    },
                    new ReactionEntity
                    {
                        Description = Enum.GetName(typeof(ReactionType), ReactionType.Dislike)
                    },
                    new ReactionEntity
                    {
                        Description = Enum.GetName(typeof(ReactionType), ReactionType.None)
                    }
                };

                context.Reaction.AddRange(reactions);
                context.SaveChanges();
            }
        }
    }
}
