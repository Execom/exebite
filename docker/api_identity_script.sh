#! /bin/bash
# Create directory to which we copy source code files and restore, build and publish Project files
if [ -d /code ]
then
    rm -rf /code
else
    mkdir /code
fi
cp -Rf /source/. /code &&
cd /code/Exebite.API && dotnet publish "Exebite.API.csproj" -c Release -o /code/build/api/ &&
cd /code/Exebite.IdentityServer && dotnet publish "Exebite.IdentityServer.csproj" -c Release -o /code/build/identity/ &&
cd /code/FeatureTestingConsole && dotnet publish "FeatureTestingConsole.csproj" -c Release -o /code/build/FeatureTestingConsole/ &&
# Copy build artifacts to local file system 
cp -Rf /code/build/. /source/docker/artifact/ && rm -rf /code