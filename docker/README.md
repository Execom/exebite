# Exebite Docker environment deployment guide

**DISCLAIMER**
This Docker environment deployment is still not finished, since there is a lack of project service information, to debug issues that are present with Identity server. This deployment creates six containers that have all of the services (described below) operational as units, but when the Login page is accessed, and Login button is selected, Identity server redirection fails.

## Docker installation

These are references for installing Docker environment on you Windows/Linux/MacOSX host

### Windows

- [Official Docker Guide](https://docs.docker.com/docker-for-windows/install/)
- _For best performance it's advisable to use WSL2 option in Docker settings_. More information on [this link](https://docs.docker.com/docker-for-windows/wsl/)

### Linux

- [Official Docker Guide](https://docs.docker.com/engine/install/) _Choose your Linux platform from guide link for appropriate instructions_
- Docker Compose needs to be installed separately on Linux. More info [here](https://docs.docker.com/compose/install/#install-compose-on-linux-systems)

### MacOSX

- [Official Docker Guide](https://docs.docker.com/docker-for-mac/install/)

## Running Exebite Docker environment

- When you pull latest _Git project code_, all Docker configuration files are located in **/docker folder**:

  - **artifact** folder will contain build artifacts for Exebite.API (_backend_), Identity server (_Google auth proxy_) and FeatureTestingConsole (_initial DB tool_) projects.
  - **appsettings** folder contains application properties files, that need to be replaced in current source code.
  - **docker-compose.yaml** file creates **exebite_build** (build server), **exebite_sql** (mysql server) and **exebite_initial_db** (initial DB tool), **exebite_identity** (Identity server), **exebite_api** (Backend) and **exebite_front** (Frontend) containers that are needed for specific stages of _exebite environment_ initialization.

- Before you start running containers, you can use application property files from **appsettings folder** to replace _origin source code_ files, but you need to update the correct connection string and Google client ID information in them. **Before you copy/replace all the files, update username and password SQL DB credentials** that you choose to set in **dockerfile_sql** script.
- Navigate in terminal to project source code **/docker** folder (_cd /project_source_code/docker_)

\$ docker-compose up -d exebite_build
(If successful this **creates exebite_build container**, and when build is complete **exebite_build container will stop** and there should be three folders (**api, FeatureTestingConsole and identity**) in **artifact** folder.

\$ docker-compose up -d exebite_sql<br/>(If successful this will build custom image and create **Microsoft SQL DB instance in container with exebite database**)

\$ docker-compose up -d exebite_initial_db<br/>(If successful this will **connect to previously created exebite database and create database structure**)

\$ docker-compose up -d exebite_identity exebite_api exebite_frontend<br/>(If successful this will **Identity server, Backend and Frontend** separate containers)

- When all off containers finish there run jobs, Exebite login page should be available on **localhost:4200** in browser

## Docker reference documentation:

- [Docker Overview](https://docs.docker.com/get-started/overview/)
- [Docker file](https://docs.docker.com/engine/reference/builder/#add)
- [Compose file](https://docs.docker.com/compose/compose-file/#volumes)

## Note

**In the current Docker for Desktop 2.3.0.4 version and WSL2 Windows 10 2004 version, there is an issue with r/w speed of bind mount, between Docker and local host files, so avoid direct code builds/executions from bind mounts.**
