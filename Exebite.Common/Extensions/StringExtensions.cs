﻿using System;
using System.Linq;

namespace Exebite.Common.Extensions
{
    public static class StringExtensions
    {
        /// <summary>
        /// Capitalize string
        /// </summary>
        /// <param name="input">String that will be capitalized</param>
        /// <returns>Capitalized string</returns>
        public static string Capitalize(this string input)
        {
            return input.TrimStart() switch
            {
                null => throw new ArgumentNullException(nameof(input)),
                "" => throw new ArgumentException($"{nameof(input)} cannot be empty", nameof(input)),
                _ => input.First().ToString().ToUpper() + input.Substring(1)
            };
        }
    }
}
