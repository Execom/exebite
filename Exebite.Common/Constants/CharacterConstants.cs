﻿namespace Exebite.Common.Constants
{
    public class CharacterConstants
    {
        public const char DOT_CHAR = '.';
        public const char SEMICOLON_CHAR = ':';
        public const char WHITESPACE_CHAR = ' ';
    }
}
