﻿namespace Exebite.Common.Constants
{
    public class DateTimeConstants
    {
        public const string DOT_HOUR_FORMAT = "HH.mm";
        public const string SEMICOLON_HOUR_FORMAT = "HH:mm";
        public const string SHORT_DATE_FORMAT = "dd-MMM-yyyy";
        public const string REVERSE_SHORT_DATE_FORMAT = "yyyy-MMM-dd";
    }
}
