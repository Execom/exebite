﻿namespace Exebite.Common
{
    public abstract class Error
    {
        public string Message { get; set; }
    }
}