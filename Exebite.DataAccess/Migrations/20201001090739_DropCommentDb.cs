﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Exebite.DataAccess.Migrations
{
    public partial class DropCommentDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Comment");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Comment",
                columns: table => new
                {
                    MealId = table.Column<long>(nullable: false),
                    CommentId = table.Column<int>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    CustomerId = table.Column<long>(nullable: false),
                    LastModified = table.Column<DateTime>(nullable: false),
                    NumberOfLikes = table.Column<long>(nullable: false),
                    Text = table.Column<string>(maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comment", x => new { x.MealId, x.CommentId });
                    table.ForeignKey(
                        name: "FK_Comment_Customer_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Comment_Meal_MealId",
                        column: x => x.MealId,
                        principalTable: "Meal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Comment_CustomerId",
                table: "Comment",
                column: "CustomerId");
        }
    }
}
