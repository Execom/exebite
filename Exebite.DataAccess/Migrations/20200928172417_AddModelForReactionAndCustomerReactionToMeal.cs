﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Exebite.DataAccess.Migrations
{
    public partial class AddModelForReactionAndCustomerReactionToMeal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ReactionToMeal",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    LastModified = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReactionToMeal", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CustomerReactionToMeal",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MealId = table.Column<long>(nullable: false),
                    CustomerId = table.Column<long>(nullable: false),
                    ReactionToMealId = table.Column<long>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    LastModified = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerReactionToMeal", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CustomerReactionToMeal_Customer_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CustomerReactionToMeal_Meal_MealId",
                        column: x => x.MealId,
                        principalTable: "Meal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CustomerReactionToMeal_ReactionToMeal_ReactionToMealId",
                        column: x => x.ReactionToMealId,
                        principalTable: "ReactionToMeal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CustomerReactionToMeal_CustomerId",
                table: "CustomerReactionToMeal",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerReactionToMeal_MealId",
                table: "CustomerReactionToMeal",
                column: "MealId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerReactionToMeal_ReactionToMealId",
                table: "CustomerReactionToMeal",
                column: "ReactionToMealId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CustomerReactionToMeal");

            migrationBuilder.DropTable(
                name: "ReactionToMeal");
        }
    }
}
