﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Exebite.DataAccess.Migrations
{
    public partial class AddFksOnReactionToComment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_ReactionToComment_CustomerId",
                table: "ReactionToComment",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_ReactionToComment_ReactionId",
                table: "ReactionToComment",
                column: "ReactionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_ReactionToComment_CustomerId",
                table: "ReactionToComment");

            migrationBuilder.DropIndex(
                name: "IX_ReactionToComment_ReactionId",
                table: "ReactionToComment");
        }
    }
}
