﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Exebite.DataAccess.Migrations
{
    public partial class ChangedNameToReactionEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CustomerReactionToMeal_ReactionToMeal_ReactionToMealId",
                table: "CustomerReactionToMeal");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ReactionToMeal",
                table: "ReactionToMeal");

            migrationBuilder.RenameTable(
                name: "ReactionToMeal",
                newName: "Reaction");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Reaction",
                table: "Reaction",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerReactionToMeal_Reaction_ReactionToMealId",
                table: "CustomerReactionToMeal",
                column: "ReactionToMealId",
                principalTable: "Reaction",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CustomerReactionToMeal_Reaction_ReactionToMealId",
                table: "CustomerReactionToMeal");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Reaction",
                table: "Reaction");

            migrationBuilder.RenameTable(
                name: "Reaction",
                newName: "ReactionToMeal");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ReactionToMeal",
                table: "ReactionToMeal",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerReactionToMeal_ReactionToMeal_ReactionToMealId",
                table: "CustomerReactionToMeal",
                column: "ReactionToMealId",
                principalTable: "ReactionToMeal",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
