﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Exebite.DataAccess.Migrations
{
    public partial class OrderToMealPkChanged : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_OrderToMeal",
                table: "OrderToMeal");

            migrationBuilder.DropIndex(
                name: "IX_OrderToMeal_OrderId",
                table: "OrderToMeal");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "OrderToMeal");

            migrationBuilder.AddPrimaryKey(
                name: "PK_OrderToMeal",
                table: "OrderToMeal",
                columns: new[] { "OrderId", "MealId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_OrderToMeal",
                table: "OrderToMeal");

            migrationBuilder.AddColumn<long>(
                name: "Id",
                table: "OrderToMeal",
                nullable: false,
                defaultValue: 0L)
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_OrderToMeal",
                table: "OrderToMeal",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_OrderToMeal_OrderId",
                table: "OrderToMeal",
                column: "OrderId");
        }
    }
}
