﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Exebite.DataAccess.Migrations
{
    public partial class ReactionToMealCreated : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ReactionToComment",
                columns: table => new
                {
                    MealId = table.Column<long>(nullable: false),
                    CommentId = table.Column<int>(nullable: false),
                    CustomerId = table.Column<long>(nullable: false),
                    ReactionId = table.Column<long>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    LastModified = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReactionToComment", x => new { x.MealId, x.CommentId, x.CustomerId, x.ReactionId });
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ReactionToComment");
        }
    }
}
