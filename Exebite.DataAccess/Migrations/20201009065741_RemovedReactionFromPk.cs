﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Exebite.DataAccess.Migrations
{
    public partial class RemovedReactionFromPk : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ReactionToMeal",
                table: "ReactionToMeal");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ReactionToMeal",
                table: "ReactionToMeal",
                columns: new[] { "MealId", "CustomerId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ReactionToMeal",
                table: "ReactionToMeal");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ReactionToMeal",
                table: "ReactionToMeal",
                columns: new[] { "MealId", "CustomerId", "ReactionId" });
        }
    }
}
