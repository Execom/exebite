﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Exebite.DataAccess.Migrations
{
    public partial class ReactionToMealPkChanged : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ReactionToMeal",
                table: "ReactionToMeal");

            migrationBuilder.DropIndex(
                name: "IX_ReactionToMeal_MealId",
                table: "ReactionToMeal");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "ReactionToMeal");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ReactionToMeal",
                table: "ReactionToMeal",
                columns: new[] { "MealId", "CustomerId", "ReactionId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ReactionToMeal",
                table: "ReactionToMeal");

            migrationBuilder.AddColumn<long>(
                name: "Id",
                table: "ReactionToMeal",
                nullable: false,
                defaultValue: 0L)
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ReactionToMeal",
                table: "ReactionToMeal",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_ReactionToMeal_MealId",
                table: "ReactionToMeal",
                column: "MealId");
        }
    }
}
