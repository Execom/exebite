﻿using Exebite.DataAccess.Entities;
using Microsoft.EntityFrameworkCore;

namespace Exebite.DataAccess.Context
{
    public interface IMealOrderingContext
    {
        DbSet<CustomerEntity> Customer { get; set; }

        DbSet<LocationEntity> Location { get; set; }

        DbSet<MealEntity> Meal { get; set; }

        DbSet<OrderEntity> Order { get; set; }

        DbSet<RestaurantEntity> Restaurant { get; set; }

        DbSet<CommentEntity> Comment { get; set; }

        DbSet<ReactionToMealEntity> ReactionToMeal { get; set; }

        DbSet<ReactionEntity> Reaction { get; set; }

        DbSet<NotificationEntity> Notification { get; set; }
    }
}