﻿namespace Exebite.DataAccess.Constants
{
    public class NotificationConstants
    {
        public const string RESTAURANT_NOTIFICATION = "Od danas u ponudi za ručak imamo novi restoran RestaurantName .";
        public const string PAYMENT_NOTIFICATION = "Trenutno stanje na tvom računu za ručak je ReplaceAmount dinara." +
                                                   " Molim te, dopuni ga kako bismo na vreme izmirivali svoje obaveze prema restoranima. Hvala!:)";

        public const string REPLACE_AMOUNT = "ReplaceAmount";
        public const string RESTAURANT_NAME = "RestaurantName";
    }
}
