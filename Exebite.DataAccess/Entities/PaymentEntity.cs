﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Exebite.DataAccess.Entities
{
    [Table("Payment")]
    public class PaymentEntity
    {
        public long Id { get; set; }

        public long CustomerId { get; set; }

        public DateTime Date { get; set; }

        public decimal Amount { get; set; }

        [ForeignKey(nameof(CustomerId))]
        public virtual CustomerEntity Customer { get; set; }
    }
}
