﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Exebite.DataAccess.Entities
{
    [Table("Order")]
    public class OrderEntity
    {
        public long Id { get; set; }

        public short LocationId { get; set; }

        public long CustomerId { get; set; }

        public decimal Total { get; set; }

        public DateTime Date { get; set; }

        [ForeignKey(nameof(CustomerId))]
        public virtual CustomerEntity Customer { get; set; }

        [ForeignKey(nameof(LocationId))]
        public virtual LocationEntity Location { get; set; }

        public virtual List<OrderToMealEntity> OrdersToMeals { get; set; } = new List<OrderToMealEntity>();
    }
}
