﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Exebite.DataAccess.Entities
{
    [Table("NotificationsHistory")]
    public class NotificationEntity
    {
        [Key]
        public long Id { get; set; }

        public string NotificationMessage { get; set; }

        public DateTime Created { get; set; }

        public DateTime LastModified { get; set; }

        [ForeignKey(nameof(Customer))]
        public long CustomerId { get; set; }

        public virtual CustomerEntity Customer { get; set; }
    }
}
