﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Exebite.DataAccess.Entities
{
    [Table("Reaction")]
    public class ReactionEntity
    {
        [Key]
        public long Id { get; set; }

        public string Description { get; set; }
    }
}
