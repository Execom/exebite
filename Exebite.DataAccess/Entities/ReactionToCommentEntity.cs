﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Exebite.DataAccess.Entities
{
    [Table("ReactionToComment")]
    public class ReactionToCommentEntity
    {
        public long MealId { get; set; }

        public int CommentId { get; set; }

        public long CustomerId { get; set; }

        public long ReactionId { get; set; }

        public virtual CommentEntity Comment { get; set; }

        [ForeignKey(nameof(CustomerId))]
        public virtual CustomerEntity Customer { get; set; }

        [ForeignKey(nameof(ReactionId))]
        public virtual ReactionEntity Reaction { get; set; }
    }
}
