﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Exebite.DataAccess.Entities
{
    [Table("CustomerToRestaurant")]
    public class CustomerToRestaurantEntity
    {
        public long CustomerId { get; set; }

        public long RestaurantId { get; set; }

        [ForeignKey(nameof(CustomerId))]
        public virtual CustomerEntity Customer { get; set; }

        [ForeignKey(nameof(RestaurantId))]
        public virtual RestaurantEntity Restaurant { get; set; }
    }
}
