﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Exebite.DataAccess.Entities
{
    [Table("ReactionToMeal")]
    public class ReactionToMealEntity
    {
        public long MealId { get; set; }

        public long CustomerId { get; set; }

        public long ReactionId { get; set; }

        [ForeignKey(nameof(MealId))]
        public virtual MealEntity Meal { get; set; }

        [ForeignKey(nameof(CustomerId))]
        public virtual CustomerEntity Customer { get; set; }

        [ForeignKey(nameof(ReactionId))]
        public virtual ReactionEntity Reaction { get; set; }
    }
}
