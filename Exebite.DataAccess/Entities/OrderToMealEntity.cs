﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Exebite.DataAccess.Entities
{
    [Table("OrderToMeal")]
    public class OrderToMealEntity
    {
        public long OrderId { get; set; }

        public long MealId { get; set; }

        public int Quantity { get; set; }

        public string Note { get; set; }

        [ForeignKey(nameof(OrderId))]
        public virtual OrderEntity Order { get; set; }

        [ForeignKey(nameof(MealId))]
        public virtual MealEntity Meal { get; set; }
    }
}
