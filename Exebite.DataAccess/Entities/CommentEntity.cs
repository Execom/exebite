﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Exebite.DataAccess.Entities
{
    [Table("Comment")]
    public class CommentEntity
    {
        public int CommentId { get; set; }

        public long MealId { get; set; }

        public long CustomerId { get; set; }

        [Required]
        [StringLength(255)]
        public string Text { get; set; }

        public DateTime Created { get; set; }

        [ForeignKey(nameof(MealId))]
        public virtual MealEntity Meal { get; set; }

        [ForeignKey(nameof(CustomerId))]
        public virtual CustomerEntity Customer { get; set; }

        public virtual List<ReactionToCommentEntity> CommentReactions { get; set; } = new List<ReactionToCommentEntity>();
    }
}
