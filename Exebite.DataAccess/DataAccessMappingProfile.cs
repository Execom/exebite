﻿using System.Linq;
using AutoMapper;
using Exebite.DataAccess.Entities;
using Exebite.DataAccess.Repositories;
using Exebite.DomainModel;
using Exebite.DtoModels;

namespace Exebite.DataAccess
{
    public class DataAccessMappingProfile : Profile
    {
        public DataAccessMappingProfile()
        {
            CreateMap<DailyMenuEntity, DailyMenu>()
                .ForMember(dm => dm.Meals, e => e.MapFrom(p => p.DailyMenuToMeals.Select(dm => dm.Meal)));
            CreateMap<LocationEntity, Location>();
            CreateMap<MealEntity, Meal>()
                .ForMember(m => m.Condiments, e => e.MapFrom(p => p.Condiments.Select(c => c.Condiment)));
            CreateMap<CustomerEntity, Customer>()
                .ForMember(m => m.FavouriteRestaurants, e => e.MapFrom(p => p.FavouriteRestaurants.Select(r => r.Restaurant)));
            CreateMap<PaymentEntity, Payment>();
            CreateMap<CreateMealDto, MealInsertModel>();
            CreateMap<UpdateMealDto, MealUpdateModel>();
        }

        public override string ProfileName => "DataAccessMappingProfile";
    }
}
