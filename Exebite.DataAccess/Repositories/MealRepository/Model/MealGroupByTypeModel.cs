﻿using System;
using System.ComponentModel.DataAnnotations;
using Exebite.DomainModel;

namespace Exebite.DataAccess.Repositories
{
    public class MealGroupByTypeModel : QueryBase
    {
        public MealGroupByTypeModel()
            : base()
        {
        }

        public MealGroupByTypeModel(int page, int size)
            : base(page, size)
        {
        }

        [Required]
        [Range(1, long.MaxValue)]
        public long CustomerId { get; set; }

        public MealType? MealType { get; set; }
    }
}
