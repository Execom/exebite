﻿using Exebite.DomainModel;

namespace Exebite.DataAccess.Repositories
{
    public class ReactionToMealInsertModel
    {
        public long MealId { get; set; }

        public long CustomerId { get; set; }

        public ReactionType Reaction { get; set; }
    }
}
