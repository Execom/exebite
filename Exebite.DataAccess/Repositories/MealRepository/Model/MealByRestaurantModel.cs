﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Exebite.DataAccess.Repositories
{
    public class MealByRestaurantModel : QueryBase
    {
        public MealByRestaurantModel()
            : base()
        {
        }

        public MealByRestaurantModel(int page, int size)
            : base(page, size)
        {
        }

        [Required]
        [Range(1, int.MaxValue)]
        public long RestaurantId { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public long CustomerId { get; set; }
    }
}
