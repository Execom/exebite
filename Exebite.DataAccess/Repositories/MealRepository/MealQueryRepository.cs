﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Either;
using Exebite.Common;
using Exebite.DataAccess.Context;
using Exebite.DomainModel;
using Microsoft.EntityFrameworkCore;

namespace Exebite.DataAccess.Repositories
{
    public class MealQueryRepository : IMealQueryRepository
    {
        private readonly IMapper _mapper;
        private readonly IMealOrderingContextFactory _factory;

        public MealQueryRepository(IMealOrderingContextFactory factory, IMapper mapper)
        {
            _factory = factory ?? throw new ArgumentNullException(nameof(factory));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public Either<Error, PagingResult<Meal>> Query(MealQueryModel queryModel)
        {
            try
            {
                if (queryModel == null)
                {
                    return new Left<Error, PagingResult<Meal>>(new ArgumentNotSet(nameof(queryModel)));
                }

                using (var context = _factory.Create())
                {
                    var query = context.Meal.Include(m => m.ReactionsToMeals).Where(x => x.Type != (int)MealType.CONDIMENT).AsQueryable();

                    if (queryModel.Id != null)
                    {
                        query = query.Where(x => x.Id == queryModel.Id.Value);
                    }

                    if (queryModel.RestaurantId != null)
                    {
                        query = query.Where(x => x.RestaurantId == queryModel.RestaurantId.Value);
                    }

                    if (!string.IsNullOrWhiteSpace(queryModel.Name))
                    {
                        query = query.Where(x => x.Name == queryModel.Name);
                    }

                    if (queryModel.Type != null)
                    {
                        query = query.Where(x => x.Type == queryModel.Type.Value);
                    }

                    var reactionToMeals = context.ReactionToMeal.AsQueryable();
                    var result = query.Select(x => new Meal
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Price = x.Price,
                        Description = x.Description,
                        IsActive = x.Restaurant.OrderDue > DateTime.Now,
                        IsFromStandardMenu = x.IsFromStandardMenu,
                        Type = x.Type,
                        Note = x.Note,
                        Condiments = _mapper.Map<List<Meal>>(x.Condiments.Select(c => new Meal
                        {
                            Id = c.Condiment.Id,
                            Name = c.Condiment.Name,
                            Price = c.Condiment.Price,
                            IsActive = c.Condiment.Restaurant.OrderDue > DateTime.Now
                        }).Where(condiment => condiment.Price == 0)),
                        Restaurant = _mapper.Map<Restaurant>(x.Restaurant),
                        NumberOfLikes = reactionToMeals.Count(l => l.MealId == x.Id && l.Reaction.Description.Equals(Enum.GetName(typeof(ReactionType), ReactionType.Like))),
                        NumberOfDislikes = reactionToMeals.Count(l => l.MealId == x.Id && l.Reaction.Description.Equals(Enum.GetName(typeof(ReactionType), ReactionType.Dislike))),
                        CustomerReactionToMeal = x.ReactionsToMeals.Any(rm => rm.MealId == x.Id && rm.CustomerId == queryModel.CustomerId) ? (ReactionType)Enum.Parse(typeof(ReactionType), x.ReactionsToMeals.Single(rm => rm.MealId == x.Id && rm.CustomerId == queryModel.CustomerId).Reaction.Description) : ReactionType.None
                    });

                    var total = result.Count();

                    result = result
                        .Skip((queryModel.Page - 1) * queryModel.Size)
                        .Take(queryModel.Size);

                    var mapped = result.ToList();

                    return new Right<Error, PagingResult<Meal>>(new PagingResult<Meal>(mapped, total));
                }
            }
            catch (Exception ex)
            {
                return new Left<Error, PagingResult<Meal>>(new UnknownError(ex.ToString()));
            }
        }

        public Either<Error, long> FindByNameAndRestaurantId(MealQueryModel queryModel)
        {
            try
            {
                if (queryModel == null)
                {
                    return new Left<Error, long>(new ArgumentNotSet(nameof(queryModel)));
                }

                using (var ctx = _factory.Create())
                {
                    var record = ctx.Meal.FirstOrDefault(c =>
                        c.Name.Equals(queryModel.Name, StringComparison.OrdinalIgnoreCase) &&
                        c.RestaurantId == queryModel.RestaurantId);

                    return new Right<Error, long>(record?.Id ?? 0);
                }
            }
            catch (Exception ex)
            {
                return new Left<Error, long>(new UnknownError(ex.ToString()));
            }
        }

        public Either<Error, PagingResult<Meal>> GetCondimentsForMeal(MealQueryModel queryModel)
        {
            try
            {
                if (queryModel == null)
                {
                    return new Left<Error, PagingResult<Meal>>(new ArgumentNotSet(nameof(queryModel)));
                }

                using (var ctx = _factory.Create())
                {
                    var condiments = ctx.Meal.FirstOrDefault(m => m.Id == queryModel.Id).Condiments.Select(c => c.Condiment);
                    var result = _mapper.Map<IList<Meal>>(condiments);

                    return new Right<Error, PagingResult<Meal>>(new PagingResult<Meal>(result, result.Count()));
                }
            }
            catch (Exception ex)
            {
                return new Left<Error, PagingResult<Meal>>(new UnknownError(ex.ToString()));
            }
        }

        public Either<Error, PagingResult<Meal>> GetMealsByRestaurantId(MealByRestaurantModel queryModel)
        {
            try
            {
                if (queryModel == null)
                {
                    return new Left<Error, PagingResult<Meal>>(new ArgumentNotSet(nameof(queryModel)));
                }

                using (var context = _factory.Create())
                {
                    if (context.Restaurant.Find(queryModel.RestaurantId) == null || context.Customer.Find(queryModel.CustomerId) == null)
                    {
                        return new Left<Error, PagingResult<Meal>>(new ArgumentNotSet(nameof(queryModel)));
                    }

                    var query = context.Meal.Where(x => x.RestaurantId == queryModel.RestaurantId).AsQueryable();

                    var reactionToMeals = context.ReactionToMeal.AsQueryable();

                    var result = query
                        .Where(x => x.Type != (int)MealType.SOUP && x.Type != (int)MealType.CONDIMENT)
                        .Select(x => new Meal
                        {
                            Id = x.Id,
                            Name = x.Name,
                            Price = x.Price,
                            Description = x.Description,
                            IsActive = x.Restaurant.OrderDue > DateTime.Now,
                            IsFromStandardMenu = x.IsFromStandardMenu,
                            Type = x.Type,
                            Note = x.Note,
                            Condiments = _mapper.Map<List<Meal>>(x.Condiments.Select(c => new Meal
                            {
                                Id = c.Condiment.Id,
                                Name = c.Condiment.Name,
                                Price = c.Condiment.Price,
                                IsActive = c.Condiment.Restaurant.OrderDue > DateTime.Now
                            }).Where(condiment => condiment.Price == 0)),
                            Restaurant = _mapper.Map<Restaurant>(x.Restaurant),
                            NumberOfLikes = reactionToMeals.Count(l => l.MealId == x.Id && l.Reaction.Description.Equals(Enum.GetName(typeof(ReactionType), ReactionType.Like))),
                            NumberOfDislikes = reactionToMeals.Count(l => l.MealId == x.Id && l.Reaction.Description.Equals(Enum.GetName(typeof(ReactionType), ReactionType.Dislike))),
                            CustomerReactionToMeal = x.ReactionsToMeals.Any(rm => rm.MealId == x.Id && rm.CustomerId == queryModel.CustomerId) ? (ReactionType)Enum.Parse(typeof(ReactionType), x.ReactionsToMeals.Single(rm => rm.MealId == x.Id && rm.CustomerId == queryModel.CustomerId).Reaction.Description) : ReactionType.None
                        });

                    var total = result.Count();

                    result = result
                        .Skip((queryModel.Page - 1) * queryModel.Size)
                        .Take(queryModel.Size);

                    return new Right<Error, PagingResult<Meal>>(new PagingResult<Meal>(result.ToList(), total));
                }
            }
            catch (Exception ex)
            {
                return new Left<Error, PagingResult<Meal>>(new UnknownError(ex.ToString()));
            }
        }

        public Either<Error, PagingResult<MealGroupByType>> GetMealsByType(MealGroupByTypeModel queryModel)
        {
            try
            {
                if (queryModel == null)
                {
                    return new Left<Error, PagingResult<MealGroupByType>>(new ArgumentNotSet(nameof(queryModel)));
                }

                using (var context = _factory.Create())
                {
                    if (context.Customer.Find(queryModel.CustomerId) == null)
                    {
                        return new Left<Error, PagingResult<MealGroupByType>>(new ArgumentNotSet(nameof(queryModel)));
                    }

                    var query = context.Meal.AsQueryable();

                    var reactionToMeals = context.ReactionToMeal.AsQueryable();

                    if (queryModel.MealType != null)
                    {
                        query = query.Where(x => x.Type == (int)queryModel.MealType.Value);
                    }

                    var result = query.GroupBy(x => Enum.GetName(typeof(MealType), x.Type))
                        .Select(x => new
                        {
                            TypeName = x.Key,
                            Meals = x.Select(m => new
                            {
                                m.Id,
                                m.Name,
                                m.Type,
                                m.Price,
                                m.Description,
                                m.Note,
                                IsActive = m.Restaurant.OrderDue > DateTime.Now,
                                m.IsFromStandardMenu,
                                NumberOfLikes = reactionToMeals.Count(l => l.MealId == m.Id && l.Reaction.Description.Equals(Enum.GetName(typeof(ReactionType), ReactionType.Like))),
                                NumberOfDislikes = reactionToMeals.Count(l => l.MealId == m.Id && l.Reaction.Description.Equals(Enum.GetName(typeof(ReactionType), ReactionType.Dislike))),
                                CustomerReactionToMeal = m.ReactionsToMeals.Any(rm => rm.MealId == m.Id && rm.CustomerId == queryModel.CustomerId) ? (ReactionType)Enum.Parse(typeof(ReactionType), m.ReactionsToMeals.Single(rm => rm.MealId == m.Id && rm.CustomerId == queryModel.CustomerId).Reaction.Description) : ReactionType.None,
                                Restaurant = _mapper.Map<Restaurant>(m.Restaurant),
                                Condiments = _mapper.Map<List<Meal>>(m.Condiments.Select(c => new Meal
                                {
                                    Id = c.Condiment.Id,
                                    Name = c.Condiment.Name,
                                    Price = c.Condiment.Price,
                                    IsActive = c.Condiment.Restaurant.OrderDue > DateTime.Now
                                }).Where(condiment => condiment.Price == 0))
                            }).Skip((queryModel.Page - 1) * queryModel.Size)
                              .Take(queryModel.Size).ToList()
                        });

                    var mapped = _mapper.Map<IList<MealGroupByType>>(result).ToList();

                    var total = result.Count();

                    return new Right<Error, PagingResult<MealGroupByType>>(new PagingResult<MealGroupByType>(mapped, total));
                }
            }
            catch (Exception ex)
            {
                return new Left<Error, PagingResult<MealGroupByType>>(new UnknownError(ex.ToString()));
            }
        }

        public Either<Error, PagingResult<Meal>> GetSoupsAndSideDishesByRestaurantId(MealByRestaurantModel queryModel)
        {
            try
            {
                if (queryModel == null)
                {
                    return new Left<Error, PagingResult<Meal>>(new ArgumentNotSet(nameof(queryModel)));
                }

                using (var context = _factory.Create())
                {
                    if (context.Restaurant.Find(queryModel.RestaurantId) == null || context.Customer.Find(queryModel.CustomerId) == null)
                    {
                        return new Left<Error, PagingResult<Meal>>(new ArgumentNotSet(nameof(queryModel)));
                    }

                    var query = context.Meal.Where(x => x.RestaurantId == queryModel.RestaurantId).AsQueryable();

                    var reactionToMeals = context.ReactionToMeal.AsQueryable();

                    var result = query
                        .Where(x => x.Price > 0 && (x.Type == (int)MealType.SOUP || x.Type == (int)MealType.CONDIMENT))
                        .Select(x => new Meal
                        {
                            Id = x.Id,
                            Name = x.Name,
                            Price = x.Price,
                            Description = x.Description,
                            IsActive = x.Restaurant.OrderDue > DateTime.Now,
                            IsFromStandardMenu = x.IsFromStandardMenu,
                            Type = x.Type,
                            Note = x.Note,
                            Condiments = _mapper.Map<List<Meal>>(x.Condiments.Select(c => new Meal
                            {
                                Id = c.Condiment.Id,
                                Name = c.Condiment.Name,
                                Price = c.Condiment.Price,
                                IsActive = c.Condiment.Restaurant.OrderDue > DateTime.Now
                            }).Where(condiment => condiment.Price == 0)),
                            Restaurant = _mapper.Map<Restaurant>(x.Restaurant),
                            NumberOfLikes = reactionToMeals.Count(l => l.MealId == x.Id && l.Reaction.Description.Equals(Enum.GetName(typeof(ReactionType), ReactionType.Like))),
                            NumberOfDislikes = reactionToMeals.Count(l => l.MealId == x.Id && l.Reaction.Description.Equals(Enum.GetName(typeof(ReactionType), ReactionType.Dislike))),
                            CustomerReactionToMeal = x.ReactionsToMeals.Any(rm => rm.MealId == x.Id && rm.CustomerId == queryModel.CustomerId) ? (ReactionType)Enum.Parse(typeof(ReactionType), x.ReactionsToMeals.Single(rm => rm.MealId == x.Id && rm.CustomerId == queryModel.CustomerId).Reaction.Description) : ReactionType.None
                        });

                    var total = result.Count();

                    result = result
                        .Skip((queryModel.Page - 1) * queryModel.Size)
                        .Take(queryModel.Size);

                    return new Right<Error, PagingResult<Meal>>(new PagingResult<Meal>(result.ToList(), total));
                }
            }
            catch (Exception ex)
            {
                return new Left<Error, PagingResult<Meal>>(new UnknownError(ex.ToString()));
            }
        }
    }
}
