﻿using Either;
using Exebite.Common;
using Exebite.DomainModel;

namespace Exebite.DataAccess.Repositories
{
    public interface INotificationQueryRepository : IDatabaseQueryRepository<Notification, NotificationQueryModel>
    {
        Either<Error, long> ReturnNumberOfNewNotifications(long customerId);
    }
}
