﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Either;
using Exebite.Common;
using Exebite.DataAccess.Constants;
using Exebite.DataAccess.Context;
using Exebite.DataAccess.Entities;
using Microsoft.EntityFrameworkCore;

namespace Exebite.DataAccess.Repositories.NotificationRepository
{
    public class NotificationCommandRepository : INotificationCommandRepository
    {
        private readonly IMealOrderingContextFactory _factory;

        public NotificationCommandRepository(IMealOrderingContextFactory factory)
        {
            _factory = factory ?? throw new ArgumentNullException(nameof(factory));
        }

        public Either<Error, bool> Update(long id, NotificationUpdateModel entity)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Delete notification entities by customerId
        /// </summary>
        /// <param name="customerId">Properties to filter by</param>
        /// <returns>Returns a logical value about the deletion outcome</returns>
        public Either<Error, bool> Delete(long customerId)
        {
            try
            {
                using (var context = _factory.Create())
                {
                    var notifications = context.Notification.Where(x => x.CustomerId == customerId);
                    if (notifications.Any())
                    {
                        context.Notification.RemoveRange(notifications);
                        context.SaveChanges();
                    }

                    return new Right<Error, bool>(true);
                }
            }
            catch (Exception ex)
            {
                return new Left<Error, bool>(new UnknownError(ex.ToString()));
            }
        }

        /// <summary>
        /// Inserts new payment notification entity
        /// </summary>
        /// <param name="entity">Properties that notification entity has</param>
        /// <returns>Id of a new entity</returns>
        public Either<Error, long> Insert(NotificationInsertModel entity)
        {
            try
            {
                using (var context = _factory.Create())
                {
                    if (context.Customer.Any(x => x.Id == entity.CustomerId))
                    {
                        string returnPaymentNotification = ReturnNotification(string.Empty, entity.AmountBalance.ToString());

                        var notificationEntity = new NotificationEntity()
                        {
                            CustomerId = entity.CustomerId,
                            NotificationMessage = returnPaymentNotification
                        };

                        var addedEntity = context.Notification.Add(notificationEntity).Entity;
                        context.SaveChanges();
                        return new Right<Error, long>(addedEntity.Id);
                    }

                    return new Left<Error, long>(new ValidationError(nameof(entity)));
                }
            }
            catch (Exception ex)
            {
                return new Left<Error, long>(new UnknownError(ex.ToString()));
            }
        }

        /// <summary>
        /// Insert new restaurant notification entities for all customer
        /// </summary>
        /// <param name="restaurantName">Parameter with which we create notifications for all customers</param>
        /// <returns>Boolean value: true</returns>
        public async Task<Either<Error, bool>> InsertRestauranNotificationsAsync(string restaurantName)
        {
            try
            {
                using (var context = _factory.Create())
                {
                    var customerIds = from customer in context.Customer
                                      select customer.Id;

                    string notificationMessage = ReturnNotification(restaurantName, string.Empty);

                    if (customerIds.Any())
                    {
                        foreach (var customerId in customerIds)
                        {
                            var notificationEntity = new NotificationEntity()
                            {
                                CustomerId = customerId,
                                NotificationMessage = notificationMessage,
                                Created = DateTime.UtcNow,
                                LastModified = DateTime.UtcNow
                            };

                            await context.Notification.AddAsync(notificationEntity).ConfigureAwait(false);
                        }

                        await context.SaveChangesAsync().ConfigureAwait(false);
                        return new Right<Error, bool>(true);
                    }

                    return new Left<Error, bool>(new ValidationError("Customers not found."));
                }
            }
            catch (Exception ex)
            {
                return new Left<Error, bool>(new UnknownError(ex.ToString()));
            }
        }

        public string ReturnNotification(string restaurantName, string amountBalance)
        {
            if (!string.IsNullOrEmpty(restaurantName))
            {
                return NotificationConstants.RESTAURANT_NOTIFICATION.Replace(NotificationConstants.RESTAURANT_NAME, restaurantName);
            }
            else if (!string.IsNullOrEmpty(amountBalance))
            {
                return NotificationConstants.PAYMENT_NOTIFICATION.Replace(NotificationConstants.REPLACE_AMOUNT, amountBalance);
            }

            return string.Empty;
        }

        public Either<Error, NotificationInsertModel> CheckUserBalance(long paymentId)
        {
            try
            {
                using (var context = _factory.Create())
                {
                    var payment = context.Payment.Include(x => x.Customer).Where(x => x.Id == paymentId).FirstOrDefault();

                    if (payment != null)
                    {
                        NotificationInsertModel notificationModel = new NotificationInsertModel()
                        {
                            CustomerId = payment.Customer.Id,
                            AmountBalance = payment.Customer.Balance,
                            GoogleUserId = payment.Customer.GoogleUserId
                        };

                        return new Right<Error, NotificationInsertModel>(notificationModel);
                    }

                    return new Left<Error, NotificationInsertModel>(new RecordNotFound($"Payment with Id: {paymentId} not found."));
                }
            }
            catch (Exception ex)
            {
                return new Left<Error, NotificationInsertModel>(new UnknownError(ex.ToString()));
            }
        }
    }
}
