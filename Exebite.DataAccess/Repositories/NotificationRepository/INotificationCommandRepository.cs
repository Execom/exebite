﻿using System.Threading.Tasks;
using Either;
using Exebite.Common;

namespace Exebite.DataAccess.Repositories.NotificationRepository
{
    public interface INotificationCommandRepository : IDatabaseCommandRepository<long, NotificationInsertModel, NotificationUpdateModel>
    {
        Task<Either<Error, bool>> InsertRestauranNotificationsAsync(string restaurantName);

        string ReturnNotification(string restaurantName, string amountBalance);

        Either<Error, NotificationInsertModel> CheckUserBalance(long paymentId);
    }
}
