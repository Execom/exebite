﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Either;
using Exebite.Common;
using Exebite.DataAccess.Context;
using Exebite.DomainModel;

namespace Exebite.DataAccess.Repositories
{
    public class NotificationQueryRepository : INotificationQueryRepository
    {
        private readonly IMapper _mapper;
        private readonly IMealOrderingContextFactory _factory;

        public NotificationQueryRepository(IMealOrderingContextFactory factory, IMapper mapper)
        {
            _factory = factory ?? throw new ArgumentNullException(nameof(factory));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        /// <summary>
        /// Get notification entities by specified properties
        /// </summary>
        /// <param name="queryModel">Properties to filter by</param>
        /// <returns>List of notification entities</returns>
        public Either<Error, PagingResult<Notification>> Query(NotificationQueryModel queryModel)
        {
            try
            {
                if (queryModel == null)
                {
                    return new Left<Error, PagingResult<Notification>>(new ArgumentNotSet(nameof(queryModel)));
                }

                using (var context = _factory.Create())
                {
                    var query = context.Notification.AsQueryable();

                    query = query.Where(x => x.CustomerId == queryModel.CustomerId);

                    var size = queryModel.Size <= QueryConstants.MaxElements ? queryModel.Size : QueryConstants.MaxElements;
                    var total = query.Count();

                    var result = query.Select(x => new
                    {
                        x.Id,
                        x.CustomerId,
                        x.NotificationMessage,
                        x.Created,
                        Read = x.Customer.LastNotification.HasValue && x.Customer.LastNotification > x.Created
                    });

                    result = result
                        .Skip((queryModel.Page - 1) * size)
                        .Take(size);

                    var mapped = _mapper.Map<IList<Notification>>(result).ToList().OrderByDescending(x => x.Created);

                    var customer = context.Customer.Find(queryModel.CustomerId);

                    if (customer == null)
                    {
                        return new Left<Error, PagingResult<Notification>>(new RecordNotFound(nameof(customer)));
                    }

                    customer.LastNotification = DateTime.UtcNow;
                    context.SaveChanges();

                    return new Right<Error, PagingResult<Notification>>(new PagingResult<Notification>(mapped, total));
                }
            }
            catch (Exception ex)
            {
                return new Left<Error, PagingResult<Notification>>(new UnknownError(ex.ToString()));
            }
        }

        public Either<Error, long> ReturnNumberOfNewNotifications(long customerId)
        {
            try
            {
                using (var context = _factory.Create())
                {
                    var query = context.Notification;
                    var customer = context.Customer.Find(customerId);
                    if (customer == null)
                    {
                        return new Left<Error, long>(new RecordNotFound($"Customer with id:{customerId} not found."));
                    }

                    var result = customer.LastNotification != null ?
                         query.Where(x => x.CustomerId == customer.Id && x.Created > customer.LastNotification).Count() :
                         query.Where(x => x.CustomerId == customer.Id).Count();

                    return new Right<Error, long>(result);
                }
            }
            catch (Exception ex)
            {
                return new Left<Error, long>(new UnknownError(ex.ToString()));
            }
        }
    }
}
