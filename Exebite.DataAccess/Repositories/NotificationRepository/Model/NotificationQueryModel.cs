﻿using System.ComponentModel.DataAnnotations;

namespace Exebite.DataAccess.Repositories
{
    public class NotificationQueryModel : QueryBase
    {
        public NotificationQueryModel()
            : base()
        {
        }

        public NotificationQueryModel(int page, int size)
            : base(page, size)
        {
        }

        [Required]
        [Range(minimum: 1, maximum: int.MaxValue)]
        public long CustomerId { get; set; }
    }
}
