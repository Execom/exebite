﻿namespace Exebite.DataAccess.Repositories
{
    public class NotificationInsertModel
    {
        public long CustomerId { get; set; }

        public decimal AmountBalance { get; set; }

        public string GoogleUserId { get; set; }
    }
}
