﻿namespace Exebite.DataAccess.Repositories
{
    public class NotificationUpdateModel
    {
        public long CustomerId { get; set; }

        public string NotificationMessage { get; set; }
    }
}
