﻿using Either;
using Exebite.Common;
using Exebite.DataAccess.Repositories.OrderRepository.Model;
using Exebite.DomainModel;

namespace Exebite.DataAccess.Repositories
{
    public interface IOrderQueryRepository : IDatabaseQueryRepository<Order, OrderQueryModel>
    {
        Either<Error, PagingResult<Order>> GetOrders(GetOrdersModel model);
    }
}
