﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Either;
using Exebite.Common;
using Exebite.DataAccess.Context;
using Exebite.DataAccess.Entities;
using Exebite.DataAccess.Repositories.OrderRepository.Model;
using Exebite.DomainModel;
using Exebite.DomainModel.Orders;
using Microsoft.EntityFrameworkCore;

namespace Exebite.DataAccess.Repositories
{
    public class OrderQueryRepository : IOrderQueryRepository
    {
        private readonly IMapper _mapper;
        private readonly IMealOrderingContextFactory _factory;

        public OrderQueryRepository(IMealOrderingContextFactory factory, IMapper mapper)
        {
            _factory = factory ?? throw new ArgumentNullException(nameof(factory));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public Either<Error, PagingResult<Order>> GetOrders(GetOrdersModel model)
        {
            try
            {
                if (model == null)
                {
                    return new Left<Error, PagingResult<Order>>(new ArgumentNotSet(nameof(model)));
                }

                using (var context = _factory.Create())
                {
                    var query = Query(context, new OrderQueryModel { CustomerId = model.CustomerId });

                    var total = query.Count();
                    query = query
                        .Skip((model.Page - 1) * model.Size)
                        .Take(model.Size);

                    var result = query.Select(o => new
                    {
                        o.Id,
                        o.Date,
                        o.Total,
                        Restaurants = o.OrdersToMeals.GroupBy(x => x.Meal.Restaurant.Id)
                                                     .Select(om => new OrderRestaurant
                                                     {
                                                         Id = om.Key,
                                                         Name = om.Select(r => r.Meal.Restaurant.Name).First(),
                                                         Meals = om.Select(r => new OrderToMeal
                                                         {
                                                             MealId = r.MealId,
                                                             Name = r.Meal.Name,
                                                             Note = r.Note,
                                                             Price = r.Meal.Price,
                                                             Quantity = r.Quantity
                                                         })
                                                     })
                    }).OrderByDescending(x => x.Date);

                    var mapped = _mapper.Map<IList<Order>>(result).ToList();

                    return new Right<Error, PagingResult<Order>>(new PagingResult<Order>(mapped, total));
                }
            }
            catch (Exception ex)
            {
                return new Left<Error, PagingResult<Order>>(new UnknownError(ex.ToString()));
            }
        }

        public Either<Error, PagingResult<Order>> Query(OrderQueryModel queryModel)
        {
            throw new NotImplementedException();
        }

        private IQueryable<OrderEntity> Query(MealOrderingContext context, OrderQueryModel queryModel)
        {
            var query = context.Order.Include(o => o.OrdersToMeals).AsQueryable();

            query = query.Where(x => x.CustomerId == queryModel.CustomerId);

            if (queryModel.Id != null)
            {
                query = query.Where(x => x.Id == queryModel.Id.Value);
            }

            if (queryModel.Date != null)
            {
                query = query.Where(x => x.Date == queryModel.Date.Value);
            }

            return query;
        }
    }
}
