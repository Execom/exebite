﻿namespace Exebite.DataAccess.Repositories.OrderRepository.Model
{
    public class GetOrdersModel : QueryBase
    {
        public GetOrdersModel()
        {
        }

        public GetOrdersModel(int page, int size)
            : base(page, size)
        {
        }

        public long CustomerId { get; set; }
    }
}
