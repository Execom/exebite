﻿using System;
using System.Collections.Generic;
using Exebite.DomainModel;

namespace Exebite.DataAccess.Repositories
{
    public class OrderInsertModel
    {
        public decimal Total { get; set; }

        public DateTime Date { get; set; }

        public short LocationId { get; set; }

        public long CustomerId { get; set; }

        public List<OrderToMeal> Meals { get; set; }
    }
}
