﻿using Exebite.DataAccess.Repositories.CommentRepository.Model;
using Exebite.DomainModel;

namespace Exebite.DataAccess.Repositories.CommentRepository
{
    public interface ICommentQueryRepository : IDatabaseQueryRepository<Comment, CommentQueryModel>
    {
    }
}
