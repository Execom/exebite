﻿using Either;
using Exebite.Common;
using Exebite.DataAccess.Repositories.CommentRepository.Model;
using Exebite.DtoModels.Comment;

namespace Exebite.DataAccess.Repositories.CommentRepository
{
    public interface ICommentCommandRepository : IDatabaseCommandRepository<long, CommentInsertModel, CommentUpdateModel>
    {
        Either<Error, bool> InsertReaction(CommentInsertReactionModel commentReactionModel);

        Either<Error, bool> DeleteReaction(long reactionId, CommentDeleteReactionModel commentReactionModel);

        Either<Error, bool> Delete(int commentId, DeleteCommentDto comment);
    }
}
