﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Either;
using Exebite.Common;
using Exebite.DataAccess.Context;
using Exebite.DataAccess.Entities;
using Exebite.DataAccess.Repositories.CommentRepository.Model;
using Exebite.DomainModel;
using Exebite.DomainModel.Enums;
using Microsoft.EntityFrameworkCore;

namespace Exebite.DataAccess.Repositories.CommentRepository
{
    public class CommentQueryRepository : ICommentQueryRepository
    {
        private readonly IMapper _mapper;
        private readonly IMealOrderingContextFactory _factory;

        public CommentQueryRepository(IMealOrderingContextFactory factory, IMapper mapper)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _factory = factory ?? throw new ArgumentNullException(nameof(factory));
        }

        /// <summary>
        /// Get comment entities by specified properties
        /// </summary>
        /// <param name="queryModel">Properties to filter by</param>
        /// <returns>List of comment entities</returns>
        public Either<Error, PagingResult<Comment>> Query(CommentQueryModel queryModel)
        {
            try
            {
                if (queryModel == null)
                {
                    return new Left<Error, PagingResult<Comment>>(new ArgumentNotSet(nameof(queryModel)));
                }

                using (var context = _factory.Create())
                {
                    var query = context.Comment.Include(c => c.CommentReactions).AsQueryable();

                    if (queryModel.MealId.HasValue)
                    {
                        query = query.Where(x => x.MealId == queryModel.MealId);
                    }

                    if (queryModel.CommentId.HasValue)
                    {
                        query = query.Where(x => x.MealId == queryModel.CommentId);
                    }

                    query = FilterByCommentSort(queryModel, query);

                    var size = queryModel.Size <= QueryConstants.MaxElements ? queryModel.Size : QueryConstants.MaxElements;
                    var total = query.Count();

                    query = query
                        .Skip((queryModel.Page - 1) * size)
                        .Take(size);

                    var result = query.Select(x => new
                    {
                        x.CommentId,
                        x.Text,
                        x.Created,
                        NumberOfLikes = x.CommentReactions.Count(),
                        IsLiked = x.CommentReactions.Any(rc => rc.CustomerId == queryModel.CustomerId),
                        x.Customer,
                        x.Meal
                    });
                    var mapped = _mapper.Map<IList<Comment>>(result).ToList();

                    return new Right<Error, PagingResult<Comment>>(new PagingResult<Comment>(mapped, total));
                }
            }
            catch (Exception ex)
            {
                return new Left<Error, PagingResult<Comment>>(new UnknownError(ex.ToString()));
            }
        }

        private IQueryable<CommentEntity> FilterByCommentSort(CommentQueryModel model, IQueryable<CommentEntity> query)
        {
            switch (model.SortType)
            {
                case CommentSort.MostLiked:
                    return query.OrderByDescending(q => q.CommentReactions.Count);
                case CommentSort.MyComments:
                    return model.CustomerId.HasValue ? query.Where(c => c.CustomerId == model.CustomerId.Value) : query.Where(c => c.CustomerId == 0);
                case CommentSort.MostRecent:
                default:
                    return query.OrderByDescending(c => c.Created);
            }
        }
    }
}
