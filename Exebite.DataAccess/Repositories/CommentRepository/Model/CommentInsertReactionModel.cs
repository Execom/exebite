﻿namespace Exebite.DataAccess.Repositories.CommentRepository.Model
{
    public class CommentInsertReactionModel
    {
        public long MealId { get; set; }

        public int CommentId { get; set; }

        public long CustomerId { get; set; }
    }
}
