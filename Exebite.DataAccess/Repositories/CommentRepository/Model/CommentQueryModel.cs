﻿using Exebite.DomainModel.Enums;

namespace Exebite.DataAccess.Repositories.CommentRepository.Model
{
    public class CommentQueryModel : QueryBase
    {
        public CommentQueryModel()
        {
        }

        public CommentQueryModel(int page, int size)
            : base(page, size)
        {
        }

        public long? MealId { get; set; }

        public long? CustomerId { get; set; }

        public int? CommentId { get; set; }

        public CommentSort SortType { get; set; }
    }
}
