﻿namespace Exebite.DataAccess.Repositories.CommentRepository.Model
{
    public class CommentDeleteReactionModel
    {
        public long MealId { get; set; }

        public int CommentId { get; set; }

        public long CustomerId { get; set; }
    }
}
