﻿namespace Exebite.DataAccess.Repositories.CommentRepository.Model
{
    public class CommentUpdateModel
    {
        public long MealId { get; set; }

        public long CustomerId { get; set; }

        public string Text { get; set; }
    }
}
