﻿using System;
using System.Linq;
using Either;
using Exebite.Common;
using Exebite.DataAccess.Context;
using Exebite.DataAccess.Entities;
using Exebite.DataAccess.Repositories.CommentRepository.Model;
using Exebite.DomainModel;
using Exebite.DtoModels.Comment;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;

namespace Exebite.DataAccess.Repositories.CommentRepository
{
    public class CommentCommandRepository : ICommentCommandRepository
    {
        private readonly IMealOrderingContextFactory _factory;

        public CommentCommandRepository(IMealOrderingContextFactory factory)
        {
            _factory = factory ?? throw new ArgumentNullException(nameof(factory));
        }

        /// <summary>
        /// Deletes comment entity
        /// </summary>
        /// <param name="commentId">Comment id</param>
        /// <param name="commentDto">Comment properties</param>
        /// <returns>Flag that states is entity deleted successfully</returns>
        public Either<Error, bool> Delete(int commentId, DeleteCommentDto commentDto)
        {
            try
            {
                using (var context = _factory.Create())
                {
                    var comment = context.Comment.SingleOrDefault(c => c.MealId == commentDto.MealId && c.CommentId == commentId && c.CustomerId == commentDto.CustomerId);
                    if (comment == null)
                    {
                        return new Left<Error, bool>(new RecordNotFound(nameof(comment)));
                    }

                    context.Comment.Remove(comment);
                    context.SaveChanges();

                    return new Right<Error, bool>(true);
                }
            }
            catch (Exception ex)
            {
                return new Left<Error, bool>(new UnknownError(ex.ToString()));
            }
        }

        public Either<Error, bool> Delete(long id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Delete comment like reaction
        /// </summary>
        /// <param name="reactionId">Id of a reaction</param>
        /// <param name="entity">Properties that describe which comment is used</param>
        /// <returns>Is reaction successfully deleted</returns>
        public Either<Error, bool> DeleteReaction(long reactionId, CommentDeleteReactionModel entity)
        {
            try
            {
                using (var context = _factory.Create())
                {
                    var comment = context.Comment.Include(c => c.CommentReactions)
                                                 .SingleOrDefault(c => c.MealId == entity.MealId && c.CommentId == entity.CommentId);
                    if (comment != null && comment.CustomerId != entity.CustomerId)
                    {
                        var reaction = comment.CommentReactions
                                              .SingleOrDefault(rc => rc.CustomerId == entity.CustomerId && rc.ReactionId == reactionId);
                        if (reaction != null)
                        {
                            context.ReactionToComment.Remove(reaction);
                            context.SaveChanges();

                            return new Right<Error, bool>(true);
                        }
                    }

                    return new Left<Error, bool>(new RecordNotFound(nameof(entity)));
                }
            }
            catch (Exception ex)
            {
                return new Left<Error, bool>(new UnknownError(ex.ToString()));
            }
        }

        /// <summary>
        /// Inserts new comment entity
        /// </summary>
        /// <param name="entity">Properties that comment entity has</param>
        /// <returns>Id of a new entity</returns>
        public Either<Error, long> Insert(CommentInsertModel entity)
        {
            try
            {
                using (var context = _factory.Create())
                {
                    if (context.Meal.Any(m => m.Id == entity.MealId) && context.Customer.Any(c => c.Id == entity.CustomerId))
                    {
                        var commentEntity = new CommentEntity()
                        {
                            MealId = entity.MealId,
                            CustomerId = entity.CustomerId,
                            Text = entity.Text
                        };

                        var addedEntity = context.Comment.Add(commentEntity).Entity;
                        context.SaveChanges();

                        return new Right<Error, long>(addedEntity.CommentId);
                    }

                    return new Left<Error, long>(new ValidationError(nameof(entity)));
                }
            }
            catch (Exception ex)
            {
                return new Left<Error, long>(new UnknownError(ex.ToString()));
            }
        }

        /// <summary>
        /// Insert like reaction to comment
        /// </summary>
        /// <param name="entity">Properties that describe which comment is used</param>
        /// <returns>Is reaction successfully saved</returns>
        public Either<Error, bool> InsertReaction(CommentInsertReactionModel entity)
        {
            try
            {
                using (var context = _factory.Create())
                {
                    var comment = context.Comment.Include(c => c.CommentReactions)
                                                 .SingleOrDefault(c => c.MealId == entity.MealId && c.CommentId == entity.CommentId);
                    if (comment != null && CheckIfCommentReactionCanBeSaved(context, comment, entity))
                    {
                        var commentReaction = new ReactionToCommentEntity
                        {
                            MealId = entity.MealId,
                            CommentId = entity.CommentId,
                            CustomerId = entity.CustomerId,
                            ReactionId = context.Reaction.Single(x => x.Description.Equals(nameof(ReactionType.Like))).Id
                        };
                        context.Add(commentReaction);
                        context.SaveChanges();

                        return new Right<Error, bool>(true);
                    }

                    return new Left<Error, bool>(new ValidationError(nameof(entity)));
                }
            }
            catch (Exception ex)
            {
                return new Left<Error, bool>(new UnknownError(ex.ToString()));
            }
        }

        /// <summary>
        /// Updates existing comment entity
        /// </summary>
        /// <param name="commentId">Id of a comment that will be updated</param>
        /// <param name="entity">Properties that comment entity has</param>
        /// <returns>Flag that states is entity updated successfully</returns>
        public Either<Error, bool> Update(long commentId, CommentUpdateModel entity)
        {
            try
            {
                if (entity == null)
                {
                    return new Left<Error, bool>(new ArgumentNotSet(nameof(entity)));
                }

                using (var context = _factory.Create())
                {
                    var currentEntity = context.Comment.SingleOrDefault(c => c.MealId == entity.MealId && c.CommentId == commentId && c.CustomerId == entity.CustomerId);
                    if (currentEntity == null)
                    {
                        return new Left<Error, bool>(new RecordNotFound(nameof(entity)));
                    }

                    currentEntity.Text = entity.Text;

                    context.SaveChanges();
                }

                return new Right<Error, bool>(true);
            }
            catch (Exception ex)
            {
                return new Left<Error, bool>(new UnknownError(ex.ToString()));
            }
        }

        private bool CheckIfCommentReactionCanBeSaved(MealOrderingContext context, CommentEntity commentEntity, CommentInsertReactionModel model)
        {
            return commentEntity.CustomerId != model.CustomerId &&
                   context.Customer.Any(c => c.Id == model.CustomerId) &&
                   !commentEntity.CommentReactions.Any(rc => rc.CustomerId == model.CustomerId);
        }
    }
}
