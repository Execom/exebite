﻿namespace Exebite.DataAccess.Repositories
{
    public class CustomerByIdModel
    {
        public long? Id{ get; set; }

        public string GoogleUserId { get; set; }
    }
}
