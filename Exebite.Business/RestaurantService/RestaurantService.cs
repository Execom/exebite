﻿using System;
using System.Collections.Generic;
using System.Linq;
using Either;
using Exebite.Business.Model;
using Exebite.Common;
using Exebite.Common.Constants;
using Exebite.DataAccess.Repositories;
using Exebite.DomainModel;
using Exebite.DomainModel.Meals;
using Exebite.GoogleSheetAPI.Services;

namespace Exebite.Business
{
    public class RestaurantService : IRestaurantService
    {
        private readonly IMealQueryRepository _mealQuery;
        private readonly ICustomerQueryRepository _customerQuery;
        private readonly ILocationQueryRepository _locationQuery;
        private readonly IGoogleSheetAPIService _apiService;

        public RestaurantService(
            IMealQueryRepository mealQuery,
            ICustomerQueryRepository customerQuery,
            ILocationQueryRepository locationQuery,
            IGoogleSheetAPIService googleSheetAPIService)
        {
            _mealQuery = mealQuery;
            _customerQuery = customerQuery;
            _locationQuery = locationQuery;
            _apiService = googleSheetAPIService;
        }

        public Either<Error, RestaurantOrder> PlaceOrdersForRestaurant(RestaurantOrder order)
        {
            try
            {
                if (order == null)
                {
                    return new Left<Error, RestaurantOrder>(new ArgumentNotSet(nameof(order)));
                }

                var customer = _customerQuery.Query(new CustomerQueryModel() { Id = order.CustomerId })
                    .Map(c => c.Items.First())
                    .Reduce(_ => throw new Exception("Customer not found!"));

                var location = _locationQuery.Query(new LocationQueryModel() { Id = order.LocationId })
                    .Map(l => l.Items.First())
                    .Reduce(_ => throw new Exception("Location not found!"));

                var restaurants = new Dictionary<string, MealContainer>();

                foreach (var mealOrder in order.Meals)
                {
                    var meal = _mealQuery.Query(new MealQueryModel() { Id = mealOrder.MealId })
                        .Map(c => c.Items.First())
                        .Reduce(_ => throw new Exception("Meal not found!"));

                    if (CheckIfRestaurantAcceptOrders(meal.Restaurant.OrderDue))
                    {
                        AddRestaurantIfItDoesntExist(restaurants, meal.Restaurant.Name);
                        AddMeal(restaurants[meal.Restaurant.Name], meal);
                    }
                }

                if (!restaurants.Any())
                {
                    return new Left<Error, RestaurantOrder>(new ArgumentNotSet(nameof(restaurants)));
                }

                _apiService.WriteOrder(customer.Name, location.Name, restaurants);

                return order;
            }
            catch (Exception ex)
            {
                return new Left<Error, RestaurantOrder>(new UnknownError(ex.ToString()));
            }
        }

        /// <summary>
        /// Update daily menu for restaurants
        /// </summary>
        public void UpdateRestaurantMenu()
        {
            _apiService.UpdateDailyMenuLipa();
            _apiService.UpdateDailyMenuMimas();
            _apiService.UpdateDailyMenuParrilla();
            _apiService.UpdateDailyMenuTopliObrok();
            _apiService.UpdateDailyMenuSerpica();
        }

        private void AddRestaurantIfItDoesntExist(Dictionary<string, MealContainer> restaurants, string restaurantName)
        {
            if (!restaurants.ContainsKey(restaurantName))
            {
                restaurants.Add(restaurantName, new MealContainer());
            }
        }

        private void AddMeal(MealContainer mealContainer, Meal meal)
        {
            if (meal.IsFromStandardMenu)
            {
                mealContainer.StandardMeals.Add(meal);
            }
            else
            {
                mealContainer.DailyMeals.Add(meal);
            }
        }

        private bool CheckIfRestaurantAcceptOrders(DateTime? orderDue)
        {
            if (orderDue.HasValue)
            {
                var currentDate = DateTime.Now;
                if (currentDate.Hour == orderDue.Value.Hour)
                {
                    return currentDate.Minute <= orderDue.Value.Minute;
                }

                return currentDate.Hour < orderDue.Value.Hour;
            }

            throw new NullReferenceException(nameof(orderDue));
        }
    }
}
