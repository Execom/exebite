﻿namespace Exebite.Business.Model
{
    public class MealOrder
    {
        public long MealId { get; set; }

        public string Note { get; set; }

        public int Quantity { get; set; }
    }
}
