﻿using System;
using System.Collections.Generic;

namespace Exebite.Business.Model
{
    public class RestaurantOrder
    {
        public decimal Total { get; set; }

        public DateTime Date { get; set; }

        public short LocationId { get; set; }

        public int CustomerId { get; set; }

        public List<MealOrder> Meals { get; set; }
    }
}
