﻿using System.Threading.Tasks;

namespace Exebite.Business
{
    public interface INotificationHubClientService
    {
        Task ReceiveMessage(string message);
    }
}
