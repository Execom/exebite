﻿using System;
using System.Threading.Tasks;
using Exebite.Business.Enums;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;

namespace Exebite.Business
{
    public class NotificationHubService : Hub<INotificationHubClientService>
    {
        [Authorize]

        // Called when a new connection is established with the hub
        public override async Task OnConnectedAsync()
        {
            if (!string.IsNullOrEmpty(Context?.User?.Identity?.Name))
            {
                await Groups.AddToGroupAsync(Context.ConnectionId, Context.User.Identity.Name).ConfigureAwait(false);
                await Groups.AddToGroupAsync(Context.ConnectionId, nameof(NameOfHubGroups.HubUsers)).ConfigureAwait(false);
            }

            await base.OnConnectedAsync().ConfigureAwait(false);
        }

        // Called when a connection with the hub is terminated.
        public override async Task OnDisconnectedAsync(Exception exception)
        {
            if (!string.IsNullOrEmpty(Context?.User?.Identity?.Name))
            {
                await Groups.RemoveFromGroupAsync(Context.ConnectionId, Context.User.Identity.Name).ConfigureAwait(false);
                await Groups.RemoveFromGroupAsync(Context.ConnectionId, nameof(NameOfHubGroups.HubUsers)).ConfigureAwait(false);
            }

            await base.OnDisconnectedAsync(exception).ConfigureAwait(false);
        }
    }
}
